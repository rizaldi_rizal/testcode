<?php
include_once APPPATH.'/third_party/mpdf/mpdf.php';


class Pdfm
{
    public $param;
    public $pdf;

    public function __construct()
    {
        $CI = & get_instance();
    }

    function load($param=NULL)
    {
        if ($param == NULL || !is_array($param))
        {
            return new mPDF("c","A5","","",10,10,15,15,0,10);
        }
        else{
            $mode = (isset($param[0]) && !empty($param[0])) ? $param[0] : "en-GB-x";
            $formatPage = (isset($param[1]) && !empty($param[1])) ? $param[1] : "A5";
            $defaultFontSize = (isset($param[2]) && !empty($param[2])) ? $param[2] : "";
            $defaultFont = (isset($param[3]) && !empty($param[3])) ? $param[3] : "";
            $marginLeft = (isset($param[4]) && !empty($param[4])) ? $param[4] : 5;
            $marginRight = (isset($param[5]) && !empty($param[5])) ? $param[5] : 5;
            $marginTop = (isset($param[6]) && !empty($param[6])) ? $param[6] : 5;
            $marginBottom = (isset($param[7]) && !empty($param[7])) ? $param[7] : 10;
            $marginHeader = (isset($param[8]) && !empty($param[8])) ? $param[8] : 0;
            $marginFooter = (isset($param[9]) && !empty($param[9])) ? $param[9] : 10;
            return new mPDF($mode, $formatPage, $defaultFontSize, $defaultFont, $marginLeft, $marginRight, $marginTop, $marginBottom, $marginHeader, $marginFooter);
        }
    }
}