<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Name:  FPDF
* 
* Author: Jd Fiscus
* 	 	  jdfiscus@gmail.com
*         @iamfiscus
*          
*
* Origin API Class: http://www.fpdf.org/
* 
* Location: http://github.com/iamfiscus/Codeigniter-FPDF/
*          
* Created:  06.22.2010 
* 
* Description:  This is a Codeigniter library which allows you to generate a PDF with the FPDF library
* 
*/
use setasign\Fpdi;
use setasign\FpdiProtection\FpdiProtection;
require_once APPPATH.'third_party/fpdf/fpdf-1.8.php';
require_once('../vendor/autoload.php');
if (!class_exists("REST_Controller")) {
    require_once APPPATH . 'libraries/REST_Controller.php';
}

class Fpdf_gen{
	
    public $files = array();
	
	public function __construct() {
		$CI =& get_instance();
		$pdf = new FPDF();
		// $pdf->AddPage();
		$CI->fpdf = $pdf;
	}
	
	public function concat($files, $sample, $password='') {
		$pdf = new Fpdi\Fpdi();
		// $pdf->AddPage();
        $pagecount = $pdf->setSourceFile($files);
        $new_pdf = new Fpdi\Fpdi();
		$new_pdf->setSourceFile($files);
		// Split each page into a new PDF
		for ($i = 1; $i <= $pagecount; $i++) {			
			if($i <= $sample){
				try {
					$template = $new_pdf->importPage($i);
					$specs = $new_pdf->getTemplateSize($template);
					$orientation = $specs['h'] > $specs['w'] ? 'P' : 'L';
					$new_pdf->AddPage($orientation);
					$new_pdf->useTemplate($template, 0, 0, $specs['w'], $specs['y'], true);
					$new_filename = str_replace('.pdf', '', $files).'_free'.".pdf";
					$resp["filename"] = $new_filename;
					$resp["message"] = "Page ".$i." split into ".$new_filename."<br />\n";
				} catch (Exception $e) {
					$resp["filename"] = "";
					$resp["message"] = 'Caught exception: '.$e->getMessage(). "\n";
				}
			}
		}
		$new_pdf->Output($new_filename, "F");
		if(file_exists($new_filename) && !empty($password)){
		    $this->setPass($files, $password);
//		    $this->setPass($new_filename, $password);
        }
		return $resp;
    }

	public function setPass($files, $password) {
        $ci =& get_instance();
		$new_pdf = new FpdiProtection();
        $pagecount=$new_pdf->setSourceFile($files);
        for ($loop = 1; $loop <= $pagecount; $loop++) {
            $template = $new_pdf->importPage($loop);
            $specs = $new_pdf->getTemplateSize($template);
            $orientation = $specs['h'] > $specs['w'] ? 'P' : 'L';
            $new_pdf->AddPage($orientation);
            $new_pdf->useTemplate($template,0, 0, $specs['w'], $specs['y'], true);
        }
        if(!empty($password)) {
            $new_pdf->SetProtection(array(), $password);
            if (base_url() != "http://" . REST_Controller::api_prod && base_url() != "https://" . REST_Controller::api_prod) {
                $ci->load->helper("file");
                $txtPath = "../uploads/pass.txt";
                if (file_exists($txtPath)) {
                    write_file($txtPath, $files . "-" . $password . "\r\n", 'a');
                } else {
                    write_file($txtPath, $files . "-" . $password . "\r\n");
                }
            }
        }
		// Split each page into a new PDF
		$new_pdf->Output($files, "F");
		if(file_exists($files)) return TRUE;
		else return FALSE;
	}

	public function countPage($files) {
		$new_pdf = new FpdiProtection();
        $pagecount=$new_pdf->setSourceFile($files);
        return $pagecount;
	}
}