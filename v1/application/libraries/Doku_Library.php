<?php
/**
 * Doku's Global Function
 */

if (!class_exists('Doku_Library')) {
	class Doku_Library
    {
		const prePaymentUrl = 'http://staging.doku.com/api/payment/PrePayment';
		const paymentUrl = 'http://staging.doku.com/api/payment/paymentMip';
		const directPaymentUrl = 'http://staging.doku.com/api/payment/PaymentMIPDirect';
		const generateCodeUrl = 'http://staging.doku.com/api/payment/doGeneratePaymentCode';
		const checkStatus = 'https://staging.doku.com/CheckStatus';

		const sharedKey = 'GDFbb91dn6O2'; //doku's merchant unique key
		const mallId = 4784; //doku's merchant id
		const codeExpired = 60; //minutes
		const chainMerchant = 'NA';
		const currency = 360;
		
		public function __construct()
        {
            $this->ci =& get_instance();
        }

		
        public static function doCreateWords($data)
        {

//            if (!empty($data['device_id'])) {
//
//                if (!empty($data['pairing_code'])) {
//
//                    return sha1($data['amount'] . mallId . sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'] . $data['device_id']);
//
//                } else {
//
//                    return sha1($data['amount'] . mallId . sharedKey . $data['invoice'] . $data['currency'] . $data['device_id']);
//
//                }
//
//            } else if (!empty($data['pairing_code'])) {
//
//                return sha1($data['amount'] . mallId . sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code']);
//
//            } else if (!empty($data['currency'])) {
//
//                return sha1($data['amount'] . mallId . sharedKey . $data['invoice'] . $data['currency']);
//
//            } else {
//
//                return sha1($data['amount'] . mallId . sharedKey . $data['invoice']);
//
//            }


            if (!empty($data['device_id'])) {
//                return sha1($data['amount']
//                    . mallId
//                    . sharedKey
//                    . $data['invoice']
//                    . $data['currency']
//                    . $data['device_id']);

                return sha1($data['amount']
                    . Doku_Library::mallId
                    . Doku_Library::sharedKey
                    . $data['invoice']
                    . $data['currency']
                    . $data['token']
                    . $data['pairing_code']
                    . $data['device_id']);

            } else if (!empty($data['trans_id_merchant'])) {
                return sha1(Doku_Library::mallId)
                    . $data['trans_id_merchant']
                    . Doku_Library::currency;
            } else {
                return sha1($data['amount']
                    . Doku_Library::mallId
                    . Doku_Library::sharedKey
                    . $data['invoice']
                    . $data['currency']);
            }

        }

        public static function doCreateWordsRaw($data)
        {

            if (!empty($data['device_id'])) {

                if (!empty($data['pairing_code'])) {

                    return $data['amount'] . Doku_Library::mallId . Doku_Library::sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'] . $data['device_id'];

                } else {

                    return $data['amount'] . Doku_Library::mallId . Doku_Library::sharedKey . $data['invoice'] . $data['currency'] . $data['device_id'];

                }

            } else if (!empty($data['pairing_code'])) {

                return $data['amount'] . Doku_Library::mallId . Doku_Library::sharedKey . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'];

            } else if (!empty($data['currency'])) {

                return $data['amount'] . Doku_Library::mallId . Doku_Library::sharedKey . $data['invoice'] . $data['currency'];

            } else {

                return $data['amount'] . Doku_Library::mallId . Doku_Library::sharedKey . $data['invoice'];

            }
            echo json_encode($data);
        }

        public static function formatBasket($data)
        {

            if (is_array($data)) {
                foreach ($data as $basket) {
                    return $basket['name'] . ',' . $basket['amount'] . ',' . $basket['quantity'] . ',' . $basket['subtotal'] . ';';
                }
            } else if (is_object($data)) {
                foreach ($data as $basket) {
                    return $basket->name . ',' . $basket->amount . ',' . $basket->quantity . ',' . $basket->subtotal . ';';
                }
            }
        }

    }
}