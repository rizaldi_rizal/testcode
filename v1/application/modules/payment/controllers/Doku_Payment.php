<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
date_default_timezone_set('Asia/Jakarta');
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

class Doku_Payment extends REST_Controller
{

    public function __construct()
    {
        //code here
        parent::__construct();
        $this->load->library("Doku_Library");
        $this->load->library("Doku_Api");
    }

    public function doGeneratePaycode_post()
    {
        $bodyPost = $this->post();
        $params = array(
            'amount' => $bodyPost["amount"],
            'invoice' => $bodyPost["trans_id"],
            'currency' => $bodyPost["currency"]
        );

        $respss = array();
        $words = Doku_Library::doCreateWords($params);

        $customer = array(
            'name' => 'TEST NAME',
            'data_phone' => '08121111111',
            'data_email' => 'test@test.com',
            'data_address' => 'bojong gede #1 08/01'
        );

        $dataPayment = array(
            'req_mall_id' => Doku_Library::mallId,
            'req_chain_merchant' => $bodyPost["chain_merchant"],
            'req_amount' => $params['amount'],
            'req_words' => $words,
            'req_trans_id_merchant' => $params['invoice'],
            'req_purchase_amount' => $params['amount'],
            'req_request_date_time' => date('YmdHis'),
            'req_session_id' => sha1(date('YmdHis')),
            'req_email' => $customer['data_email'],
            'req_name' => $customer['name'],
            'req_expiry_time' => Doku_Library::codeExpired
        );

        $response = Doku_Api::doGeneratePaycode($dataPayment);

        if ($response->res_response_code == '0000') {
            $respss["code"] = REST_Controller::HTTP_OK;
            $respss["message"] = "TRANSACTION SUCCESS : ".$response->res_response_msg;
            $respss["data"] = $response;
            $this->echoResponse(REST_Controller::HTTP_OK, $respss);
        } else {
            $respss["code"] = REST_Controller::HTTP_BAD_REQUEST;
            $respss["message"] = "BAD REQUEST [".$response->res_response_code."] : ".$response->res_response_msg;
            $respss["data"] = $response;
            $this->echoResponse(REST_Controller::HTTP_ACCEPTED, $respss);
        }

    }

    public function doPay_post()
    {
        $bodyPost = (object)$this->post();

        //get json request
        $token = $bodyPost->doku_token;
        $device_id = $bodyPost->device_id;
        $currency = (int)$bodyPost->currency_id;
        $pairing_code = $bodyPost->doku_pairing_code;
        $invoice_no = $bodyPost->doku_invoice_no;

        $basket = $bodyPost->baskets;
        $totals = array();
        foreach($basket as $baskey=>$basval){
            $totals[] = $basval["subtotal"];
        }

        $totalamount =  (string)number_format(array_sum($totals),2,".","");

        $params = array(
            'amount' => $totalamount,
            'invoice' => $invoice_no,
            'currency' => $currency,
            'pairing_code' => $pairing_code,
            'token' => $token,
            'device_id' => $device_id
        );

        /* ini untuk test words phrase validation kalau nantinya ada resp words doesn't match
         * words without device
         $wordsss= sha1(
                            $params['amount'] .
                            Doku_Library::mallId .
                            Doku_Library::sharedKey .
                            $params['invoice'] .
                            $params['currency'] .
                            $params['pairing_code']
                    );
         *words with device id
        $wordsss2=   sha1($params['amount']
                    . Doku_Library::mallId
                    . Doku_Library::sharedKey
                    . $params['invoice']
                    . $params['currency']
                    . $params['token']
                    . $params['pairing_code']
                    . $params['device_id']);

         *data dummy for baskets
         *test data for input to db
         *
         $basket[] = array(
            'name' => 'sayur',
            'amount' => '10000.00',
            'quantity' => '1',
            'subtotal' => '10000.00'
        );
        */
        $words = Doku_Library::doCreateWords($params);
        $book_id = $bodyPost->book_id;
        $cust_id = $bodyPost->cust_id;
        $check_book = $this->db->get_where("book",array("user_id"=>$book_id));
        $check_cust = $this->db->get_where("users",array("user_id"=>$cust_id));
        if($check_cust->num_rows() > 0 && $check_book->num_rows() > 0) {
            $insid = array();
            foreach($basket as $baskey=>$basval){
                $totals[] = $basval["subtotal"];
                $data_insertrans = array(
                    "id_mtrans" => $bodyPost->invoice_no,
                    "mtrans_cust" => $cust_id,
                    "mtrans_book" => $book_id,
                    "mtrans_price" => $basval["amount"],
                    "mtrans_type" => "credit card"
                );
                $this->db->insert("transbaboo",$data_insertrans);
                $insid[] = $this->db->insert_id();
            }
            if(empty($insid)){
                $responses = array(
                    "code"=>REST_Controller::HTTP_BAD_REQUEST,
                    "message"=>"data not inserted to trans table",
                    "data"=>(object)array()
                );
                $this->echoResponse(REST_Controller::HTTP_BAD_REQUEST, $responses);
            }
            /*$customer = array(
                'name' => 'TEST NAME',
                'data_phone' => '08121111111',
                'data_email' => 'test@test.com',
                'data_address' => 'bojong gede #1 08/01'
            );*/
            $customer = array(
                'name' => $check_cust->row()->fullname,
                'data_phone' => '0812158123',
                'data_email' => $check_cust->row()->email,
                'data_address' => 'alamat cust #1 08/01' . $check_cust->row()->fullname
            );
            $dataPayment = array(
                'req_mall_id' => Doku_Library::mallId,
                'req_chain_merchant' => 'NA',
                'req_amount' => '10000.00',
                'req_words' => $words,
                'req_purchase_amount' => '10000.00',
                'req_trans_id_merchant' => $invoice_no,
                'req_request_date_time' => date('YmdHis'),
                'req_currency' => $params['currency'],
                'req_purchase_currency' => $params['currency'],
                'req_session_id' => sha1(date('YmdHis')),
                'req_name' => $customer['name'],
                'req_mobile_phone' => $customer['data_phone'],
                'req_work_phone' => $customer['data_phone'],
                'req_payment_channel' => 15,
                'req_basket' => $basket,
                'req_address' => $customer['data_address'],
                'req_email' => $customer['data_email'],
                'req_token_id' => $token
            );

            $result = Doku_Api::doPayment($dataPayment);
            $responses = array();
            if ($result->res_response_code == '0000') {
                $array_result = json_decode(json_encode($result), true);
                $id_doku = $this->insertBabooDoku($array_result);
                if (!empty($id_doku)) {
                    $this->db->update("transbaboo",array("mtrans_status"=>$result->res_response_msg),array("id_mtrans"=>$bodyPost->invoice_no));
                    $responses["code"] = (int)1000;
                    $responses["message"] = $result->res_response_msg;
                    $responses["data"] = $result;
                } else {
                    $responses["code"] = (int)2000;
                    $responses["message"] = $result->res_response_msg;
                    $responses["data"] = $result;
                }
                $this->echoResponse(REST_Controller::HTTP_OK, $responses);
            } else {
                $responses["code"] = (int)3000;
                $responses["message"] = $result->res_response_msg;
                $responses["data"] = $result;
                //    $responses["wordsGenerate"]      = $words;
                //    $responses["wordswithoutdevice"]      = $wordsss;
                //    $responses["wordswithdevice"]      = $wordsss2;
                //    $responses["totalamount"]      = $totalamount;
                //    $responses["basket"]      = $basket;
                $this->echoResponse(REST_Controller::HTTP_BAD_REQUEST, $responses);
            }
        } else {
            $responses["code"] = REST_Controller::HTTP_NOT_FOUND;
            $responses["message"] = "user not found";
            $responses["data"] = (object)array();
            $this->echoResponse(REST_Controller::HTTP_BAD_REQUEST, $responses);
        }
    }

    public function doMandiriClickPay_post()
    {
        $bodyPost = (object)$this->post();
        $baskets = $bodyPost->baskets;
        $totals = array();
        foreach($baskets as $baskey=>$basval){
            $baskets[$baskey]["quantity"] = "1";
            $subtotal = ($baskets[$baskey]["quantity"] * $basval["amount"]);
            $baskets[$baskey]["subtotal"] = $subtotal;
            $totals[] = $subtotal;
        }
        $totalamount =  number_format(array_sum($totals),2,".","");

        $params = array(
            'amount' => $totalamount,
            'invoice' => $bodyPost->invoice_no,
            'currency' => Doku_Library::currency
        );

        $cc = str_replace(" - ", "", $bodyPost->cc_number);
        $words = Doku_Library::doCreateWords($params);
        $cust_id = $bodyPost->cust_id;
        $check_cust = $this->db->get_where("users",array("user_id"=>$cust_id));
        if($check_cust->num_rows() > 0){
            $insid = array();
            foreach($baskets as $baskey=>$basval){
                $totals[] = $basval["subtotal"];
                $data_insertrans = array(
                    "id_mtrans" => $bodyPost->invoice_no,
                    "mtrans_cust" => $cust_id,
                    "mtrans_book" => $basval["book_id"],
                    "mtrans_price" => $basval["amount"],
                    "mtrans_type" => "mandiri clickpay"
                );
                $this->db->insert("transbaboo",$data_insertrans);
                $insid[] = $this->db->insert_id();
            }
            if(empty($insid)){
                $responses = array(
                    "code"=>REST_Controller::HTTP_BAD_REQUEST,
                    "message"=>"data not inserted to trans table",
                    "data"=>(object)array()
                );
                $this->echoResponse(REST_Controller::HTTP_BAD_REQUEST, $responses);
            }

            /*$customer = array(
                'name' => $check_cust->row()->fullname,
                'data_phone' => '0812158123',
                'data_email' => $check_cust->row()->email,
                'data_address' => 'alamat cust #1 08/01'.$check_cust->row()->fullname
            );*/
            $customer = array(
                'name' => $check_cust->row()->fullname,
                'data_phone' => '0812158123',
                'data_email' => $check_cust->row()->email,
                'data_address' => 'alamat cust #1 08/01' . $check_cust->row()->fullname
            );

            $dataPayment = array(
                'req_mall_id' => Doku_Library::mallId,
                'req_chain_merchant' => Doku_Library::chainMerchant,
                'req_amount' => $params['amount'],
                'req_words' => $words,
                'req_purchase_amount' => $params['amount'],
                'req_trans_id_merchant' => $params['invoice'],
                'req_request_date_time' => date('YmdHis'),
                'req_currency' => $params['currency'],
                'req_purchase_currency' => $params['currency'],
                'req_session_id' => sha1(date('YmdHis')),
                'req_name' => $customer['name'],
                'req_payment_channel' => 02,
                'req_email' => $customer['data_email'],
                'req_mobile_phone' => $customer['data_phone'],
                'req_work_phone' => $customer['data_phone'],
                'req_card_number' => $cc,
                'req_basket' => $baskets,
                'req_challenge_code_1' => $bodyPost->challenge_code_1,
                'req_challenge_code_2' => $bodyPost->challenge_code_2,
                'req_challenge_code_3' => $bodyPost->challenge_code_3,
                'req_response_token' => $bodyPost->response_token,
                'req_address' => $customer['data_address']
            );

            $response = Doku_Api::doDirectPayment($dataPayment);

            if ($response->res_response_code == '0000') {
                $array_result = json_decode(json_encode($response),true);
                $id_doku = $this->insertBabooDoku($array_result);
                if(!empty($id_doku)){
                    $this->db->update("transbaboo",array("mtrans_status"=>$response->res_response_msg),array("id_mtrans"=>$bodyPost->invoice_no));
                    $responses["code"]      = (int)1000;
                    $responses["message"]   = $response->res_response_msg;
                    $responses["data"]      = $response;
                }else{
                    $responses["code"]      = (int)2000;
                    $responses["message"]   = $response->res_response_msg;
                    $responses["data"]      = $response;

                }
                $this->echoResponse(REST_Controller::HTTP_OK, $responses);
            }else{
                $responses["code"]      = (int)3000;
                $responses["message"]   = $response->res_response_msg;
                $responses["data"]      = $response;
                $this->echoResponse(REST_Controller::HTTP_BAD_REQUEST, $responses);
            }
        }
    }

    public function doDirectPay()
    {
        $bodyPost = (object)$this->post();

        $token = $bodyPost->doku_token;
        $device_id = $bodyPost->device_id;
        $pairing_code = $bodyPost->doku_pairing_code;
        $invoice_no = $bodyPost->doku_invoice_no;

        $params = array(
            'amount' => '10000.00',
            'invoice' => $invoice_no,
            'currency' => '360',
            'pairing_code' => $pairing_code,
            'token' => $token,
            'device_id' => $device_id
        );

        $words = Doku_Library::doCreateWords($params);

        $basket[] = array(
            'name' => 'sayur',
            'amount' => '10000.00',
            'quantity' => '1',
            'subtotal' => '10000.00'
        );

        $customer = array(
            'name' => 'TEST NAME',
            'data_phone' => '08121111111',
            'data_email' => 'test@test.com',
            'data_address' => 'bojong gede #1 08/01'
        );

        $dataPayment = array(
            'req_mall_id' => Doku_Library::mallId,
            'req_chain_merchant' => 'NA',
            'req_amount' => '10000.00',
            'req_words' => $words,
            'req_purchase_amount' => '10000.00',
            'req_trans_id_merchant' => $invoice_no,
            'req_request_date_time' => date('YmdHis'),
            'req_currency' => '360',
            'req_purchase_currency' => '360',
            'req_session_id' => sha1(date('YmdHis')),
            'req_name' => $customer['name'],
            'req_payment_channel' => 15,
            'req_basket' => $basket,
            'req_address' => $customer['data_address'],
            'req_email' => $customer['data_email'],
            'req_token_id' => $token
        );

        $result = Doku_Api::doDirectPayment($dataPayment);
        $responses = array();
        if ($result->res_response_code == '0000') {
            $response = $result;
            $array_result = json_decode(json_encode($response),true);
            $id_doku = $this->insertBabooDoku($array_result);
            if(!empty($id_doku)){
                $responses["code"]      = (int)1000;
                $responses["message"]   = $response->res_response_msg;
                $responses["data"]      = $response;
            }else{
                $responses["code"]      = (int)2000;
                $responses["message"]   = $response->res_response_msg;
                $responses["data"]      = $response;
            }
            $this->echoResponse(REST_Controller::HTTP_OK, $responses);
        } else {
            $responses["code"]      = (int)3000;
            $responses["message"]   = $result->res_response_msg;
            $responses["data"]      = $result;
            $this->echoResponse(REST_Controller::HTTP_BAD_REQUEST, $responses);
        }
    }

    public function checkStatus_post()
    {
        $bodyPost = (object)$this->post();
        $trans_id_merchant = $bodyPost->trans_id_merchant;
        $params = array(
            'trans_id_merchant' => $trans_id_merchant,
            'currency' => Doku_Library::currency
        );

        $dataPayment = array(
            'MALLID' => Doku_Library::mallId,
            'CHAINMERCHANT' => Doku_Library::chainMerchant,
            'SESSIONID' => sha1(date('YmdHis')),
            'TRANSIDMERCHANT' => $params['trans_id_merchant'],
            'WORDS' => Doku_Library::doCreateWords($params)
        );

        $response = Doku_Api::checkStatus($dataPayment);
        var_dump($response);

        //
        //    if ($response->res_response_code == '0000') {
        //        $this->echoResponse(REST_Controller::HTTP_OK, $response);
        //    } else {
        //        $this->echoResponse(REST_Controller::HTTP_BAD_REQUEST, $response);
        //    }
    }

    private function insertBabooDoku($data_doku){
        $resps_id = 0;
        if(!empty($data_doku) && is_array($data_doku)){
            $data_insert = array();
            if(isset($data_doku["res_trans_id_merchant"])) $data_insert["transidmerchant"] = $data_doku["res_trans_id_merchant"];
            if(isset($data_doku["res_mcn"])) $data_insert["creditcard"] = $data_doku["res_mcn"];
            if(isset($data_doku["res_approval_code"])) $data_insert["approvalcode"]= $data_doku["res_approval_code"];
            if(isset($data_doku["res_amount"])) $data_insert["totalamount"] = $data_doku["res_amount"];
            if(isset($data_doku["res_response_code"])) $data_insert["response_code"] = $data_doku["res_response_code"];
            if(isset($data_doku["res_response_msg"])) $data_insert["trxstatus"] = $data_doku["res_response_msg"];
            if(isset($data_doku["res_bank"])) $data_insert["bank_issuer"] = $data_doku["res_bank"];
            if(isset($data_doku["res_session_id"])) $data_insert["session_id"] = $data_doku["res_session_id"];
            if(isset($data_doku["res_payment_channel"])) $data_insert["payment_channel"] = $data_doku["res_payment_channel"];
            if(isset($data_doku["res_payment_date_time"])){
                $datetime = strtotime($data_doku["res_payment_date_time"]);
                $data_insert["payment_date_time"] = date("Y-m-d H:i:s", $datetime);
            }
            if(isset($data_doku["res_payment_date"])){
                $datetime = strtotime($data_doku["res_payment_date"]);
                $data_insert["payment_date_time"] = date("Y-m-d H:i:s", $datetime);
            }
            if(isset($data_doku["res_words"])) $data_insert["words"] = $data_doku["res_words"];
            if(isset($data_doku["res_verify_status"])) $data_insert["verifystatus"] = $data_doku["res_verify_status"];
            if(isset($data_doku["res_verify_score"])) $data_insert["verifyscore"] = $data_doku["res_verify_score"];
            if(isset($data_doku["res_verify_id"])) $data_insert["verifyid"] = $data_doku["res_verify_id"];
            if(isset($data_doku["res_mid"])) $data_insert["mid_number"] = $data_doku["res_mid"];
            if(isset($data_doku["res_edu_status"])) $data_insert["edu_status"] = $data_doku["res_edu_status"];
            $ins = $this->db->insert("doku", $data_insert);
            if($ins) $resps_id = $this->db->insert_id();
        }
        return $resps_id;
    }

    private function _generate_key()
    {
        do {
            // Generate a random salt
            $salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);
            // base_convert(
            // If an error occurred, then fall back to the previous method
            if ($salt === FALSE) {
                $salt = hash('sha256', time() . mt_rand());
            }

            $new_key = substr($salt, 0, config_item('rest_key_length'));
        } while ($this->_key_exists($new_key));

        return $new_key;
    }

    private function _key_exists($key, $uid = '')
    {
        return $this->rest->db
                ->where(config_item('rest_key_column'), $key)
                ->count_all_results(config_item('rest_keys_table')) > 0;
    }

    private function echoResponse($status_code, $response)
    {
        $headers = array();
        foreach (getallheaders() as $name => $value) {
            $name = strtolower($name);
            $headers[$name] = $value;
        }
        $key = $headers["zal-auth-key"];
        $new_key = $this->regenerateKey($key, '', '');
        if (empty($new_key)) {
            $this->set_response(array("code" => REST_Controller::HTTP_FORBIDDEN, "message" => "Invalid API Key"), REST_Controller::HTTP_FORBIDDEN);
        }

        $this->set_response($response, $status_code, $new_key);
    }

    private function regenerateKey($key, $uid = '', $controller_method = '')
    {
        $old_key = $key;
        $key_details = $this->_get_key($old_key);

        // Does this key exist?
        if (!$key_details) {
            // It doesn't appear the key exists
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'Invalid API key',
                'data' => ''
            ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Build a new key
        $new_key = $this->_generate_key();

        // Insert the new key
        if ($this->_insert_key($new_key, ['level' => $key_details->level, 'ignore_limits' => $key_details->ignore_limits])) {
            // delete old key
            $cdkey = $this->db->get_where("api_access", "key = '".$old_key."' AND DATE_FORMAT(NOW(), '%Y-%m-%d %h:%i:%s') >= (date_created + INTERVAL 10 MINUTE)");
            if ($cdkey->num_rows() > 0){
                $this->_delete_key($old_key);
                $this->db->delete("api_access", array("key" => $key));
            }
            $data_detail = array(
                "key" => $new_key,
                "all_access" => 1,
                "controller" => $controller_method,
                "user_id" => $uid,
                "date_created" => date("Y-m-d h:i:s")
            );

            $this->db->insert("api_access", $data_detail);
        } else {
            $this->response([
                'code' => 500,
                'message' => 'Could not save the key',
                'data' => ''
            ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR); // INTERNAL_SERVER_ERROR (500) being the HTTP response code
        }
        return $new_key;
    }

    private function _get_key($key, $uid = '')
    {
        return $this->rest->db
            ->where(config_item('rest_key_column'), $key)
            ->get(config_item('rest_keys_table'))
            ->row();
    }


    private function _insert_key($key, $data)
    {
        $data[config_item('rest_key_column')] = $key;
        $data['date_created'] = function_exists('now') ? now() : time();

        return $this->rest->db
            ->set($data)
            ->insert(config_item('rest_keys_table'));
    }


    private function _delete_key($key)
    {
        return $this->rest->db
            ->where(config_item('rest_key_column'), $key)
            ->delete(config_item('rest_keys_table'));
    }

}