<?php 

require APPPATH . 'libraries/REST_Controller.php';

class Midtrans extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper("baboo");
    }
    public function charge(){
        $server_key = key_trans();
		$api_url = base_trans();

        $request_method = $_SERVER['REQUEST_METHOD'];
        $request_body = file_get_contents('php://input');
        switch ($request_method) {
            case 'POST':
                http_response_code(200);
                echo $this->chargeAPI($api_url, $server_key, $request_body);
                break;
            case 'GET':
                http_response_code(404);
                echo "Page not found or wrong HTTP request method is used";
                exit();
                break;
            default:
                http_response_code(404);
                echo "Page not found or wrong HTTP request method is used";
                exit();
                break;
        }
    }
    private function chargeAPI($api_url, $server_key, $request_body)
    {
        $ch = curl_init();
        $curl_options = array(
            CURLOPT_URL => $api_url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Basic ' . base64_encode($server_key . ':')
            ),
            CURLOPT_POSTFIELDS => $request_body
        );
        curl_setopt_array($ch, $curl_options);
        $result = curl_exec($ch);
        return $result;
    }
}