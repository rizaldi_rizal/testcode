<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Users extends REST_Controller
{
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper("baboo");
    }

    public function blockUser_post()
    {
        $keys = sethead();
        $user_request = (!empty($keys)) ? getUserFromKey($keys) : 0;
        $user_id = $this->post("user_id");
        if (!empty($user_request) && $user_request == 1) {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "data request not found";
            if (!empty($user_id)) {
                $this->db->where("user_id", $user_id);
                $this->db->select("fullname, IFNULL(prof_pict, '') as prof_pict");
                $get_user = $this->db->get("users");
                if ($get_user->num_rows() > 0) {
                    $upd = $this->db->update("users", array("is_active" => 1), array("user_id" => $user_id));
                    if ($upd) {
                        $resp_code = REST_Controller::HTTP_OK;
                        $http_response_header = REST_Controller::HTTP_OK;
                        $resp_message = "user blocked";
                    } else {
                        $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                        $http_response_header = REST_Controller::HTTP_ACCEPTED;
                        $resp_message = "something went wrong..";
                    }
                } else {
                    $resp_message = "user not found";
                    $http_response_header = REST_Controller::HTTP_ACCEPTED;
                }
            }
        } else {
            $resp_code = REST_Controller::HTTP_FORBIDDEN;
            $http_response_header = REST_Controller::HTTP_FORBIDDEN;
            $resp_message = "only admin can access this feature";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message
        );
        set_resp($responses, $http_response_header);
    }

    public function reportUser_post()
    {
        $keys = sethead();
        $user_request = (!empty($keys)) ? getUserFromKey($keys) : 0;
        $user_id = $this->post("user_id");
        $resp_code = REST_Controller::HTTP_NOT_FOUND;
        $resp_message = "data request not found";
        if (!empty($user_id) && !empty($user_request)) {
            $this->db->where(array("report_by" => $user_request, "user_reported" => $user_id));
            $this->db->select("user_reported");
            $cekuser = $this->db->get("user_report");
            $this->db->where("user_id", $user_id);
            $this->db->select("fullname, IFNULL(prof_pict, '') as prof_pict");
            $get_user = $this->db->get("users");
            if ($get_user->num_rows() > 0 && $cekuser->num_rows() == 0) {
                $data_insert = array(
                    "user_reported" => $user_id,
                    "report_by" => $user_request
                );
                if (!empty($this->post("report_reason"))) $data_insert["report_reason"] = $this->post("report_reason");
                $ins = $this->db->insert("user_report", $data_insert);
                if ($ins) {
                    $resp_code = REST_Controller::HTTP_OK;
                    $http_response_header = REST_Controller::HTTP_OK;
                    $resp_message = "user reported, we will checking through this users data and block it if violated our rule";
                } else {
                    $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                    $http_response_header = REST_Controller::HTTP_ACCEPTED;
                    $resp_message = "something went wrong.. " . implode(",", $this->db->error());
                }
            } else {
                $resp_message = "user not found or you already report this person";
                $http_response_header = REST_Controller::HTTP_ACCEPTED;
            }
        }

        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message
        );
        set_resp($responses, $http_response_header);
    }

    public function allUsers_get()
    {
        $keys = sethead();
        $user_id = (!empty($keys)) ? getUserFromKey($keys) : 0;
        $this->db->where("user_id !=", $user_id);
        $this->db->where("is_active", 0);
        $this->db->select("user_id, replace(fullname , ' ','') as fullname , IFNULL(prof_pict, '') as prof_pict");
        $get_users = $this->db->get("users");
        $arr_users = array();
        $resp_code = REST_Controller::HTTP_OK;
        $resp_message = "user not found";
        if ($get_users->num_rows() > 0) {
            $resp_message = "users found!";
            $arr_users = $get_users->result();
        }
        $respnses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $arr_users
        );
        set_resp($respnses, REST_Controller::HTTP_OK);
    }
}