<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class OAuth extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper("baboo");
        $this->load->model("User", "user");

        $abase = base_url();

        $this->load->library("CloudStorage", NULL, "s3");

        $this->bucket = REST_Controller::bucket_staging;
        if (strpos($abase, REST_Controller::api_prod) !== false) {
            $this->bucket = REST_Controller::bucket_prod;
        }
        //check if dev
        if (strpos($abase, REST_Controller::api_dev) !== false || strpos($abase, REST_Controller::api_local) !== false) {
            $this->ParentUploadir = "../../uploads/";
            $this->Uploadir = $this->ParentUploadir . "baboo-avatar/";
            $this->DirAssets = REST_Controller::assets_dev . "baboo-avatar/";
            if (!file_exists($this->Uploadir)) {
                mkdir($this->Uploadir, 0777);
            }
        }
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['usersfb_get']['limit'] = 100; // 500 requests per hour per user/key
        $this->methods['users_get']['limit'] = 100; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_put']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function profile_post()
    {
        $resp = array();
        $data_resp = array();
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $mail = $this->post("email");
        $oauth_uid = $this->post("oauth_uid");
        $role = $this->post("role");
        $limit = $this->post("limit");

        $user = NULL;
        if (!empty($mail) || !empty($user_id) || !empty($oauth_uid) || !empty($role) || !empty($limit)) {
            if (!empty($limit)) {
                $this->db->limit($limit, 0);
            } else if (!empty($user_id)) {
                $this->db->where("user_id", $user_id);
            } else if (!empty($mail)) {
                $this->db->where("email", $mail);
            } else if (!empty($oauth_uid)) {
                $this->db->where("oauth_uid", $oauth_uid);
            } else if (!empty($role)) {
                $this->db->where("role", $role);
            }
            $this->db->select("user_id, fullname, email, IFNULL(date_of_birth, '') as date_of_birth,  IFNULL(jk, '') as jk, IFNULL(prof_pict, '') as prof_pict, IFNULL(address, '') as address,  IFNULL(about_me, '') as about_me , IFNULL(phone, '') as phone_number, balance, book_sold");
            $user = $this->db->get("users");
            if ($user->num_rows() > 0) {
                $resp["code"] = REST_Controller::HTTP_OK;
                $resp["message"] = "";
                $data_resp = $user->row_array();
                $data_resp["balance"] = $user->row()->balance;
                $data_resp["book_sold"] = $user->row()->book_sold;
                $data_resp["is_newuser"] = false;
                $data_resp["isFollow"] = false;
                //cek has pin
                $has_pin = false;
//                $user_pin = $this->db->get_where("user_pin", array("id_user" => $user->row()->user_id));
                $activated = false;
                $user_pin = $this->db->query("SELECT id_pin FROM user_pin WHERE id_user = ? AND no_pin IS NOT NULL", array($user->row()->user_id));
                if($user_pin && $user_pin->num_rows() > 0) $has_pin = true;
                $data_resp["has_pin"] = $has_pin;
                $uss_pin = $this->db->query("SELECT id_pin FROM user_pin WHERE id_user = ? AND question1 IS NOT NULL AND question2 IS NOT NULL AND answer1 IS NOT NULL AND answer2 IS NOT NULL", array($user->row()->user_id));
                if($uss_pin && $uss_pin->num_rows() > 0) $activated = true;
                $data_resp["is_activated"] = $activated;
                //count followers
                $followers = $this->db->get_where("follows", array("is_follow" => $user->row()->user_id))->num_rows();
                $data_resp["followers"] = $followers;
                //count book made
                $book_made = $this->db->get_where("book", array("author_id" => $user->row()->user_id, "status_publish !=" => "draft"))->num_rows();
                $data_resp["book_made"] = $book_made;
                //like book
                $this->db->where("author_id", $user->row()->user_id);
                $this->db->join("likes", "likes.id_book = book.book_id");
                $ppl_like = $this->db->get("book")->num_rows();
                $data_resp["ppl_like"] = $ppl_like;

                $resp["data"] = $data_resp;
                set_resp($resp, REST_Controller::HTTP_OK);
//                $this->set_response($resp, REST_Controller::HTTP_OK, $new_key); // OK (200) being the HTTP response code
            } else {
                // $lq = $this->db->last_query();
                $this->set_response([
                    'code' => REST_Controller::HTTP_NOT_FOUND,
                    'message' => 'User could not be found',
                    'data' => (object)array()
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->set_response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'minimum request is the id or user gids',
                'data' => (object)array()
            ], REST_Controller::HTTP_OK);
        }

    }

    public function otherProfile_post()
    {
        $resp = array();
        // Insert the new key if not exist and update it
        // If no key level provided, give them a rubbish one
        $mail = $this->post("email");
        $key = sethead();
        $user_id = getUserFromKey($key);
        $user_profile = $this->post("user_profile");
        $oauth_uid = $this->post("oauth_uid");
        $role = $this->post("role");
        $limit = $this->post("limit");
        $user = NULL;
        if (!empty($mail) || !empty($user_profile) || !empty($oauth_uid) || !empty($role) || !empty($limit)) {
            if (!empty($limit)) {
                $this->db->limit($limit, 0);
            } else if (!empty($user_id)) {
                $this->db->where("user_id", $user_profile);
            } else if (!empty($mail)) {
                $this->db->where("email", $mail);
            } else if (!empty($oauth_uid)) {
                $this->db->where("oauth_uid", $oauth_uid);
            } else if (!empty($role)) {
                $this->db->where("role", $role);
            }
            $this->db->select("user_id, fullname, email, IFNULL(date_of_birth, '') as date_of_birth,  IFNULL(jk, '') as jk, IFNULL(prof_pict, '') as prof_pict, IFNULL(address, '') as address,  IFNULL(about_me, '') as about_me, IFNULL(phone, '') as phone_number, book_sold");
            $user = $this->db->get("users");
            if ($user->num_rows() > 0) {
                $resp["code"] = REST_Controller::HTTP_OK;
                $resp["message"] = "look other profile success";
                $data_resp = $user->row_array();
                $data_resp["is_newuser"] = false;
                //count followers
                $data_resp["has_pin"] = false;
                $data_resp["is_activated"] = false;
                $followers = $this->db->get_where("follows", array("is_follow" => $user->row()->user_id))->num_rows();
                $data_resp["followers"] = $followers;
                $is_follow = false;
                $getfollows = $this->db->get_where("follows", array("is_follow" => $user->row()->user_id, "created_by" => $user_id));
                if ($getfollows->num_rows() > 0) {
                    $is_follow = true;
                }
                $data_resp["isFollow"] = $is_follow;
                //count book made
                $book_made = $this->db->get_where("book", array("author_id" => $user->row()->user_id, "status_publish !=" => "draft"))->num_rows();
                $data_resp["book_made"] = $book_made;
                //like book
                $this->db->where("author_id", $user->row()->user_id);
                $this->db->join("likes", "likes.id_book = book.book_id");
                $ppl_like = $this->db->get("book")->num_rows();
                $data_resp["ppl_like"] = $ppl_like;
                //resp book published
                $resp_data = array();
                $limit = REST_Controller::limit_timeline;
                $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;
                $this->db->limit(REST_Controller::limit_timeline, $count);
                $this->db->order_by("book.book_id", "desc");
                $this->db->where("book.status_publish !=", "draft");
                $this->db->where("book.author_id", $user->row()->user_id);
                $this->db->join("users", "users.user_id = book.author_id");
                $books = $this->db->get("book");
                // Check if the books data store contains book id(in case the database result returns NULL)
                if ($books->num_rows() > 0) {
                    $limit_text = REST_Controller::text_limits;
                    foreach ($books->result() as $bokeys => $boval) {
                        $bokeks = $bokeys + 1;
                        if($boval->is_pdf == 1) $res_par_text = getParagraphByBook($boval->book_id);
                        else $res_par_text = substr((string)$boval->descriptions, 0, $limit_text) . "...";
                        if (empty($user_id)) $user_id = $user_profile;
                        $is_likes = checkIsLike($boval->book_id, $user_id);
                        $is_bookmark = checkIsBookmark($boval->book_id, $user_id);
                        if (!empty($boval->title_book)) {
                            $category = getCategoryBook($boval->category_id);
                            $publish_date = time_ago($boval->publish_at);
                            $is_download = false;
                            $this->db->select("is_download");
                            $cekBought = $this->db->get_where("collections", array("id_user" => $user_id, "id_book" => $boval->book_id));
                            if ($cekBought->num_rows() > 0) {
                                if (isset($cekBought->row()->is_download) && $cekBought->row()->is_download == 1) $is_download = true;
                            }
                            $latest_update = $publish_date;
                            if (!empty($boval->latest_update) && $boval->latest_update != $boval->publish_at && $boval->status_publish == "publish") $latest_update = time_ago($boval->latest_update);
                            $getComment = $this->db->get_where("comment", array("id_book" => $boval->book_id));
                            $dasray = array(
                                "book_id" => $boval->book_id,
                                "author_id" => $boval->author_id,
                                "author_name" => $boval->fullname,
                                "author_avatar" => (string)$boval->prof_pict,
                                "cover_url" => (string)$boval->file_cover,
                                "image_url" => (string)$boval->image_url,
                                "title_book" => $boval->title_book,
                                "category" => $category,
                                "is_like" => $is_likes,
                                "is_bookmark" => $is_bookmark,
                                "is_download" => $is_download,
                                "is_pdf" => (!empty($boval->is_pdf)) ? false : true,
                                "book_type" => (int)$boval->book_type,
                                "publish_date" => $publish_date,
                                "latest_update" => $latest_update,
                                "like_count" => (int)$boval->like_count,
                                "comment_count" => $getComment->num_rows(),
                                "view_count" => (int)$boval->view_count,
                                "share_count" => (int)$boval->share_count,
                                "desc" => trim($res_par_text)
                            );
                            $resp_data[] = $dasray;
                        }
                    }
                }
                $resp["data"]["user_info"] = $data_resp;
                $resp["data"]["book_published"] = $resp_data;
                set_resp($resp, REST_Controller::HTTP_OK);
            } else {
                $this->set_response([
                    'code' => REST_Controller::HTTP_NOT_FOUND,
                    'message' => 'User could not be found',
                    'data' => (object)array()
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->set_response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'minimum request is the id or user gids',
                'data' => (object)array()
            ], REST_Controller::HTTP_OK);
        }
    }

    public function logout_post()
    {
        $key = sethead();
        $id = getUserFromKey($key);
        $device_id = $this->post("device_id");
        if (empty($id) || empty($device_id)) {
            $response = array("code" => REST_Controller::HTTP_BAD_REQUEST, "message" => "User cannot be empty");
            // Invalid id, set the response and exit.
            $this->set_response($response, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code
        } else {
            //set status response for success logout or failed based on request
            $headers = array();
            foreach (getallheaders() as $name => $value) {
                $name = strtolower($name);
                $headers[$name] = $value;
            }
            $key = isset($headers["zal-auth-key"]) ? $headers["zal-auth-key"] : "";
            $response = $this->user->delete_device($this->post());
            if ($response["code"] == REST_Controller::HTTP_OK) {
                $upd = $this->db->update("users", array("pre_hash" => NULL, "current_hash" => NULL), array("user_id" => $id));
                if ($upd) $this->set_response($response, REST_Controller::HTTP_OK);
                else $this->set_response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            } else {
                $this->set_response($response, REST_Controller::HTTP_OK, $key);
            }
        }
    }

    public function questionSecure_get()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $resp_code = REST_Controller::HTTP_OK;
        $resp_message = "";
        $resp_data = array();
        $this->db->limit(5, 0);
        $this->db->select("question_id, question_text");
        $qg = $this->db->get("question_secure");
        if ($qg->num_rows() > 0) {
            $resp_data = $qg->result();
        } else {
            $resp_message .= "question not exist";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($responses, REST_Controller::HTTP_OK);
    }

    public function listQuestionForgot_get()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_code = REST_Controller::HTTP_OK;
        $resp_message = "";
        $resp_data = array();
        $question_1 = 0;
        $question_2 = 0;
        $this->db->where("id_user", $user_id);
        $usrpin = $this->db->get("user_pin");
        if ($usrpin && $usrpin->num_rows() > 0) {
            $question_1 = $usrpin->row()->question1;
            $question_2 = $usrpin->row()->question2;
        }
        //checking the data
        $qg = $this->db->query("SELECT question_id, question_text FROM question_secure WHERE question_id = $question_2 OR question_id = $question_1 ORDER BY modified ASC LIMIT 1");
        if ($qg->num_rows() > 0) {
            $resp_data = $qg->row();
            $this->db->update("question_secure", array("modified" => date("Y-m-d H:i:s")), array("question_id" => $resp_data->question_id));
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message .= "question not exist";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$resp_data
        );
        set_resp($responses, REST_Controller::HTTP_OK);
    }

    public function login_post()
    {
        $resp = array();
        $phone_number = $this->post("phone_number");
        $username = $this->post("username");
        $password = $this->post("password");
        if (empty($username) || empty($this->post("password"))) {
            $this->set_response(array("code" => REST_Controller::HTTP_FORBIDDEN, "message" => "username or password cannot be empty", "data" => (object)array()), REST_Controller::HTTP_FORBIDDEN);
        }
        $this->db->select("user_id, password, fullname, prof_pict, email, oauth_uid, oauth_provider, phone");
        $this->db->where(array("username" => $username, "is_active" => 0));
        // $this->db->where(array("password" => $password);
        // $this->db->where(array("phone" => $phone_number, "is_active" => 0));
        $cekada = $this->db->get("users");
        if ($cekada->num_rows() > 0) {
            $phone = $cekada->row()->phone;
            $passdb = password_decrypt($cekada->row()->password);
            $uid = $cekada->row()->user_id;
            if ($passdb == $password) {
                if (!empty($this->post("device_id")) && !empty($this->post("fcm_id"))) {
                    $data_insert = array(
                        "device_id" => $this->post("device_id"),
                        "fcm_id" => $this->post("fcm_id")
                    );
                    $this->db->select("device_id");
                    $cekDuid = $this->db->get_where("device", array("uuid"=>$uid, "device_id"=>$this->post("device_id")));
                    if($cekDuid && $cekDuid->num_rows() > 0) $insd = $this->db->update("device", $data_insert, array("uuid"=>$uid, "device_id"=>$this->post("device_id")));
                    else{
                        $data_insert["uuid"] = $uid;
                        $insd = $this->db->insert("device", $data_insert);
                    }
                    // checking
                }
                $resp["code"] = REST_Controller::HTTP_OK;
                $resp["message"] = "login data";
                $this->db->select("user_id, role_id, email, fullname, IFNULL(date_of_birth, '') as date_of_birth, IFNULL(prof_pict, '') as prof_pict, email, oauth_uid, IFNULL(jk, '') as jk, IFNULL(oauth_provider, '') as oauth_provider,  IFNULL(address, '') as address, IFNULL(about_me, '') as about_me,  balance, book_sold, phone");
                $this->db->from("users");
                $this->db->where(array("user_id" => $uid));
                $getada = $this->db->get();
                $data_resp = array();
                if ($getada->num_rows() > 0) {
                    $data_resp["user_id"] = $getada->row()->user_id;
                    $data_resp["role_id"] = $getada->row()->role_id;
                    $data_resp["email"] = $getada->row()->email;
                    $data_resp["date_of_birth"] = $getada->row()->date_of_birth;
                    $data_resp["fullname"] = $getada->row()->fullname;
                    $data_resp["jk"] = (string)$getada->row()->jk;
                    $data_resp["prof_pict"] = (string)$getada->row()->prof_pict;
                    $data_resp["oauth_uid"] = (string)$getada->row()->oauth_uid;
                    $data_resp["address"] = (string)$getada->row()->address;
                    $data_resp["about_me"] = (string)$getada->row()->about_me;
                    $data_resp["phone_number"] = (string)$getada->row()->phone;
                    $data_resp["oauth_provider"] = (string)$getada->row()->oauth_provider;
                    $data_resp["book_sold"] = $getada->row()->book_sold;
                    $data_resp["balance"] = $getada->row()->balance;
                    $data_resp["is_newuser"] = false;
                    $data_resp["isFollow"] = false;
                    //count followers
                    // // $has_pin = false;
                    // // $activated = false;
                    // // $user_pin = $this->db->query("SELECT id_pin FROM user_pin WHERE id_user = ? AND no_pin IS NOT NULL", array($getada->row()->user_id));
                    // // if($user_pin && $user_pin->num_rows() > 0) $has_pin = true;
                    // // $data_resp["has_pin"] = $has_pin;
                    // // $uss_pin = $this->db->query("SELECT id_pin FROM user_pin WHERE id_user = ? AND question1 IS NOT NULL AND question2 IS NOT NULL AND answer1 IS NOT NULL AND answer2 IS NOT NULL", array($getada->row()->user_id));
                    // // if($uss_pin && $uss_pin->num_rows() > 0) $activated = true;
                    // // $data_resp["is_activated"] = $activated;
                    // // $followers = $this->db->get_where("follows", array("is_follow" => $getada->row()->user_id))->num_rows();
                    // // // $data_resp["followers"] = $followers;
                    // // //count book made
                    // // $book_made = $this->db->get_where("book", array("author_id" => $getada->row()->user_id, "status_publish !=" => "draft"))->num_rows();
                    // // $data_resp["book_made"] = $book_made;
                    // // //like book
                    // // $this->db->where("author_id", $getada->row()->user_id);
                    // // $this->db->join("likes", "likes.id_book = book.book_id");
                    // // $ppl_like = $this->db->get("book")->num_rows();
                    // // $data_resp["ppl_like"] = $ppl_like;

                    // $otp = randoms_otp();
                    // if (!empty((string)$getada->row()->phone)) {
                    //     logOTP('08990091860', $otp);
                    // }
                }
                $resp["data"] = (object)$data_resp;
//                $this->response($resp, REST_Controller::HTTP_OK, TRUE, $new_key);
                new_resp($resp, REST_Controller::HTTP_OK, $uid);

                // $this->set_response($resp, REST_Controller::HTTP_OK);
            } else {
                $resp["code"] = REST_Controller::HTTP_BAD_REQUEST;
                $resp["message"] = "wrong phone number";
                $resp["data"] = (object)array();
                $this->set_response($resp, REST_Controller::HTTP_OK);
            }
        } else {
            $this->set_response([
                'code' => REST_Controller::HTTP_NOT_FOUND,
                'message' => 'No users were found or user already blocked',
                'data' => (object)array()
            ], REST_Controller::HTTP_OK);
        }
    }

    public function listFollowers_post()
    {
        $key = sethead();
        $http_response_header = REST_Controller::HTTP_OK;
        $resp_code = REST_Controller::HTTP_NOT_FOUND;
        $resp_message = "no follower data";
        $user_id = (!empty($this->post("user_id"))) ? $this->post("user_id") : getUserFromKey($key);
        $followers = $this->db->get_where("follows", array("is_follow" => $user_id));
        $sarus = array();
        if ($followers->num_rows() > 0) {
            $resp_message = "show followers data success";
            foreach ($followers->result() as $folval) {
                $users = $this->db->get_where("users", array("user_id" => $folval->created_by));
                if ($users->num_rows() > 0) {
                    $resp_code = REST_Controller::HTTP_OK;
                    $sasrow = $users->row();
                    $is_follow = false;
                    $cekfollow = $this->db->get_where("follows", array("created_by" => $user_id, "is_follow" => $sasrow->user_id));
                    if ($cekfollow->num_rows() > 0) $is_follow = true;
                    $followers = $this->db->get_where("follows", array("is_follow" => $sasrow->user_id))->num_rows();
                    $book_made = $this->db->get_where("book", array("author_id" => $sasrow->user_id, "status_publish !=" => "draft"))->num_rows();
                    $arus["user_id"] = $sasrow->user_id;
                    $arus["role_id"] = $sasrow->role_id;
                    $arus["fullname"] = (string)$sasrow->fullname;
                    $arus["email"] = $sasrow->email;
                    $arus["prof_pict"] = (string)$sasrow->prof_pict;
                    $arus["date_of_birth"] = (string)$sasrow->date_of_birth;
                    $arus["jk"] = (string)$sasrow->jk;
                    $arus["address"] = (string)$sasrow->address;
                    $arus["about_me"] = (string)$sasrow->about_me;
                    $arus["oauth_uid"] = (string)$sasrow->oauth_uid;
                    $arus["phone_number"] = (string)$sasrow->phone;
                    $arus["oauth_provider"] = (string)$sasrow->oauth_provider;
                    $arus["followers"] = $followers;
                    $arus["book_sold"] = (int)$sasrow->book_sold;
                    $arus["book_made"] = $book_made;
                    $arus["balance"] = (int)$sasrow->balance;
                    $arus["is_newuser"] = false;
                    $arus["isFollow"] = $is_follow;
                    $arus["has_pin"] = false;
                    $arus["is_activated"] = false;
                    $sarus[] = $arus;
                }
            }
        }
        $resps["code"] = $resp_code;
        $resps["message"] = $resp_message;
        $resps["data"] = (array)$sarus;
        set_resp($resps, $http_response_header);
    }

    public function login_fb_post()
    {
        $key_resp = array();
        // Insert the new key
        $user_id = 0;
        $post = $this->post();
        if (!empty($post) && !empty($post["email"])) {
            if (!isset($post["oauth_provider"]) || empty($post["oauth_provider"])) $post["oauth_provider"] = "facebook";
            $response = $this->user->checkUserFB($post);
            if (!empty($response) && isset($response['data']->user_id)) {
                $user_id = $response['data']->user_id;
            }
            $key_resp = array_merge($key_resp, $response);
            // If no key level provided, give them a rubbish one
        } else {
            $key_resp["code"] = REST_Controller::HTTP_ACCEPTED;
        }
        new_resp($key_resp, REST_Controller::HTTP_OK, $user_id);
    }

    public function login_google_post()
    {
        $key_resp = array();
        // If no key level provided, give them a rubbish one
        $user_id = '';
        // Insert the new key
        $post = $this->post();
        if (!empty($post) && !empty($post["email"])) {
            if (!isset($post["oauth_provider"]) || empty($post["oauth_provider"])) $post["oauth_provider"] = "google";
            $response = $this->user->checkUserGoogle($post);
            if (!empty($response) && isset($response['data']->user_id)) {
                $user_id = $response['data']->user_id;
            }
            $key_resp = array_merge($key_resp, $response);
        } else {
            $key_resp["code"] = REST_Controller::HTTP_ACCEPTED;
            $key_resp["message"] = "empty request";
            $key_resp["data"] = (object)array();
        }
        new_resp($key_resp, REST_Controller::HTTP_OK, $user_id);
    }

    public function uploadProfpict_post()
    {
        $data = array();
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        if (!empty($user_id)) $data["user_id"] = $user_id;
        $a = base_url();
        if (strpos($a, REST_Controller::api_dev) !== false || strpos($a, REST_Controller::api_local) !== false) {
            $config['upload_path'] = $this->Uploadir;
            $config['allowed_types'] = 'bmp|jpg|jpeg|png';
            $config['max_size'] = 8000;
            $config["file_name"] = "user_" . time();
            //in case you want to add something like limited size to perfect your social media
            //$config['max_width']            = 1024;
            //$config['max_height']           = 768;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('prof_pict')) {
                $this->response(array('code' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'message' => strip_tags($this->upload->display_errors()), 'data' => (object)array()), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            } else {
                if (!empty($user_id)) {
                    $where_id = array("user_id" => $user_id);
                    $upload = $this->upload->data();
                    $cover_url = array();
                    $assetscover = $this->DirAssets . $upload["file_name"];
                    $cover_url["prof_pict"] = $assetscover;
                    $update = $this->db->update("users", $cover_url, $where_id);
                    if ($update) {
                        $http_response_header = REST_Controller::HTTP_OK;
                        $resps = array('code' => REST_Controller::HTTP_OK, 'message' => 'Upload Image Success', 'data' => (object)array("asset_url" => $cover_url["prof_pict"]));
                    } else {
                        $http_response_header = REST_Controller::HTTP_ACCEPTED;
                        $resps = array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "Error Input to DB Image", "data" => (object)array());
                    }
                } else {
                    $http_response_header = REST_Controller::HTTP_ACCEPTED;
                    $resps = array("code" => REST_Controller::HTTP_NOT_FOUND, "message" => "Not found any user that want to upload the image", "data" => (object)array());
                }
            }
        } else {
            if (!empty($_FILES) && $_FILES["prof_pict"]["error"] != 4) {
                $files = $_FILES["prof_pict"]["name"];
                $ext = $ext = pathinfo($files, PATHINFO_EXTENSION);
                $tmpName = $_FILES["prof_pict"]["tmp_name"];
                $fileName = "baboo-avatar/user_" . time() . ".$ext";
                //check if uploaded
                if ($this->s3->upload($fileName, $tmpName, $this->bucket)) {
                    $assetscover = $this->s3->url($fileName, $this->bucket);
                    if (!empty($user_id)) {
                        $where_id = array("user_id" => $user_id);
                        $cover_url["prof_pict"] = $assetscover;
                        $update = $this->db->update("users", $cover_url, $where_id);
                        if ($update) {
                            $http_response_header = REST_Controller::HTTP_OK;
                            $resps = array('code' => REST_Controller::HTTP_OK, 'message' => 'Upload Image Success', 'data' => (object)array("asset_url" => $cover_url["prof_pict"]));
                        } else {
                            $http_response_header = REST_Controller::HTTP_ACCEPTED;
                            $resps = array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "Error Input to DB Image", "data" => (object)array());
                        }
                    } else {
                        $http_response_header = REST_Controller::HTTP_ACCEPTED;
                        $resps = array("code" => REST_Controller::HTTP_NOT_FOUND, "message" => "Not found any user that want to upload the image", "data" => (object)array());
                    }

                } else {
                    $http_response_header = REST_Controller::HTTP_ACCEPTED;
                    $resps = array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "Error upload Image", "data" => (object)array());
                }
            } else {
                $http_response_header = REST_Controller::HTTP_ACCEPTED;
                $resps = array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "Error upload Image, file not exist", "data" => (object)array());
            }
        }
        set_resp($resps, $http_response_header);
    }

    public function register_post()
    {
        $data = array();
        if (empty($this->post("email")) || empty($this->post("fullname")) || empty($this->post("password"))) {
            $this->set_response(array("code" => REST_Controller::HTTP_FORBIDDEN, "message" => "email, name and password cannot be empty", "data" => (object)array()), REST_Controller::HTTP_FORBIDDEN);
        }
        $data["username"] = $this->post('username');
        $data["fullname"] = $this->post('fullname');
        $data["email"] = $this->post('email');
        $data["phone"] = $this->post('phone');
        $data["password"] = password_encrypt($this->post('password'));
        if (!empty($this->post("place_of_birth"))) $data["place_of_birth"] = $this->post('place_of_birth');
        if (!empty($this->post("date_of_birth"))) $data["date_of_birth"] = $this->post('date_of_birth');
        $data["role_id"] = 3;
        $data["jk"] = $this->post('jk');
        $data["is_active"] = 0;
        $data["oauth_provider"] = $this->post('oauth_provider');
        $data["oauth_uid"] = $this->post('oauth_uid');
        // $this->some_model->insert_user( ... );
        $user_id = 0;
        $resps = $this->user->insert_user($data);
        if (!empty($resps) && isset($resps["data"]->user_id)) {
            $user_id = $resps['data']->user_id;
        }

        $http_response_header = REST_Controller::HTTP_OK;
        $this->set_response($resps, $http_response_header);
        // new_resp($resps, REST_Controller::HTTP_CREATED, $user_id); // CREATED (201) being the HTTP response code
    }

    public function editProfile_post()
    {
        $data = array();
        $data_tambahan = null;
        $key = sethead();
        $user_id = getUserFromKey($key);

        if (!empty($this->post("username"))) $data["username"] = $this->post('username');
        if (!empty($this->post("fullname"))) $data["fullname"] = $this->post('fullname');
        //if (!empty($this->post("email"))) $data["email"] = $this->post('email');
        if (!empty($this->post("password"))) $data["password"] = MD5($this->post('password') . $this->config->item("encryption_key"));
        if (!empty($this->post("place_of_birth"))) $data["place_of_birth"] = $this->post('place_of_birth');
        if (!empty($this->post("address"))) $data["address"] = $this->post('address');
        if (!empty($this->post("date_of_birth"))) $data["date_of_birth"] = date("Y-m-d", strtotime($this->post('date_of_birth')));
//        if (!empty($this->post("role"))) $data["role"] = $this->post('role');
        if (!empty($this->post("about_me"))) $data["about_me"] = $this->post('about_me');
        if (!empty($this->post("bank_user"))) $data["bank_user"] = $this->post('bank_user');
        if (!empty($this->post("account_bank"))) $data["account_bank"] = $this->post('account_bank');
        if (!empty($this->post("jk"))) $data["jk"] = $this->post('jk');
        if (!empty($this->post("is_active"))) $data["is_active"] = $this->post('is_active');
        if (!empty($this->post("oauth_provider"))) $data["oauth_provider"] = $this->post('oauth_provider');
        if (!empty($this->post("oauth_uid"))) $data["oauth_uid"] = $this->post('oauth_uid');
        if (!empty($this->post("user_genre"))) {
            $user_genre = $this->post('user_genre');
            foreach ($user_genre as $ussval) {
                $data_tambahan[] = array(
                    "id_users" => $user_id,
                    "category_genre" => $ussval
                );
            }
        }
        $update["responses"] = array(
            "code" => REST_Controller::HTTP_FORBIDDEN,
            "message" => "Invalid API Key",
            "data" => (object)array()
        );
        //check if user_id from key generated
        if (!empty($user_id)) $update = $this->user->update_user($data, $user_id, $data_tambahan);
        //checking updated
        if (!empty($update) && is_array($update) && isset($update["http_response_code"])) {
            set_resp($update["responses"], $update["http_response_code"]);
        } else {
            set_resp($update["responses"], REST_Controller::HTTP_ACCEPTED);
        }
    }

    public function confirmAccount_post()
    {
        $data = array();
        $data_user = null;
        $key = sethead();
        $user_id = getUserFromKey($key);

        if (!empty($this->post("ktp_no"))) $data_user["ktp_no"] = $this->post('ktp_no');
        if (!empty($this->post("fullname"))) $data_user["ktp_name"] = $this->post('fullname');
        if (!empty($this->post("ktp_image"))) $data_user["ktp_image"] = $this->post('ktp_image');
        if (!empty($this->post("phone"))) $data_user["phone"] = $this->post('phone');

        $http_response_header = REST_Controller::HTTP_OK;
        $arr = array();
        //check if user_id from key generated
        if (!empty($user_id)) {
            $data["id_user"] = $user_id;
            if (!empty($data_user)) {
                $a = base_url();
                if (strpos($a, REST_Controller::api_dev) !== false || strpos($a, REST_Controller::api_local) !== false) {
                    $config['upload_path'] = $this->Uploadir;
                    $config['allowed_types'] = 'bmp|jpg|jpeg|png';
                    $config['max_size'] = 8000;
                    $config["file_name"] = "ktp_" . time();
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('ktp_image')) {
                        $this->response(array('code' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'message' => strip_tags($this->upload->display_errors()), 'data' => (object)array()), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                    } else {
                        if (!empty($user_id)) {
                            $upload = $this->upload->data();
                            $assetscover = $this->DirAssets . $upload["file_name"];
                            $data_user["ktp_image"] = $assetscover;
                            $http_response_header = REST_Controller::HTTP_OK;
                        } else {
                            $http_response_header = REST_Controller::HTTP_ACCEPTED;
                        }
                    }
                } else {
                    if (!empty($_FILES) && $_FILES["ktp_image"]["error"] != 4) {
                        $files = $_FILES["ktp_image"]["name"];
                        $ext = $ext = pathinfo($files, PATHINFO_EXTENSION);
                        $tmpName = $_FILES["ktp_image"]["tmp_name"];
                        $fileName = "baboo-avatar/ktp_" . time() . ".$ext";
                        //check if uploaded
                        if ($this->s3->upload($fileName, $tmpName, $this->bucket)) {
                            $assetscover = $this->s3->url($fileName, $this->bucket);
                            $data_user["ktp_image"] = $assetscover;
                        } else {
                            $http_response_header = REST_Controller::HTTP_ACCEPTED;
                        }
                    } else {
                        $http_response_header = REST_Controller::HTTP_ACCEPTED;
                    }
                }

                $upd_user = $this->db->update("users", $data_user, array("user_id" => $user_id));
                if ($upd_user) {
                    $resp_code = $http_response_header;
                    $resp_message = "set account success";
                    $user_data = getUsersById($user_id);
                    if (count($user_data) > 0) {
                        $phone_number = $user_data["phone"];
                        delOTP($phone_number);
                    }
                    $otp = randoms_otp();
                    if (!empty($phone_number)) {
                        logOTP($phone_number, $otp);
                    }
                    //set function sendOTP

                    $responses = array(
                        "code" => $resp_code,
                        "message" => $resp_message,
                        "data" => (object)$arr
                    );
                } else {
                    $http_response_header = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                    $resp_code = $http_response_header;
                    $resp_message = "set account failed because : " . implode(",", $this->db->error());
                    $responses = array(
                        "code" => $resp_code,
                        "message" => $resp_message,
                        "data" => (object)$arr
                    );

                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "data not match or empty";
                $responses = array(
                    "code" => $resp_code,
                    "message" => $resp_message,
                    "data" => (object)$arr
                );

            }
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "user empty or unauthorized";
            $responses = array(
                "code" => $resp_code,
                "message" => $resp_message,
                "data" => (object)$arr
            );

        }
        //checking updated
        set_resp($responses, $http_response_header);
    }

    public function insertPIN_post()
    {
        $data = array();
        $data_user = null;
        $key = sethead();
        $user_id = getUserFromKey($key);
        if (!empty($this->post("pin"))) $data["no_pin"] = password_encrypt($this->post('pin'));
        $http_response_header = REST_Controller::HTTP_OK;
        //check if user_id from key generated
        if (!empty($user_id)) {
            if (!empty($data)) {
                $data["id_user"] = $user_id;
                $shis = $this->db->get_where("user_pin", array("id_user" => $user_id));
                if ($shis && $shis->num_rows() == 0) {
                    $this->db->insert("user_pin", $data);
                    $this_id = $this->db->insert_id();
                    if ($this_id) {
                        $resp_code = REST_Controller::HTTP_OK;
                        $resp_message = "set pin success";
                    } else {
                        $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                        $resp_message = "error input : " . implode(",", $this->db->error());
                        $http_response_header = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                    }
                } else {
                    $resp_code = REST_Controller::HTTP_NO_CONTENT;
                    $resp_message = "you already had a pin, forgot your pin?";
                }
            } else {
                $resp_code = REST_Controller::HTTP_NO_CONTENT;
                $resp_message = "data to insert not available or no match request";
            }
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "user not found or unauthorized";
        }

        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)array()
        );
        set_resp($responses, $http_response_header);
    }

    public function changePassword_post()
    {
        $key = sethead();
        $user_id = getUserFromKey($key);
        $old_password = $this->post("old_password");
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        if (!empty($user_id)) {
            $users = $this->db->get_where("users", array("user_id" => $user_id));
            if ($users->num_rows() > 0 && $old_password == password_decrypt($users->row()->password)) {
                $new_pass = $this->post("new_password");
                if (!empty($new_pass)) {
                    $resp_code = REST_Controller::HTTP_OK;
                    $resp_message = "ubah password berhasil";
                    $upd = $this->db->update("users", array("password" => password_encrypt($new_pass)), array("user_id" => $user_id));
                    if (!$upd) {
                        $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                        $resp_message = "ubah password gagal karena " . implode(",", $this->db->error());
                    }
                } else {
                    $resp_message = "password baru harus diisi";
                }
            } else {
                $resp_message = "password lama salah, silahkan cek kembali";
            }
        } else {
            $resp_message = "user tidak terdaftar atau tidak terautentikasi";
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message
        );
        set_resp($responses, REST_Controller::HTTP_OK);
    }

    public function forgotPassword_post()
    {
        $email = $this->post("email");
        $response["code"]=REST_Controller::HTTP_ACCEPTED;
        if (!empty($email)) {
            $users = $this->db->get_where("users", array("email" => $email));
            if ($users->num_rows() > 0) {
                $name = $users->row()->fullname;
                $new_pass = random_pass();
                if(!empty($new_pass)) $this->db->update("users",array("password"=>password_encrypt($new_pass)), array("email"=>$email));
                $data["name"] = $name;
                $data["new_pass"] = $new_pass;
                $message = $this->load->view('email', $data, true);
                $send = sendx_email($email, "Ubah Password", $message);
                if ($send) {
                    $response["code"] = REST_Controller::HTTP_OK;
                    $response['message'] = "ubah password berhasil";
                }else{
                    $response["message"] = "ubah password gagal $send";
                }
            }else{
                $response["message"] = "email anda tidak terdaftar di baboo, silahkan register terlebih dahulu";
            }
        }else{
            $response["message"] = "email tidak boleh kosong";
        }
        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function updatePIN_post()
    {
        $data = array();
        $data_user = null;
        $key = sethead();
        $user_id = getUserFromKey($key);
        if (!empty($this->post("pin"))) $data["no_pin"] = password_encrypt($this->post('pin'));
        $http_response_header = REST_Controller::HTTP_OK;
        //check if user_id from key generated
        if (!empty($user_id)) {
            if (!empty($data)) {
                $data_where = array();
                $data_where["id_user"] = $user_id;
                $shis = $this->db->get_where("user_pin", array("id_user" => $user_id));
                if ($shis && $shis->num_rows() > 0) {
                    $this_id = $this->db->update("user_pin", $data, $data_where);
                    if ($this_id) {
                        $resp_code = REST_Controller::HTTP_OK;
                        $resp_message = "set pin success";
                    } else {
                        $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                        $resp_message = "error input : " . implode(",", $this->db->error());
                        $http_response_header = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                    }
                } else {
                    $resp_code = REST_Controller::HTTP_NO_CONTENT;
                    $resp_message = "you don't had any pin, set your pin first";
                }
            } else {
                $resp_code = REST_Controller::HTTP_NO_CONTENT;
                $resp_message = "data to insert not available or no match request";
            }
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "user not found or unauthorized";
        }

        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)array()
        );
        set_resp($responses, $http_response_header);
    }

    public function confirmPIN_post()
    {
        $data_user = null;
        $key = sethead();
        $user_id = getUserFromKey($key);
        $no_pin = "";
        if (!empty($this->post("pin"))) $no_pin = $this->post('pin');
        $http_response_header = REST_Controller::HTTP_OK;
        //check if user_id from key generated
        if (!empty($user_id) && !empty($no_pin)) {
            $data_where["id_user"] = $user_id;
            $usr_pin = $this->db->get_where("user_pin", $data_where);
            if ($usr_pin && $usr_pin->num_rows() > 0) {
                $pin_db = (!empty($usr_pin->row()->no_pin)) ? password_decrypt($usr_pin->row()->no_pin) : "";
                if (!empty($pin_db)) {
                    if ($pin_db == $no_pin) {
                        $resp_code = REST_Controller::HTTP_OK;
                        $resp_message = "pin matching success";
                    } else {
                        $resp_code = REST_Controller::HTTP_ACCEPTED;
                        $resp_message = "pin not match";
                    }
                } else {
                    $resp_code = REST_Controller::HTTP_NOT_FOUND;
                    $resp_message = "pin not set in db";
                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "data not found";
            }
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "user not found or unauthorized";
        }

        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)array()
        );
        //checking updated
        set_resp($responses, $http_response_header);
    }

    public function insertQuestion_post()
    {
        $data = array();
        $data_user = null;
        $key = sethead();
        $user_id = getUserFromKey($key);

        if (!empty($this->post("question1"))) $data["question1"] = $this->post('question1');
        if (!empty($this->post("answer1"))) $data["answer1"] = $this->post('answer1');
        if (!empty($this->post("question2"))) $data["question2"] = $this->post('question2');
        if (!empty($this->post("answer2"))) $data["answer2"] = $this->post('answer2');

        $http_response_header = REST_Controller::HTTP_OK;
        //check if user_id from key generated
        if (!empty($user_id)) {
            $data_where["id_user"] = $user_id;
            $uspin = $this->db->get_where("user_pin", array("id_user" => $user_id));
            if (!empty($data) && $uspin && $uspin->num_rows() > 0) {
                $this_id = $this->db->update("user_pin", $data, $data_where);
                if ($this_id) {
                    $resp_code = REST_Controller::HTTP_OK;
                    $resp_message = "set question security success";
                } else {
                    $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                    $resp_message = "error update on : " . implode(",", $this->db->error());
                    $http_response_header = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                }
            } else {
                $resp_code = REST_Controller::HTTP_NO_CONTENT;
                $resp_message = "data to set up not available or no match request";
            }
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "user not found or unauthorized";
        }

        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)array()
        );
        //checking updated
        set_resp($responses, $http_response_header);
    }

    public function checkQuestion_post()
    {
        $key = sethead();
        $user_id = getUserFromKey($key);
        $question = "";
        $answer = "";
        $resp_data = array();
        if (!empty($this->post("question"))) $question = $this->post('question');
        if (!empty($this->post("answer"))) $answer = $this->post('answer');

        $http_response_header = REST_Controller::HTTP_OK;
        //check if user_id from key generated
        if (!empty($user_id)) {
            $data_where["id_user"] = $user_id;
            $this->db->where(array("id_user" => $user_id));
            $uspin = $this->db->get_where("user_pin");
            if ($uspin && $uspin->num_rows() > 0) {
                if ($question == $uspin->row()->question1 && $answer == $uspin->row()->answer1) $stat = true;
                else if ($question == $uspin->row()->question2 && $answer == $uspin->row()->answer2) $stat = true;
                else $stat = false;
                //check if status true
                if ($stat == true) {
                    $resp_code = REST_Controller::HTTP_OK;
                    $resp_message = "question security matched";
                    $phone_number = '';
                    $user_data = getUsersById($user_id);
                    if (count($user_data) > 0) $phone_number = $user_data["phone"];
                    $otp = randoms_otp();
                    if (!empty($phone_number)) {
                        delOTP($phone_number);
                        logOTP($phone_number, $otp);
                    }

                } else {
                    $resp_code = REST_Controller::HTTP_NO_CONTENT;
                    $resp_message = "question security not matched";
                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "you are not setting any pin yet";
            }
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "user not found or unauthorized";
        }

        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$resp_data
        );
        //checking updated
        set_resp($responses, $http_response_header);
    }

    public function confirmOTP_post()
    {
        // $key = sethead();
        // $user_id = getUserFromKey($key);
        $user_id = $this->post("user_id");
        $otp = "";
        if (!empty($this->post("otp"))) $otp = $this->post('otp');
        $http_response_header = REST_Controller::HTTP_OK;
        //check if user_id from key generated
        if (!empty($otp)) {
            $phone_number = "";
            $user_data = getUsersById($user_id);
            $gotp = array();
            if (count($user_data) > 0) $phone_number = $user_data["phone"];
            if (!empty($phone_number)) $gotp = getOTP($phone_number);
            if (count($gotp) > 0 && !empty($gotp["otp"])) {
                if ($otp == $gotp["otp"]) {
                    $resp_code = REST_Controller::HTTP_OK;
                    $this->db->select("user_id, role_id, email, fullname, IFNULL(date_of_birth, '') as date_of_birth, IFNULL(prof_pict, '') as prof_pict, email, IFNULL(oauth_uid, '') as oauth_uid, IFNULL(oauth_provider, '') as oauth_provider, IFNULL(address, '') as address, IFNULL(about_me, '') as about_me, IFNULL(phone, '') as phone_number, balance, book_sold, 0 as followers, 0 as book_made, 0 as ppl_like");
                    $this->db->from("users");
                    $this->db->where(array("user_id" => $user_id));
                    $getada = $this->db->get();
                    $data_resp = array();
                    if ($getada->num_rows() > 0) {
                        $data_resp = $getada->row();
                        $data_resp->is_newuser = true;
                        $data_resp->isFollow = false;
                        $data_resp->has_pin = false;
                        $data_resp->is_activated = false;
                        $data_resp->followers = 0;
                        $data_resp->book_made = 0;
                        $data_resp->ppl_like = 0;
                    }

                    $datas = $data_resp;
                    $resp_message = "otp matching success";
                    delOTP($phone_number);
                } else {
                    $resp_code = REST_Controller::HTTP_BAD_REQUEST;
                    $resp_message = "otp not match";
                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "otp not exist, re-send again to confirm your otp";
            }
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "user not found or unauthorized";
        }

        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$datas
        );
        //checking updated

        // $this->set_response($responses, $http_response_header);
        // set_resp($responses, $http_response_header);
        new_resp($responses, REST_Controller::HTTP_CREATED, $user_id);
    }

    public function resendOTP_get()
    {
        $key = sethead();
        $user_id = getUserFromKey($key);
        $arr = array();
        $http_response_header = REST_Controller::HTTP_OK;
        //check if user_id from key generated
        if (!empty($user_id)) {
            $resp_code = REST_Controller::HTTP_OK;
            $resp_message = "resend otp successfully, you have 10 minutes left";
            $user_data = getUsersById($user_id);
            if (count($user_data) > 0) {
                $phone_number = $user_data["phone"];
                delOTP($phone_number);
            }
            $otp = randoms_otp();
            if (!empty($phone_number)) {
                logOTP($phone_number, $otp);
            }
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "user not found or unauthorized";
        }

        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$arr
        );
        //checking updated
        set_resp($responses, $http_response_header);
    }

    public function users_delete()
    {
        $id = (int)$this->get('id');
        // Validate the id.
        if ($id <= 0) {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        // $this->some_model->delete_something($id);
        $response = $this->user->delete_user($id);
        $message = [
            'code' => $response,
            'message' => 'Deleted the resource',
            'data' => (object)array()
        ];
        set_resp($message, REST_Controller::HTTP_OK); // NO_CONTENT (204) being the HTTP response code
    }
}