<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
/**
* 
*/
class Sliders extends REST_Controller
{
	
	function __construct()
	{
		parent::__construct();
        $this->load->helper("baboo");
	}
	public function index_post()
	{
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_code = REST_Controller::HTTP_ACCEPTED;

        $getSlider = $this->db->get("sliders");
        $resp_data = array();
        if ($getSlider->num_rows() > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;
            $resp_message = "success get sliders";
            $resp_data = $getSlider->result();
        } else {
            $resp_message = "sliders not found";
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data,
        );
        set_resp($responses, $http_response_header);
	}
    public function index_get()
    {
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_code = REST_Controller::HTTP_ACCEPTED;

        $getSlider = $this->db->get("sliders");
        $resp_data = array();
        if ($getSlider->num_rows() > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;
            $resp_message = "success get sliders";
            $resp_data = $getSlider->result();
        } else {
            $resp_message = "sliders not found";
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data,
        );
        set_resp($responses, $http_response_header);
    }
}
