<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Home extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper("baboo");
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['bestBook_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['bestWriter_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['books_put']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['books_delete']['limit'] = 10; // 50 requests per hour per user/key
    }

    public function popularWriter_get()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $http_response_header = REST_Controller::HTTP_OK;
        $resp_message = "show popular writer success";
        $resp_data = (object)array();
        $count = 0;
        $writers = getAllPopularWriter($count);
        // Check if the books data store contains book id(in case the database result returns NULL)
        if (!empty($writers)) {
            // Set the response and exit
            $http_code_resp = REST_Controller::HTTP_OK;
            $resp_data = $writers;
        } else {
            // Set the response and exit
            $resp_message = 'No popular writers were found';
            $http_code_resp = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $http_code_resp,
            "message" => $resp_message,
            "data" => $resp_data
        );
        if (!empty($user_id)) set_resp($responses, $http_response_header);
        else $this->set_response($responses, $http_response_header);
    }

    public function bestWriter_get()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $http_response_header = REST_Controller::HTTP_OK;
        $count = 0;
        $resp_message = "";
        $resp_data = array();
        $books = getPopularWriter($count);
        // Check if the books data store contains book id(in case the database result returns NULL)
        if (!empty($books)) {
            // Set the response and exit
            $http_code_resp = REST_Controller::HTTP_OK;
            $resp_data = $books;
        } else {
            // Set the response and exit
            $resp_message = 'No best writers were found';
            $http_code_resp = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $http_code_resp,
            "message" => $resp_message,
            "data" => $resp_data
        );
        if (empty($user_id)) $this->set_response($responses, $http_response_header);
        else set_resp($responses, $http_response_header);
    }

    public function bestBook_get()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $count = 0;
        $limit = REST_Controller::limit_BestBook;
        if (!empty($this->get("count"))) {
            if ((int)$this->get("count") > 1) {
                if (!empty($this->get("count")) && $this->get("count") > 1) {
                    $mcount = (int)$this->get("count") - 1;
                    $county = $mcount * $limit;
                    $count = $county + 1;
                }
            }
        }
        $resp_message = "show best book success";
        $resp_data = (object)array();
        $books = getPopularBook($count);
        // Check if the books data store contains book id(in case the database result returns NULL)
        if (!empty($books)) {
            $http_code_resp = REST_Controller::HTTP_OK;
            $resp_data = $books;
        } else {
            // Set the response and exit
            $resp_message = 'No best books were found';
            $http_code_resp = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $http_code_resp,
            "message" => $resp_message,
            "data" => $resp_data
        );
        $this->set_response($responses, $http_response_header);
    }

    public function slideBook_get()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $count = (int)$this->get("count");
        if (empty($count)) $count = 5;
        $resp_message = "show slide book success";
        $resp_data = (object)array();
        $books = getSlideBook(0, REST_Controller::limit_slideBook);
        // Check if the books data store contains book id(in case the database result returns NULL)
        if (!empty($books)) {
            $http_code_resp = REST_Controller::HTTP_OK;
            $resp_data = $books;
        } else {
            // Set the response and exit
            $resp_message = 'No best books were found';
            $http_code_resp = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $http_code_resp,
            "message" => $resp_message,
            "data" => $resp_data
        );
        $this->set_response($responses, $http_response_header);
    }

    public function finalisBook_post()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $resp_message = "tulisan buku lomba terbaik berhasil ditampilkan";
        $http_code_resp = REST_Controller::HTTP_ACCEPTED;
        $resp_data = (object)array();
        $limit = REST_Controller::limit_BestBook;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;
        $books = getFinalisLomba($count);
        // Check if the books data store contains book id(in case the database result returns NULL)
        if (!empty($books)) {
            // Set the response and exit
            $http_code_resp = REST_Controller::HTTP_OK;
            $resp_data = $books;
        } else {
            // Set the response and exit
            $resp_message = 'No best books were found';
            $http_code_resp = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $http_code_resp,
            "message" => $resp_message,
            "data" => $resp_data
        );
        $this->set_response($responses, $http_response_header);
    }

    public function finalisWinner_post()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $resp_message = "show finalist winner success";
        $http_code_resp = REST_Controller::HTTP_ACCEPTED;
        $resp_data = (object)array();
        $limit = REST_Controller::limit_BestBook;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;

        $books = getFinalisWinner($count);
        // Check if the books data store contains book id(in case the database result returns NULL)
        if (!empty($books)) {
            // Set the response and exit
            $http_code_resp = REST_Controller::HTTP_OK;
            $resp_data = $books;
        } else {
            // Set the response and exit
            $resp_message = 'No winners were found';
            $http_code_resp = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $http_code_resp,
            "message" => $resp_message,
            "data" => $resp_data
        );
        $this->set_response($responses, $http_response_header);
    }

    public function index_get()
    {
        $http_response_header = REST_Controller::HTTP_OK;
        $count = 0;
        $limit = REST_Controller::limit_timeline;
        if (!empty($this->get("count"))) {
            if ((int)$this->get("count") > 1) {
                if (!empty($this->get("count")) && $this->get("count") > 1) {
                    $mcount = (int)$this->get("count") - 1;
                    $county = $mcount * $limit;
                    $count = $county;
                }
            }
        }
        $resp_message = "";
        $http_code_resp = REST_Controller::HTTP_ACCEPTED;
        $resp_data = array();
        $this->db->limit(REST_Controller::limit_timeline, $count);
        $this->db->order_by("book.share_count", "desc");
        $this->db->order_by("book.view_count", "desc");
        $this->db->order_by("book.publish_at", "desc");
        $this->db->where("book.status_publish !=", "draft");
        //$this->db->where("chapter.chapter_title = book.title_book");
        $this->db->join("users", "users.user_id = book.author_id");
        $this->db->select("book_id, title_book, book_type, author_id, fullname, prof_pict, category_id, author_id, file_cover, image_url, latest_update, publish_at, like_count, view_count, share_count, is_pdf, descriptions");
        $books = $this->db->get("book");
        // Check if the books data store contains book id(in case the database result returns NULL)
        if ($books && $books->num_rows() > 0) {
            // Set the response and exit
            $http_code_resp = REST_Controller::HTTP_OK;
            foreach ($books->result() as $bokeys => $boval) {
                $bokeks = $bokeys + 1;
                if(!empty($boval->is_pdf)) $res_par_text = getParagraphByBook($boval->book_id);
                else $res_par_text = substr((string)$boval->descriptions, 0, REST_Controller::text_limits) . "...";
                $getComment = $this->db->query("SELECT COUNT(comment_id) as comment_count FROM comment WHERE id_book = ?", array($boval->book_id));
                $category = getCategoryBook($boval->category_id);
                $dasray = array(
                    "book_id" => $boval->book_id,
                    "author_id" => $boval->author_id,
                    "author_name" => $boval->fullname,
                    "author_avatar" => (string)$boval->prof_pict,
                    "cover_url" => (string)$boval->file_cover,
                    "image_url" => (string)$boval->image_url,
                    "publish_date" => time_ago($boval->publish_at),
                    "title_book" => (string)$boval->title_book,
                    "like_count" => (int)$boval->like_count,
                    "is_pdf" => (!empty($boval->is_pdf)) ? false : true,
                    "book_type" => (int)$boval->book_type,
                    "view_count" => (int)$boval->view_count,
                    "comment_count" => ($getComment && $getComment->num_rows() > 0) ? (int)$getComment->row()->comment_count : 0,
                    "share_count" => (int)$boval->share_count,
                    "category" => $category,
                    "desc" => trim($res_par_text)
                );
                $populars = (object)array();
                if ($bokeks == REST_Controller::popularBook_position || $bokeks == REST_Controller::editorsChoice_position) {
                    if ($bokeks == REST_Controller::popularBook_position) {
                        $desc = "Popular Book";
                        $bookzy = getPopularBook();
                    } else {
                        $desc = "Finalist Book";
                        $bookzy = getFinalisLomba();
                    }
                    if (!empty($bookzy)) {
                        $populars = (object)array("desc" => $desc, "data" => $bookzy);
                    }
                }
                $dasray["populars"] = $populars;
                $resp_data[] = $dasray;
            }
        } else {
            // Set the response and exit
            $resp_message = 'no timeline data';
            $http_code_resp = REST_Controller::HTTP_NOT_FOUND;
            //$resp_data = (object)$resp_data;
        }
        $responses = array(
            "code" => $http_code_resp,
            "message" => $resp_message,
            "data" => $resp_data
        );
        $this->set_response($responses, $http_response_header);
    }

    public function detailPDF_post()
    {
        $book_id = $this->post("book_id");
        $data = $this->post();
        $this->db->select("book_id as id, category_id, price, total_price, book_type, is_free, view_count, like_count, share_count, title_book, author_id, IFNULL(file_cover,'') as cover_url, fullname as author, prof_pict as author_pict, status_publish, publish_at, latest_update, generated_date, pdf_url, pdf_free, pdf_start");
        $this->db->from("book");
        $this->db->join("users", "users.user_id = book.author_id");
        $this->db->where(array('book_id' => $data['book_id'], 'is_pdf' => 0));
        $prevQuery = $this->db->get();
        $prevCheck = $prevQuery->num_rows();
        $stat_publish = "publish";
        $author_id = "";
        $books = array();
        $http_response = REST_Controller::HTTP_ACCEPTED;
        if ($prevCheck > 0) {
            $stat_publish = $prevQuery->row()->status_publish;
            $author_id = $prevQuery->row()->author_id;
            $view_count = 1;
            $is_bookmark = false;
            $is_likes = false;
            $publish_date = $prevQuery->row()->latest_update;
            $is_follow = false;
            $is_download = false;
            $is_bought = false;
            if ($prevQuery->row()->status_publish != "draft") {
                $publish_date = $prevQuery->row()->publish_at;
            }
            $generateDate = (!empty($prevQuery->row()->generated_date)) ? date("Y-m-d H:i:s O", strtotime($prevQuery->row()->generated_date)) : "";
            $latest_update = "";
            if (!empty($prevQuery->row()->latest_update) && $prevQuery->row()->latest_update != $prevQuery->row()->publish_at && $stat_publish == "publish") $latest_update = time_ago($prevQuery->row()->latest_update);
            $count_comment = 0;
            $getcomment = $this->db->get_where("comment", array("id_book" => $prevQuery->row()->id));
            if ($getcomment->num_rows() > 0) $count_comment = $getcomment->num_rows();
            $is_free = false;
            $pdf_url = (!empty($prevQuery->row()->pdf_free)) ? $prevQuery->row()->pdf_free : $prevQuery->row()->pdf_url;
            $price = 0;
            if ($is_free == false) {
                $price = (double)$prevQuery->row()->total_price;
            }
            $prevResult = array(
                "book_info" => array(
                    "book_id" => $prevQuery->row()->id,
                    "title_book" => (string)$prevQuery->row()->title_book,
                    "view_count" => (int)$prevQuery->row()->view_count,
                    "like_count" => (int)$prevQuery->row()->like_count,
                    "is_bookmark" => $is_bookmark,
                    "is_bought" => $is_bought,
                    "is_download" => $is_download,
                    "epoch_time" => $generateDate,
                    "is_like" => $is_likes,
                    "is_free" => $is_free,
                    "chapter_sale" => (int)$prevQuery->row()->pdf_start,
                    "book_price" => $price,
                    "url_book" => (string)$pdf_url,
                    "book_type" => (int)$prevQuery->row()->book_type,
                    "share_count" => (int)$prevQuery->row()->share_count,
                    "book_comment_count" => (int)$count_comment,
                    "latest_update" => $latest_update,
                    "publish_date" => time_ago($publish_date),
                    "cover_url" => $prevQuery->row()->cover_url
                ),
                "author" => array(
                    "author_id" => $prevQuery->row()->author_id,
                    "author_name" => $prevQuery->row()->author,
                    "avatar" => (string)$prevQuery->row()->author_pict,
                    "isFollow" => $is_follow
                )
            );
            $data_category = array();
            $this->db->select("CAST(id_catbook AS UNSIGNED) AS category_id, cat_book AS category_name");
            if (!empty($prevQuery->row()->category_id)) $this->db->where("id_catbook", $prevQuery->row()->category_id);
            else $this->db->order_by("id_catbook", "asc");
            $dct = $this->db->get("book_category");
            if ($dct->num_rows() > 0) $data_category = $dct->row();
            $prevResult["category"] = (object)$data_category;
            //set response
            $books["code"] = REST_Controller::HTTP_OK;
            $books["message"] = "show detail book success";
            $books["data"] = $prevResult;
            $http_response = REST_Controller::HTTP_OK;
        } else {
            $bbid = (object)array();
            $books["code"] = REST_Controller::HTTP_NOT_FOUND;
            $books["message"] = "book is not pdf type or not found";
            $books["data"] = $bbid;
        }
        $this->set_response($books, $http_response);
    }

    public function allChapters_post()
    {
        $book_id = $this->post("book_id");
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $resp_message = "get all chapter success";
        $data_chapter = array();
        $book_info = array();
        $chapters = array();
        $books = $this->db->get_where("book", array("book_id" => $book_id, "status_publish !=" => "draft"));
        $is_free = false;
        if ($books->num_rows() > 0) {
            $count_comment = 0;
            $getcomment = $this->db->get_where("comment", array("id_book" => $books->row()->book_id));
            if ($getcomment->num_rows() > 0) $count_comment = $getcomment->num_rows();
            $is_likes = false;
            $is_bookmark = false;
            //validate and fill is_bookmark true when theres data on bookmark table
            if ($books->row()->is_free == "free") {
                $is_free = true;
            }
            $price = 0;
            if ($is_free == false) {
                $price = (double)$books->row()->total_price;
            }
            $book_info = array(
                "book_id" => $books->row()->book_id,
                "title_book" => (string)$books->row()->title_book,
                "view_count" => (int)$books->row()->view_count,
                "like_count" => (int)$books->row()->like_count,
                "is_like" => $is_likes,
                "is_free" => $is_free,
                "book_price" => $price,
                "is_bookmark" => $is_bookmark,
                "share_count" => (int)$books->row()->share_count,
                "book_comment_count" => (int)$count_comment,
                "cover_url" => (string)$books->row()->file_cover
            );
        }
        $data_chapter["book_info"] = (object)$book_info;
        $this->db->select("chapter_id, chapter_title");
        $dcg = $this->db->get_where("chapter", array("id_book" => $book_id));
        if (empty($book_id)) {
            $resp_code = REST_Controller::HTTP_BAD_REQUEST;
            $resp_message = "book id cannot empty";
        }
        if ($dcg->num_rows() > 0) {
            $limit_text = REST_Controller::limitDescChapter;
            foreach ($dcg->result() as $dckey => $dcval) {
                $res_par_text = "";
                $gepar = $this->db->get_where("paragraph", array("id_chapter" => $dcval->chapter_id));
                foreach ($gepar->result() as $par_result => $paragraph) {
                    $par_text = $clear = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($paragraph->txt_paragraph))))));
                    $res_par_text .= $par_text;
                    $limit_count = $gepar->num_rows() - 1;
                    if ($par_result != $limit_count) $res_par_text .= " ";
                    if (strlen($res_par_text) > $limit_text) {
                        $res_par_text = substr($res_par_text, 0, $limit_text) . "...";
                        break;
                    }
                }
                $limit_free = REST_Controller::limitFree - 1;
                $data_free = true;
                if ($is_free == false && $dckey > $limit_free) {
                    $data_free = false;
                }
                $chapters[] = array(
                    "chapter_id" => $dcval->chapter_id,
                    "chapter_title" => $dcval->chapter_title,
                    "desc" => trim($res_par_text),
                    "chapter_free" => $data_free
                );
            }
            // $data_chapter = $dcg->result_array();
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "chapter not found";
        }
        $data_chapter["chapter"] = $chapters;
        $resp_array = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$data_chapter,
        );
        $this->set_response($resp_array, $http_response_header);
    }

    public function checkDiff_post()
    {
        $book_id = $this->post("book_id");
        $chapter_id = $this->post("chapter_id");
        $new_par = $this->post("paragraph");
        $resp_code = update_paragraph($book_id, $chapter_id, $new_par);
        $resp_message = "update paragraph success";
        if ($resp_code["code"] != REST_Controller::HTTP_OK) {
            $resp_message = "update failed : " . $resp_code["message"];
        }
        $this->set_response(array("code" => $resp_code["code"], "message" => $resp_message, "data" => $resp_code["data"]), REST_Controller::HTTP_OK);
    }

    public function detailBook_post()
    {
        $book_id = $this->post("book_id");
        $data = $this->post();

        $this->db->select("book_id as id, category_id, price, baboo_profit, is_free, view_count, like_count, share_count, title_book, author_id, file_cover as cover_url, fullname as author, prof_pict as author_pict, status_publish, is_pdf, descriptions");
        $this->db->from("book");
        $this->db->join("users", "users.user_id = book.author_id");
        $this->db->where(array('book_id' => $data['book_id']));
        $prevQuery = $this->db->get();
        $prevCheck = $prevQuery->num_rows();

        $books = array();
        $http_response = REST_Controller::HTTP_ACCEPTED;
        if ($prevCheck > 0) {
            $view_count = 1;
            $is_bookmark = false;

            $count_comment = 0;
            $getcomment = $this->db->get_where("comment", array("id_book" => $prevQuery->row()->id));
            if ($getcomment->num_rows() > 0) $count_comment = $getcomment->num_rows();
            $is_follow = false;
            $is_likes = false;
            $is_free = false;
            if ($prevQuery->row()->is_free == "free") {
                $is_free = true;
            }
            $price = "free";
            if ($is_free == false) {
                $price = (double)$prevQuery->row()->price + (double)$prevQuery->row()->baboo_profit;
                $price = "Rp. " . number_format($price, 0, '.', '.') . ",-";
            }
            $is_pdf = true;
            if($prevQuery->row()->is_pdf == 1) $is_pdf = false;
            $prevResult = array(
                "book_info" => array(
                    "book_id" => $prevQuery->row()->id,
                    "title_book" => $prevQuery->row()->title_book,
                    "view_count" => (int)$prevQuery->row()->view_count,
                    "like_count" => (int)$prevQuery->row()->like_count,
                    "is_like" => $is_likes,
                    "is_free" => $is_free,
                    "is_pdf" => $is_pdf,
                    "book_price" => $price,
                    "is_bookmark" => $is_bookmark,
                    "share_count" => (int)$prevQuery->row()->share_count,
                    "book_comment_count" => (int)$count_comment,
                    "cover_url" => $prevQuery->row()->cover_url
                ),
                "author" => array(
                    "author_id" => $prevQuery->row()->author_id,
                    "author_name" => $prevQuery->row()->author,
                    "avatar" => (string)$prevQuery->row()->author_pict,
                    "isFollow" => $is_follow
                )
            );

            $chapters_id = "";
            $data_category = array();
            $paragraph = array();
            if($is_pdf == false) {
                if (isset($data['chapter']) && !empty($data['chapter'])) {
                    $this->db->where(array('chapter_id' => $data['chapter']));
                    $this->db->where(array('status_free' => 'free'));
                } else {
                    $this->db->order_by("chapter_id", "asc");
                }
                $this->db->where(array("id_book" => $data["book_id"]));
                $gchap = $this->db->get("chapter");
                if ($gchap->num_rows() > 0) {
                    $chav = $gchap->row();
                    $chapters_id = $chav->chapter_id;

                    $paragraph = array(
                        "chapter_id" => $chav->chapter_id,
                        "chapter_title" => $chav->chapter_title
                    );
                    $total_par = 0;
                    $cpar = $this->db->query("SELECT COUNT(paragraph_id) as total_par FROM paragraph WHERE id_chapter = ?", array($chav->chapter_id));
                    if($cpar && $cpar->num_rows() > 0) $total_par = $cpar->row()->total_par;
                    if($total_par > 0) {
                        $limit = floor($total_par / 3);
                        if($limit == 0) $limit = 1;
                        $this->db->limit($limit, 0);
                    }
                    $this->db->select("txt_paragraph, paragraph_id");
                    $gpar = $this->db->get_where("paragraph", array("id_chapter" => $chav->chapter_id));
                    if ($gpar->num_rows() > 0) {
                        foreach ($gpar->result() as $gpkey=>$gpval) {
                            $par_text = $gpval->txt_paragraph;
                            $gpp = $gpkey + 1;
                            if($gpp == $gpar->num_rows()) $par_text.= "..";
                            if (substr($gpval->txt_paragraph, 0, 2) != "<p") {
                                $par_text = "<p>" . $par_text."</p>";
                            } elseif (substr($gpval->txt_paragraph, -4) != "</p>") {
                                $par_text .= "</p>";
                            }
                            $count_commentp = 0;
                            $this->db->select("id_paragraph");
                            $getcommentp = $this->db->get_where("comment", array("id_paragraph" => $gpval->paragraph_id));
                            if ($getcommentp->num_rows() > 0) $count_commentp = $getcommentp->num_rows();

                            $paragraph["paragraphs"][] = array(
                                "paragraph_id" => $gpval->paragraph_id,
                                "paragraph_text" => $par_text,
                                "comment_count" => (int)$count_commentp
                            );
                        }
                    }
                }
            }else{
                $paragraphoy = trim(preg_replace('/\v+|\\\r\\\n/','<br/>', htmlspecialchars($prevQuery->row()->descriptions)));
                if(!empty($paragraphoy)) $paragraphoy = "<p>".$paragraphoy."</p>";
                $paragraph["paragraphs"][] = array(
                    "paragraph_text" => (string)$paragraphoy,
                );

            }
            //check if !empty $chapters_id
            if ((!empty($chapters_id) || !empty($data["book_id"])) && !empty($this->post("ip_client"))) {
                if(!empty($chapters_id)){
                    $qwr_c = $this->db->query("SELECT COUNT(id_chapter) as total_chap FROM view_counts WHERE id_chapter = ? AND count_by = ?", array($chapters_id, $this->post("ip_client")));
                }else{
                    $qwr_c = $this->db->query("SELECT COUNT(id_book) as total_chap FROM view_counts WHERE id_book = ? AND count_by = ?", array($data["book_id"], $this->post("ip_client")));
                }
                if($qwr_c && $qwr_c->row()->total_chap == 0){
                    $arr_c = array(
                        "count_by"  => $this->post("ip_client"),
                        "id_book"  => $data["book_id"],
                        "id_chapter"  => $chapters_id,
                    );
                    $this->db->insert("view_counts", $arr_c);
                    $acid = $this->db->insert_id();
                    if($acid){
                        $data_update = array();
                        $this->db->select("view_count");
                        $cgs = $this->db->get_where("book", array("book_id" => $data["book_id"]));
                        if ($cgs->num_rows() > 0) $view_count = (int)$cgs->row()->view_count + 1;
                        $data_update['view_count'] = $view_count;
                        $this->db->update("book", $data_update, array('book_id' => $data["book_id"]));
                    }
                }
            }

            $this->db->select("id_catbook as category_id, cat_book as category_name");
            $this->db->where("id_catbook", $prevQuery->row()->category_id);
            $dct = $this->db->get("book_category");
            if ($dct->num_rows() > 0) $data_category = $dct->row();
            $prevResult["category"] = (object)$data_category;
            $prevResult["chapter"] = $paragraph;

            $books["code"] = REST_Controller::HTTP_OK;
            $books["message"] = "show detail book success";
            $books["data"] = $prevResult;
            $http_response = REST_Controller::HTTP_OK;
        } else {
            $bbid = (object)array();
            $books["code"] = REST_Controller::HTTP_NOT_FOUND;
            $books["message"] = "Book Not Found";
            $books["data"] = $bbid;
        }
        $this->set_response($books, $http_response);
    }
}
