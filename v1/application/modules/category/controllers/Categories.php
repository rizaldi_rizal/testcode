<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
/**
* 
*/
class Categories extends REST_Controller
{
	
	function __construct()
	{
		parent::__construct();
        $this->load->helper("baboo");
	}
	public function index_post()
	{
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_code = REST_Controller::HTTP_ACCEPTED;

        $getCategory = $this->db->get("categories");
        $resp_data = array();
        if ($getCategory->num_rows() > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;
            $resp_message = "success get category";
            $resp_data = $getCategory->result();
        } else {
            $resp_message = "category not found";
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data,
        );
        set_resp($responses, $http_response_header);
	}
    public function index_get()
    {
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_code = REST_Controller::HTTP_ACCEPTED;

        $getCategory = $this->db->get("categories");
        $resp_data = array();
        if ($getCategory->num_rows() > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;
            $resp_message = "success get category";
            $resp_data = $getCategory->result();
        } else {
            $resp_message = "category not found";
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data,
        );
        set_resp($responses, $http_response_header);
    }
}

/* End of file Categories.php */
/* Location: ./application/modules/category/controllers/Categories.php */