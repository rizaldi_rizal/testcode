<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

class Bg_process extends MX_Controller
{
    public function __construct()
    {
        $this->load->helper('baboo');
        $this->load->helper('url');
        $this->load->library("CloudStorage", NULL, "s3");
        $this->bucket = REST_Controller::bucket_prod;
        if (strpos(base_url(), REST_Controller::api_prod) !== false){
            $this->bucket = REST_Controller::bucket_staging;
        }
        $this->ParentUploadir = "../uploads/";
        $this->Uploadir = $this->ParentUploadir . "baboo-docs/";
        if (base_url() == "http://" . REST_Controller::api_dev || base_url() == "https://" . REST_Controller::api_dev || base_url() == REST_Controller::api_local) {
            $this->ParentUploadir = "../../uploads/";
            $this->Uploadir = $this->ParentUploadir . "baboo-docs/";
            if (!file_exists($this->ParentUploadir)) {
                mkdir($this->ParentUploadir, 0777);
            }
            if (!file_exists($this->Uploadir)) {
                mkdir($this->Uploadir, 0777);
            }
        }
        $this->DirAssets = REST_Controller::assets_dev . "baboo-docs/";
    }

    public function createEpubFree($book_id = ''){
        $datanya = $this->getChapters($book_id)['data'];
        $book = $datanya->book_info;
        $chapter = $datanya->chapter;
        $author_id = $book->author_id;
        $bookInfo = array(
            'title'       => $book->title_book,
            'creator'     => $book->author_name,
            'subject'     => $book->category,
            'description' => $chapter[0]['desc'],
            'publisher'   => 'Baboo',
            'contributor' => 'Baboo',
            'date'        => date('YY-M-d H:i:s'),
            'type'        => 'book',
            'format'      => 'ePub',
            'source'      => 'Baboo.id',
            'language'    => 'id-ID',
            'relation'    => '',
            'coverage'    => '',
            'rights'      => 'zBook.EpubMaker'
        );

        $bookCover = $book->cover_url;
        $dataChapter = array();
        foreach ($chapter as $ch){
            if ($ch['status_publish'] == 'publish' && (bool)$ch['chapter_free'] == TRUE) {
                $ch_title = $ch['chapter_title'];
                $ch_desc = $ch['desc'];
                $dataChapter[$ch_title] = $ch_desc;
            }
        }

        $resval = $this->epubcreator->create_epub_free($book->book_id, $book->title_book, $bookInfo, $bookCover, './', $dataChapter);
        print_r($resval);
        $output = str_replace(" ", "-", strtolower($book->title_book))."-free.epub";
        $tmpName =  $this->Uploadir.$output;
        $generateDate = date("Y-m-d H:i:s O");
        $assetOutput = $this->DirAssets . $output;
        if (file_exists($tmpName)) {
            if (base_url() == "http://" . REST_Controller::api_prod || base_url() == "https://" . REST_Controller::api_prod || base_url() == "http://" . REST_Controller::api_staging || base_url() == "https://" . REST_Controller::api_staging) {
                $fileName = "baboo-docs/" . $output;
                if ($this->s3->upload($fileName, $tmpName, $this->bucket)) {
                    $realpath = realpath(trim($this->Uploadir));
                    if (unlink(realpath($tmpName))) {
                    $notif_text = "Buku anda berhasil dihapus";
                    } else {
                    $notif_text = "Buku anda gagal dihapus ".realpath($tmpName);
                    }
                    $assetOutput = $this->s3->url($fileName, $this->bucket);
                    $upd = $this->db->query("UPDATE book SET  pdf_url = '" . $assetOutput . "', generated_date = '" . $generateDate . "' WHERE book_id = " . $book_id);
                    if ($upd) {
                        $notif_text = "Buku anda sudah dipublikasikan";
                        $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
                        $instys = $this->db->insert_id();
                    }else {
                        $notif_text = "Buku anda gagal dipublikasikan";
                        $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
                    }
                } else {
                    $notif_text = "Buku anda gagal dipublikasikan";
                    $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
                }
            }
            else{
                $upd = $this->db->query("UPDATE book SET  pdf_url = '" . $assetOutput . "', generated_date = '" . $generateDate . "' WHERE book_id = " . $book_id);
                if ($upd) {
                    $notif_text = "Buku anda sudah dipublikasikan";
                    $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
                    $instys = $this->db->insert_id();
                }else {
                    $notif_text = "Buku anda gagal dipublikasikan";
                    $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
                }
            }

            if($instys && !empty($instys)){
                $colls = $this->db->query("SELECT COUNT(id_user) as total_collection FROM collections WHERE id_book = ?", array($book_id));
                if($colls && $colls->num_rows() > 0 && $colls->row()->total_collection > 0) $this->db->update("collections",array("is_download"=>0), array("id_book"=>$book_id));
            }

        } else {
            $notif_text = "Buku anda gagal dipublikasikan ".$tmpName;
            $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
        }
    }

    public function createEpubFull($book_id = ''){
        $datanya = $this->getChapters($book_id)['data'];
        $book = $datanya->book_info;
        $chapter = $datanya->chapter;
        $author_id = $book->author_id;
        $bookInfo = array(
            'title'       => $book->title_book,
            'creator'     => $book->author_name,
            'subject'     => $book->category,
            'description' => $chapter[0]['desc'],
            'publisher'   => 'Baboo',
            'contributor' => 'Baboo',
            'date'        => date('YY-M-d H:i:s'),
            'type'        => 'book',
            'format'      => 'ePub',
            'source'      => 'Baboo.id',
            'language'    => 'id-ID',
            'relation'    => '',
            'coverage'    => '',
            'rights'      => 'zBook.EpubMaker'
        );

        $bookCover = $book->cover_url;
        $dataChapter = array();
        foreach ($chapter as $ch){
            if ($ch['status_publish'] == 'publish') {
                $ch_title = $ch['chapter_title'];
                $ch_desc = $ch['desc'];
                $dataChapter[$ch_title] = $ch_desc;
            }
        }

        $resval = $this->epubcreator->create_epub_full($book->book_id, $book->title_book, $bookInfo, $bookCover, './', $dataChapter);
        print_r($resval);
        $output = str_replace(" ", "-", strtolower($book->title_book)).".epub";
        $tmpName =  $this->Uploadir.$output;
        $generateDate = date("Y-m-d H:i:s O");
        $assetOutput = $this->DirAssets . $output;
        if (file_exists($tmpName)) {
            if (base_url() == "http://" . REST_Controller::api_prod || base_url() == "https://" . REST_Controller::api_prod || base_url() == "http://" . REST_Controller::api_staging || base_url() == "https://" . REST_Controller::api_staging) {
                $fileName = "baboo-docs/" . $output;
                if ($this->s3->upload($fileName, $tmpName, $this->bucket)) {
                    $realpath = realpath(trim($this->Uploadir));
                    if (unlink(realpath($tmpName))) {
                    $notif_text = "Buku anda berhasil dihapus";
                    } else {
                    $notif_text = "Buku anda gagal dihapus ".realpath($tmpName);
                    }
                    $assetOutput = $this->s3->url($fileName, $this->bucket);
                    $upd = $this->db->query("UPDATE book SET  pdf_url = '" . $assetOutput . "', generated_date = '" . $generateDate . "' WHERE book_id = " . $book_id);
                    if ($upd) {
                        $notif_text = "Buku anda sudah dipublikasikan";
                        $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
                        $instys = $this->db->insert_id();
                    }else {
                        $notif_text = "Buku anda gagal dipublikasikan";
                        $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
                    }
                } else {
                    $notif_text = "Buku anda gagal dipublikasikan";
                    $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
                }
            }
            else{
                $upd = $this->db->query("UPDATE book SET  pdf_url = '" . $assetOutput . "', generated_date = '" . $generateDate . "' WHERE book_id = " . $book_id);
                if ($upd) {
                    $notif_text = "Buku anda sudah dipublikasikan";
                    $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
                    $instys = $this->db->insert_id();
                }else {
                    $notif_text = "Buku anda gagal dipublikasikan";
                    $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
                }
            }

            if($instys && !empty($instys)){
                $colls = $this->db->query("SELECT COUNT(id_user) as total_collection FROM collections WHERE id_book = ?", array($book_id));
                if($colls && $colls->num_rows() > 0 && $colls->row()->total_collection > 0) $this->db->update("collections",array("is_download"=>0), array("id_book"=>$book_id));
            }

        } else {
            $notif_text = "Buku anda gagal dipublikasikan ".$tmpName;
            $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
        }
    }

    public function generateEpub() {
        $book_id = (!empty($this->input->post("book_id"))) ? $this->input->post("book_id") : 0;
        if(!empty($book_id)){
            $wherebook = array("book_id" => $book_id);
            $this->db->where($wherebook);
            $this->db->select("is_free");
            $bookys = $this->db->get("book");
            if($bookys && $bookys->num_rows() > 0){
                $this->createEpubFree($book_id);
                if($bookys->row()->is_free != "free") $this->createEpubFull($book_id);
            }
        }
    }

    public function getChapters($book_id = 0)
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_message = "get all chapter success";
        $data_chapter = array();
        $book_info = array();
        $chapters = array();
        $status_publish = 1;
        $wherebook = array(
            "book_id" => $book_id
        );
        $this->db->where($wherebook);
        $this->db->join("users", "users.user_id = book.author_id");
        $this->db->select("book_id, title_book, author_id, fullname, prof_pict, category_id, author_id, file_cover, image_url, latest_update, publish_at, like_count, view_count, share_count, is_pdf, descriptions");
        $books = $this->db->get("book");
        $is_free = false;
        $is_bought = false;
        $is_download = false;
        $valid = true;
        $stat_publish = "publish";
        if ($books && $books->num_rows() > 0) {
            $stat_publish = $books->row()->status_publish;
            $this->db->select("is_download");
            $cekBought = $this->db->get_where("collections", array("id_user" => $user_id, "id_book" => $book_id));
            if ($cekBought->num_rows() > 0) {
                $is_bought = true;
                if (isset($cekBought->row()->is_download) && $cekBought->row()->is_download == 1) $is_download = true;
            }
            if ($stat_publish == "on editor") $stat_publish = "edit publish";
            if ($books->row()->status_publish != "publish" && $books->row()->author_id == $user_id) {
                $status_publish = 2;
            }
            if ($books->row()->status_publish == "draft" && $books->row()->author_id != $user_id) {
                $valid = false;
            }
            //check if valid true
            if ($valid) {
                $count_comment = 0;
                $this->db->select("id_book");
                $getcomment = $this->db->get_where("comment", array("id_book" => $books->row()->book_id));
                if ($getcomment->num_rows() > 0) $count_comment = $getcomment->num_rows();
                $is_likes = false;
                $this->db->select("id_book");
                $getlikes = $this->db->get_where("likes", array("id_book" => $books->row()->book_id, "id_user" => $user_id));
                if ($getlikes->num_rows() > 0) {
                    $is_likes = true;
                }
                $is_bookmark = false;
                $this->db->where("id_user", $user_id);
                $this->db->where("id_book", $book_id);
                $this->db->select("id_book");
                $sbookmark = $this->db->get("bookmark");
                //validate and fill is_bookmark true when theres data on bookmark table
                if ($sbookmark->num_rows() > 0) $is_bookmark = true;
                if ($books->row()->is_free == "free" || $is_bought == true) {
                    $is_free = true;
                }

                $price = 0;
                if ($is_free == false) {
                    $price = (double)$books->row()->total_price;
                }
                $titleBook = $books->row()->title_book;
                if (!empty($books->row()->title_draft)) {
                    $titleBook = $books->row()->title_draft;
                }
                $category = getCategoryBook($books->row()->category_id);
                $book_info = array(
                    "book_id" => $books->row()->book_id,
                    "author_id" => $books->row()->author_id,
                    "author_name" => $books->row()->fullname,
                    "category" => $category,
                    "title_book" => (string)$titleBook,
                    "view_count" => (int)$books->row()->view_count,
                    "like_count" => (int)$books->row()->like_count,
                    "is_like" => $is_likes,
                    "is_bought" => $is_bought,
                    "is_download" => $is_download,
                    "is_free" => $is_free,
                    "book_price" => $price,
                    "is_bookmark" => $is_bookmark,
                    "share_count" => (int)$books->row()->share_count,
                    "book_comment_count" => (int)$count_comment,
                    "cover_url" => (string)$books->row()->file_cover
                );
            } else {
                $resp_code = '403';
                $resp_message = "unauthorized";
            }
        }
        $data_chapter["book_info"] = (object)$book_info;
        if ($status_publish == 1) $this->db->where("status_publish", $status_publish);
        $this->db->where("id_book", $book_id);
        $dcg = $this->db->get("chapter");
        if (empty($book_id)) {
            $resp_code = '400';
            $resp_message = "book id cannot empty";
        }

        //check
        if ($dcg->num_rows() > 0 && $valid !== false) {
            foreach ($dcg->result() as $dckey => $dcval) {
                $stat_draft = $dcval->status_publish;
                $stat_free = $dcval->status_free;
                $statey = "publish";
                if ($stat_draft > 1) {
                    if ($stat_draft == 2 && $stat_publish == "edit publish") $statey = "edit publish";
                    else $statey = "draft";
                }
                $res_par_text = "";
                $chapter_title = $dcval->chapter_title;
                if ($status_publish == 1) {
                    $gepar = $this->db->get_where("paragraph", array("id_chapter" => $dcval->chapter_id));
                } else {
                    if (!empty($dcval->chapter_draft)) $chapter_title = $dcval->chapter_draft;
                    $gepar = $this->db->get_where("draft_paragraph", array("id_chapter" => $dcval->chapter_id));
                    if ($gepar->num_rows() == 0) $gepar = $this->db->get_where("paragraph", array("id_chapter" => $dcval->chapter_id));
                }
                foreach ($gepar->result() as $par_result => $paragraph) {
                    $res_par_text .= $paragraph->txt_paragraph;
                    $limit_count = $gepar->num_rows() - 1;
                    if ($par_result != $limit_count) $res_par_text .= " ";
                }
                $data_free = false;
                if ($stat_free == "free" || $is_bought == true) {
                    $data_free = true;
                }
                $chapters[] = array(
                    "chapter_id" => $dcval->chapter_id,
                    "chapter_title" => $chapter_title,
                    "desc" => trim($res_par_text),
                    "status_publish" => $statey,
                    "chapter_free" => $data_free
                );
            }
            // $data_chapter = $dcg->result_array();
            $http_response_header = '200';
            $resp_code = '200';

        } else {
            $resp_code = '404';
            $resp_message = "chapter not found or unauthorized";
        }
        $data_chapter["chapter"] = $chapters;

        $resp_array = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$data_chapter,
        );

        return $resp_array;
    }


    public function index()
    {
        $this->load->view('rest_server');
    }

    function insertBooks()
    {
        $this->load->library("REST_Controller");
        $this->load->model("Book_model", "books");
        $this->load->library("dobackground");
        $data = array();
        $data["author_id"] = $this->input->post('user_id');
        $data["category_id"] = $this->input->post('category');
        $data["title_book"] = $this->input->post('title_book');
        $data["file_cover"] = $this->input->post('file_cover');
        if (!empty($this->input->post("stat_book"))) $data["is_free"] = $this->input->post('stat_book');
        if (!empty($this->input->post("book_id"))) $data["book_id"] = $this->input->post('book_id');
        if (!empty($this->input->post("is_publish"))) $data["is_publish"] = $this->input->post('is_publish');
        $data_chapter = null;
        $data_phar = null;
        if (!empty($this->input->post("chapter_title"))) $data_chapter = $this->input->post('chapter_title');
        if (!empty($this->input->post('paragraph'))) $data_phar = $this->input->post('paragraph');
        // $this->some_model->insert_user( ... );
        $message = $this->books->insert_books($data, $data_chapter, $data_phar);
        if ($message["code"] == REST_Controller::HTTP_OK) {
            $url = site_url("book/Bg_process/saveDownload");
            $param = array(
                'book_id' => $message["data"]->book_id,
            );
            //background proses
            $this->dobackground->do_in_background($url, $param);
        }
    }

    function deleteFile()
    {
        $tmpFile = trim($this->input->post("filepath"));
        if (!empty($tmpFile) && file_exists($tmpFile)) {
            if (is_file($tmpFile)) @unlink(realpath($tmpFile));
            else rmdir(realpath($tmpFile));
        }
    }

    function saveDownload()
    {
        ini_set('memory_limit', '-1');
        $book_id = $this->input->post("book_id");
        $title_book = "";
        $author_id = "";
        $generateDate = date("Y-m-d H:i:s O");
        if (empty($book_id)) exit();
        # load library
        $cover_url = "";
        if (!empty($book_id)) {
            $gbb = $this->db->get_where("book", array("book_id" => $book_id));
            if ($gbb->num_rows() > 0) {
                $title_book = $gbb->row()->title_book;
                $author_id = $gbb->row()->author_id;
                $cover_url = $gbb->row()->file_cover;
            }
        }
        $authors = (!empty($author_id)) ? getUsersById($author_id) : array();
        $this->load->library('Pdfm', 'pdfm');
        $pdf = $this->pdfm->load();
        $data["pdf"] = $pdf;
        $datapassword = 'ID#' . $book_id . '#' . $title_book . '#' . strtotime($generateDate);
        $password = hash_hmac('sha512', $datapassword, strtotime($generateDate));
        $pdf->SetProtection(array(), 'username', $password);
        if (base_url() != "http://" . REST_Controller::api_prod && base_url() != "https://" . REST_Controller::api_prod) {
            $this->load->helper("file");
            $txtPath = "../uploads/pass.txt";
            if (file_exists($txtPath)) {
                write_file($txtPath, $book_id . "-" . $password . "\r\n", 'a');
            } else {
                write_file($txtPath, $book_id . "-" . $password . "\r\n");
            }
        }
        # retrieve data from model
        $data = $this->input->post();
        $fullname = (count($authors) > 0 && isset($authors["fullname"])) ? $authors["fullname"] : "";
//        $html = $this->load->view('content/mpdf', $data);
        $html = $this->load->view('content/mpdf', $data, true);
        $html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
        # render the view into HTML
        $pdf->SetTitle($title_book);
        if(!empty($cover_url)) {
            $pdf->AddPage('P','','','','',0,0,0,0,0,0);
            $cover = "<div style='text-align: center;'><img src='" . $cover_url . "' style='vertical-align:middle; height:2480px;padding:0;margin:0' /></div>";
            $pdf->WriteHTML($cover);
        }
        $pdf->AddPage('P','','','','',10,10,15,15,10,10);
        if(!empty($fullname)) {
            $judul = "<div style='height: 200px; width: 500px; text-align: center; position: fixed;top: 50%; left: 50%; margin-top: -100px; margin-left: -170px;'><h1 style='font-size:20em!important;'>$title_book</h1><br /><h1 style='font-size: 10em!important;'><i>$fullname</i></h1></div><pagebreak sheet-size='A5' />";
            $pdf->WriteHTML($judul);
        }

        $pdf->SetFooter('{PAGENO} / {nbpg}');
        $pdf->TOCpagebreakByArray(array(
            'tocfont' => '',
            'tocfontsize' => '',
            'tocindent' => '5',
            'TOCusePaging' => true,
            'TOCuseLinking' => '',
            'toc_orientation' => '',
            'toc_mgl' => '10',
            'toc_mgr' => '10',
            'toc_mgt' => '',
            'toc_mgb' => '',
            'toc_mgh' => '5',
            'toc_mgf' => '5',
            'toc_ohname' => '',
            'toc_ehname' => '',
            'toc_ofname' => '',
            'toc_efname' => '',
            'toc_ohvalue' => 0,
            'toc_ehvalue' => 0,
            'toc_ofvalue' => -1,
            'toc_efvalue' => -1,
            'toc_preHTML' => '<h1>Contents</h1><br />',
            'toc_postHTML' => '',
            'toc_bookmarkText' => 'Content list',
            'resetpagenum' => '1',
            'pagenumstyle' => '1',
            'suppress' => 'off',
            'orientation' => '',
            'mgl' => '',
            'mgr' => '',
            'mgt' => '5',
            'mgb' => '',
            'mgh' => '',
            'mgf' => '',
            'ohname' => '',
            'ehname' => '',
            'ofname' => '',
            'efname' => '',
            'ohvalue' => 0,
            'ehvalue' => 0,
            'ofvalue' => 0,
            'efvalue' => 0,
            'toc_id' => 0,
            'pagesel' => '',
            'toc_pagesel' => '',
            'sheetsize' => '',
            'toc_sheetsize' => ''
        ));
        $pdf->WriteHTML($html);
        # write the HTML into the PDF
        $output = $book_id . '_' . date('Y_m_d_H_i_s') . '_.pdf';
        $assetOutput = $this->DirAssets . $output;
        ob_clean();
        $tmpName = $this->Uploadir . $output;
        $pdfs = $pdf->Output($tmpName);
        if (file_exists($tmpName)) {
            if (base_url() == "http://" . REST_Controller::api_prod || base_url() == "https://" . REST_Controller::api_prod || base_url() == "http://" . REST_Controller::api_staging || base_url() == "https://" . REST_Controller::api_staging) {
                $fileName = "baboo-docs/" . $output;
                if ($this->s3->upload($fileName, $tmpName, $this->bucket)) {
                    $realpath = realpath(trim($this->Uploadir));
                    if (unlink(realpath($tmpName))) {
//                    $notif_text = "Buku anda berhasil dihapus";
                    } else {
//                    $notif_text = "Buku anda gagal dihapus ".realpath($tmpName);
                    }
                    $assetOutput = $this->s3->url($fileName, $this->bucket);
                    $upd = $this->db->query("UPDATE book SET  pdf_url = '" . $assetOutput . "', generated_date = '" . $generateDate . "' WHERE book_id = " . $book_id);
                    if ($upd) {
                        $notif_text = "Buku anda sudah dipublikasikan";
                        $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
                        $instys = $this->db->insert_id();
                    }else {
                        $notif_text = "Buku anda gagal dipublikasikan";
                        $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
                    }
                } else {
                    $notif_text = "Buku anda gagal dipublikasikan";
                    $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
                }
            }else{
                $upd = $this->db->query("UPDATE book SET  pdf_url = '" . $assetOutput . "', generated_date = '" . $generateDate . "' WHERE book_id = " . $book_id);
                if ($upd) {
                    $notif_text = "Buku anda sudah dipublikasikan";
                    $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
                    $instys = $this->db->insert_id();
                }else {
                    $notif_text = "Buku anda gagal dipublikasikan";
                    $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
                }
            }

            if($instys && !empty($instys)){
                $colls = $this->db->query("SELECT COUNT(id_user) as total_collection FROM collections WHERE id_book = ?", array($book_id));
                if($colls && $colls->num_rows() > 0 && $colls->row()->total_collection > 0) $this->db->update("collections",array("is_download"=>0), array("id_book"=>$book_id));
            }

        } else {
            $notif_text = "Buku anda gagal dipublikasikan";
            $this->db->insert("notifications", array("notif_to" => $author_id, "notif_type" => REST_Controller::notif_publish, "notif_text" => $notif_text, "notif_book" => $book_id));
        }

    }
}
