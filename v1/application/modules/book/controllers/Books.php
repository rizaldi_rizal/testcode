<?php
ini_set("date.timezone", "Asia/Jakarta");
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Books extends REST_Controller
{
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper("baboo");
//        $this->load->helper("htmlpurifier");
        $this->load->model("Book_model", "books");
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $a = base_url();
        $this->bucket = REST_Controller::bucket_staging;

        if (strpos($a, REST_Controller::api_prod) !== false) {
            $this->bucket = REST_Controller::bucket_prod;
        }

        $this->methods['books_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['books_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['books_put']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['books_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->ParentUploadir = "../../uploads/";
        $this->Dirdir = "/uploads/"; #for server use
        $this->Uploadir = $this->ParentUploadir . "baboo-cover/";
        $this->PDFUploadir = $this->ParentUploadir . "baboo-docs/";
        $this->Videodir = $this->ParentUploadir . "baboo-cover/";
//        $this->DUploadir = "D:\\work\\xampp\\htdocs\uploads\\baboo-cover\\";  #windows local version > path upload
        $this->DUploadir = $this->Dirdir . "baboo-cover/"; #for server use
        $this->thumbdir = $this->ParentUploadir . "baboo-thumbs/";
//        $this->thumbndir = "D:\\work\\xampp\\htdocs\uploads\\baboo-thumbs\\"; #windows local version > path taro thumbnail
        $this->thumbndir = $this->Dirdir . "baboo-thumbs/"; #for server use
        if (strpos($a, REST_Controller::api_dev) !== false || strpos($a, REST_Controller::api_local) !== false) {
            $this->DirAssets = (strpos($a, REST_Controller::api_dev) !== false) ? REST_Controller::assets_dev . "baboo-cover/" : "http://localhost/uploads/baboo-cover/";
            $this->PDFAssets = (strpos($a, REST_Controller::api_dev) !== false) ? REST_Controller::assets_dev . "baboo-docs/" : "http://localhost/uploads/baboo-docs/";
            if (!file_exists($this->Uploadir)) {
                mkdir($this->Uploadir, 0777);
            }
            if (!file_exists($this->thumbdir)) {
                mkdir($this->thumbdir, 0777);
            }
        } else {
            $this->load->library("CloudStorage", NULL, "s3");
        }
    }

    public function addComment_post()
    {
        $data_where = array();
        $data = array();
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $resp_message = "";
        $resp_data = array();
        $data_book = array();
        $author_id = "";
        $book_id = "";
        $last_query = "";
        if (!empty($this->post("book_id"))) {
            $data["id_book"] = $this->post('book_id');
            $data_where["id_book"] = $this->post('book_id');
            $this->db->where("book_id", $this->post("book_id"));
            $gbook = $this->db->get("book");
            $data_book = $gbook->row();
            if ($gbook->num_rows() > 0) {
                $book_id = $this->post("book_id");
                $author_id = $gbook->row()->author_id;
                $this->checkPublish($book_id);
            }
        }
        //checking
        if (!empty($this->post("paragraph_id"))) {
            $data["id_paragraph"] = $this->post('paragraph_id');
            $data_where["id_paragraph"] = $this->post('paragraph_id');
            $gphar = $this->db->query("SELECT * FROM book WHERE book_id IN(SELECT id_book FROM chapter WHERE chapter_id IN(SELECT id_chapter FROM paragraph WHERE paragraph_id = ?))", array($this->post("paragraph_id")));
            $data_book = $gphar->row();
            $book_id = (string)$gphar->row()->book_id;
            $author_id = (string)$gphar->row()->author_id;
            $this->checkPublish($book_id);
        }
        //still checking too
        if (!empty($this->post("comment_id"))) {
            $data["id_comment"] = $this->post('comment_id');
            $data_where["id_comment"] = $this->post('comment_id');
            $this->db->select("id_book, id_paragraph");
            $gcekcom = $this->db->get_where("comment", array("comment_id" => $this->post("comment_id")));
            if ($gcekcom->num_rows() > 0) {

                if (!empty($gcekcom->row()->id_book)) {
                    $book_id = $gcekcom->row()->id_book;
                    $this->db->select("author_id");
                    $gpharays = $this->db->get_where("book", array("book_id" => $book_id));
                    $author_id = (string)$gpharays->row()->author_id;
                } elseif (!empty($gcekcom->row()->id_paragraph)) {
                    $gpharays = $this->db->query("SELECT book_id, author_id FROM book WHERE book_id IN(SELECT id_book FROM chapter WHERE chapter_id IN(SELECT id_chapter FROM paragraph WHERE paragraph_id = ?))", array($this->post("paragraph_id")));
                    $book_id = (string)$gpharays->row()->book_id;
                    $author_id = (string)$gpharays->row()->author_id;
                }
            }
        }

        $data["comments"] = $this->post("comments");
        $data["created"] = $user_id;
        $names = "";
        $avatar = "";
        $times = date("Y-m-d H:i:s");
        $gusr = getUsersById($user_id);
        if (!empty($gusr)) {
            $names = $gusr['fullname'];
            $avatar = (string)$gusr['prof_pict'];
        }

        if (!empty($this->post("commentupdate_id"))) {
            $upd = $this->db->update("comment", $data, array("comment_id" => $this->post("commentupdate_id")));
            if ($upd) {
                $http_response_header = REST_Controller::HTTP_OK;
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = "edit comment success";
                $resp_data = array(
                    "comment_user" => $user_id,
                    "comment_user_name" => $names,
                    "comment_user_avatar" => $avatar,
                    "comment_id" => $this->post("commentupdate_id"),
                    "comment_text" => $data["comments"],
                    "comment_time" => time_ago($times)
                );
            } else {
                $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                $error_message = $this->db->error();
                $messageror = "";
                if (is_array($error_message)) $messageror = $error_message["code"] . " = " . $error_message["message"];
                $resp_message = "comment failed, because " . $messageror;
            }
        } else {
            $this->db->insert("comment", $data);
            $insid = $this->db->insert_id();
            if ($insid) {
                if (!empty($this->post("user_mention")) && is_array($this->post("user_mention")) && count($this->post("user_mention")) > 0) {
                    foreach ($this->post("user_mention") as $vasmen) {
                        $notifica_text = "menandakan anda pada komentarnya";
                        $arr_notifica = array(
                            "notif_type" => REST_Controller::notif_comment,
                            "notif_to" => $vasmen,
                            "notif_book" => $book_id,
                            "notif_by" => $user_id,
                            "notif_text" => $notifica_text
                        );
                        $this->db->insert("notifications", $arr_notifica);
                    }
                }
                $http_response_header = REST_Controller::HTTP_OK;
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = "add comment success";
                $resp_data = array(
                    "comment_user" => $user_id,
                    "comment_user_name" => $names,
                    "comment_user_avatar" => $avatar,
                    "comment_id" => $insid,
                    "comment_text" => $data["comments"],
                    "comment_time" => time_ago($times)
                );
                if ($author_id != $user_id) {
                    $this->db->group_by("created");
                    $this->db->where("(created != $author_id AND created != $user_id)");
                    // $this->db->where(array("created !=" => $user_id));
                    if (count($data_where) > 0) $this->db->where($data_where);
                    $gcomm = $this->db->get("comment");
//                    $print["error"] = $this->db->error();
//                    $print["last_query"] = $this->db->last_query();
//                    print_r($print);
//                    $prada = array();
                    if ($gcomm->num_rows() > 0) {
                        // $last_query = $this->db->last_query();
                        $auhor_names = "";
                        $gauth = getUsersById($author_id);
                        if (!empty($gauth)) $auhor_names = $gauth['fullname'];
                        foreach ($gcomm->result() as $gcval) {
                            $notifs_text = "juga mengomentari tulisan " . $auhor_names;
                            $arr_notif = array(
                                "notif_type" => REST_Controller::notif_comment,
                                "notif_to" => $gcval->created,
                                "notif_book" => $book_id,
                                "notif_by" => $user_id,
                                "notif_text" => $notifs_text
                            );
//                            $prada[] = $arr_notif;
                            $this->db->insert("notifications", $arr_notif);
                            $insids = $this->db->insert_id();
                            if ($insids) {
                                $teuser = getUsersById($gcval->created);
                                if (!empty($teuser)) $notifs_text = $names . " " . $notifs_text;
                                setpush_notif($gcval->created, $notifs_text, REST_Controller::type_comment, $book_id);
                            }
                        }
                    }
//                    print_r($prada);
                    $notif_text = " mengomentari tulisan anda";
                    $arr_notif = array(
                        "notif_type" => REST_Controller::notif_comment,
                        "notif_to" => $author_id,
                        "notif_book" => $book_id,
                        "notif_by" => $user_id,
                        "notif_text" => $notif_text
                    );
                    $this->db->insert("notifications", $arr_notif);
                    $insad = $this->db->insert_id();
                    if ($insad) {
                        $seuser = getUsersById($user_id);
                        if (!empty($seuser)) $notif_text = $seuser["fullname"] . " " . $notif_text;
                        setpush_notif($author_id, $notif_text, REST_Controller::type_comment, $book_id);
                    }
                }
            } else {
                $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                $error_message = $this->db->error();
                $messageror = "";
                if (is_array($error_message)) $messageror = $error_message["code"] . " = " . $error_message["message"];
                $resp_message = "comment failed, because " . $messageror;
            }
        }

        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$resp_data,
        );
        set_resp($responses, $http_response_header);
    }

    public function allChapters_post()
    {
        $book_id = $this->post("book_id");
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $resp_message = "get all chapter success";
        $data_chapter = array();
        $book_info = array();
        $chapters = array();
        $status_publish = 1;
        $this->db->where(array("book_id" => $book_id));
        $books = $this->db->get("book");
        $is_free = false;
        $is_bought = false;
        $is_download = false;
        $valid = true;
        $stat_publish = "publish";
        if ($books->num_rows() > 0) {
            $stat_publish = $books->row()->status_publish;
            $this->db->select("is_download");
            $cekBought = $this->db->get_where("collections", array("id_user" => $user_id, "id_book" => $book_id));
            if ($cekBought->num_rows() > 0) {
                $is_bought = true;
                if (isset($cekBought->row()->is_download) && $cekBought->row()->is_download == 1) $is_download = true;
            }
            if ($stat_publish == "on editor") $stat_publish = "edit publish";
            if ($books->row()->status_publish != "publish" && $books->row()->author_id == $user_id) {
                $status_publish = 2;
            }
            if ($books->row()->status_publish == "draft" && $books->row()->author_id != $user_id) {
                $valid = false;
            }
            //check if valid true
            if ($valid) {
                $count_comment = 0;
                $this->db->select("id_book");
                $getcomment = $this->db->get_where("comment", array("id_book" => $books->row()->book_id));
                if ($getcomment->num_rows() > 0) $count_comment = $getcomment->num_rows();
                $is_likes = false;
                $this->db->select("id_book");
                $getlikes = $this->db->get_where("likes", array("id_book" => $books->row()->book_id, "id_user" => $user_id));
                if ($getlikes->num_rows() > 0) {
                    $is_likes = true;
                }
                $is_bookmark = false;
                $this->db->where("id_user", $user_id);
                $this->db->where("id_book", $book_id);
                $this->db->select("id_book");
                $sbookmark = $this->db->get("bookmark");
                //validate and fill is_bookmark true when theres data on bookmark table
                if ($sbookmark->num_rows() > 0) $is_bookmark = true;
                if ($books->row()->is_free == "free" || $is_bought == true) {
                    $is_free = true;
                }

                $price = 0;
                if ($is_free == false) {
                    $price = (double)$books->row()->total_price;
                }
                $titleBook = $books->row()->title_book;
                if (!empty($books->row()->title_draft)) {
                    $titleBook = $books->row()->title_draft;
                }
                $book_info = array(
                    "book_id" => $books->row()->book_id,
                    "title_book" => (string)$titleBook,
                    "view_count" => (int)$books->row()->view_count,
                    "like_count" => (int)$books->row()->like_count,
                    "is_like" => $is_likes,
                    "is_bought" => $is_bought,
                    "is_download" => $is_download,
                    "is_free" => $is_free,
                    "book_price" => $price,
                    "is_bookmark" => $is_bookmark,
                    "share_count" => (int)$books->row()->share_count,
                    "book_comment_count" => (int)$count_comment,
                    "cover_url" => (string)$books->row()->file_cover
                );
            } else {
                $resp_code = REST_Controller::HTTP_FORBIDDEN;
                $resp_message = "unauthorized";
            }
        }
        $data_chapter["book_info"] = (object)$book_info;
        if ($status_publish == 1) $this->db->where("status_publish", $status_publish);
        $this->db->where("id_book", $book_id);
        $dcg = $this->db->get("chapter");
        if (empty($book_id)) {
            $resp_code = REST_Controller::HTTP_BAD_REQUEST;
            $resp_message = "book id cannot empty";
        }

        //check
        if ($dcg->num_rows() > 0 && $valid !== false) {
            $limit_text = REST_Controller::limitDescChapter;
            foreach ($dcg->result() as $dckey => $dcval) {
                $stat_draft = $dcval->status_publish;
                $stat_free = $dcval->status_free;
                $statey = "publish";
                if ($stat_draft > 1) {
                    if ($stat_draft == 2 && $stat_publish == "edit publish") $statey = "edit publish";
                    else $statey = "draft";
                }
                $res_par_text = "";
                $chapter_title = $dcval->chapter_title;
                if ($status_publish == 1) {
                    $gepar = $this->db->get_where("paragraph", array("id_chapter" => $dcval->chapter_id));
                } else {
                    if (!empty($dcval->chapter_draft)) $chapter_title = $dcval->chapter_draft;
                    $gepar = $this->db->get_where("draft_paragraph", array("id_chapter" => $dcval->chapter_id));
                    if ($gepar->num_rows() == 0) $gepar = $this->db->get_where("paragraph", array("id_chapter" => $dcval->chapter_id));
                }
                foreach ($gepar->result() as $par_result => $paragraph) {
                    $res_par_text .= strip_tags_with_whitespace($paragraph->txt_paragraph);
                    $limit_count = $gepar->num_rows() - 1;
                    if ($par_result != $limit_count) $res_par_text .= " ";
                    if (strlen($res_par_text) > $limit_text) {
                        $res_par_text = substr($res_par_text, 0, $limit_text) . "...";
                        break;
                    }
                }
                $data_free = false;
                if ($stat_free == "free" || $is_bought == true) {
                    $data_free = true;
                }
                $chapters[] = array(
                    "chapter_id" => $dcval->chapter_id,
                    "chapter_title" => $chapter_title,
                    "desc" => trim($res_par_text),
                    "status_publish" => setStats($statey),
                    "chapter_free" => $data_free
                );
            }
            // $data_chapter = $dcg->result_array();
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;

        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "chapter not found or unauthorized";
        }
        $data_chapter["chapter"] = $chapters;
        $resp_array = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$data_chapter,
        );
        set_resp($resp_array, $http_response_header);
    }

    public function allCategory_get()
    {
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $resp_message = "sucessfully get all category";
        $data_chapter = array();
        $this->db->select("id_catbook as category_id, cat_book as category_name");
        $this->db->where("stat_catbook", 0);
        $dcg = $this->db->get("book_category");
        if ($dcg->num_rows() > 0) {
            $data_chapter = $dcg->result_array();
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "category not found";
        }
        $resp_array = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (array)$data_chapter,
        );
        set_resp($resp_array, $http_response_header);
    }

    public function validatePublish_post()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_data = array();
        $book_id = $this->post("book_id");
        if (!empty($book_id)) {
            $this->db->where(array('book_id' => $book_id, "author_id" => $user_id));
            $gbook = $this->db->get("book");
            if ($gbook->num_rows() > 0) {
                $is_sellable = false;
                $is_publishable = false;
                $price = (string)$gbook->row()->price;
                $is_pdf = (string)$gbook->row()->is_pdf;
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = "get payment fee success";
                $res_par_text = "";
                $total_chap = 0;
                if ($is_pdf == 0) {
                    $this->load->helper("pdff");
                    $this->load->library("Fpdf_gen", "fpdf_gen");
                    $total_chap_sellable = REST_Controller::limitPDFJual;
                    $total_char_sellable = 0;
                    $pdf_url = (string)$gbook->row()->pdf_url;
                    $uploaddir = "../uploads/baboo-docs/";
                    $pdf = $uploaddir . "docs_" . time() . ".pdf";
                    file_put_contents($pdf, fopen($pdf_url, 'r'));
                    chmod($pdf, 0777);
                    if (file_exists($pdf)) {
                        $pdf_real = realpath($pdf);
                        $new_name = str_replace('.pdf', '', basename($pdf)) . "_decrypted.pdf";
                        $location_pdf = realpath($uploaddir) . "/$new_name";
                        $oldgenerateDate = (!empty($gbook->row()->generated_date)) ? date("Y-m-d H:i:s O", strtotime($gbook->row()->generated_date)) : date("Y-m-d H:i:s O");
                        $olddatapassword = 'ID#' . $book_id . '#' . $gbook->row()->title_book . '#' . strtotime($oldgenerateDate);
                        $oldpassword = hash_hmac('sha512', $olddatapassword, strtotime($oldgenerateDate));
                        exec("qpdf --decrypt $pdf_real --password=$oldpassword $location_pdf", $output, $return);
                        if (!$return) {
//                        $decrypted_pdf = $uploaddir.$new_name;
                            $newest_name = str_replace('.pdf', '', basename($pdf)) . "_1.4.pdf";
                            $locations_pdf = realpath($uploaddir) . "/$newest_name";
                            if (file_exists($uploaddir . $new_name)) chmod($uploaddir . $new_name, 0777);
                            $gs = "gs";
                            if (strpos(base_url(), REST_Controller::api_local) !== false) $gs = "gswin64c";
                            exec("$gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dBATCH -sOutputFile=$locations_pdf $location_pdf", $output, $return);
                            $total_chap = $this->fpdf_gen->countPage($uploaddir . $newest_name);
                            if (file_exists($pdf_real)) unlink($pdf_real);
                            if (file_exists($location_pdf)) unlink($location_pdf);
                            if (file_exists($locations_pdf)) unlink($locations_pdf);
                        }
                    }
                    if ($total_chap >= REST_Controller::limitPDFPublish) {
                        $is_publishable = true;
                        $resp_message = "your book is publish-able";
                        $total_chap_sellable = round((int)$total_chap * (REST_Controller::limitPDFJual / 100));
                        if ($total_chap >= $total_chap_sellable && $total_chap >= REST_Controller::limitPDFJualLimit) {
                            $is_sellable = true;
                        } else {
                            if ($total_chap < REST_Controller::limitPDFJual) {
                                $kurang_chap = REST_Controller::limitPDFJual - $total_chap;
                                $resp_message .= ", just need $kurang_chap more page(s) to sell your book";
                            }
                        }
                    } else {
                        $resp_message = "your book need more page to publish";
                    }

                } else {
                    $total_char_sellable = REST_Controller::limitSellableChar;
                    $total_chap_sellable = REST_Controller::limitSellable;
                    $schap = $this->db->get_where("chapter", array("id_book" => $book_id));
                    if ($schap && $schap->num_rows() > 0) {
                        $total_chap = $schap->num_rows();
                        $sval = $schap->result();
                        foreach ($sval as $svol) {
                            $gepar = $this->db->get_where("draft_paragraph", array("id_chapter" => $svol->chapter_id));
                            if ($gepar->num_rows() == 0) $gepar = $this->db->get_where("paragraph", array("id_chapter" => $svol->chapter_id));
                            foreach ($gepar->result() as $par_result => $paragraph) {
                                $res_par_text .= strip_tags_with_whitespace($paragraph->txt_paragraph);
                            }
                        }
                    } else {
                        if (!empty($this->post("paragraph"))) {
                            $res_par_text .= strip_tags_with_whitespace($this->post("paragraph"));
                        }
                    }

                    if (strlen($res_par_text) >= REST_Controller::limitPublish) {
                        $is_publishable = true;
                        $resp_message = "your book is publish-able";
                        if ($total_chap >= REST_Controller::limitSellable && strlen($res_par_text) >= REST_Controller::limitSellableChar) {
                            $is_sellable = true;
                        } else {
                            $less_chap = false;
                            if ($total_chap < REST_Controller::limitSellable) {
                                $less_chap = true;
                                $kurang_chap = REST_Controller::limitSellable - $total_chap;
                                $resp_message .= ", just need $kurang_chap more chapter(s)";
                            }
                            if (strlen($res_par_text) < REST_Controller::limitSellableChar) {
                                $kurang_laman = round(REST_Controller::limitSellableChar / REST_Controller::limitsellperPage) - ceil(strlen($res_par_text) / REST_Controller::limitsellperPage);
                                if ($less_chap == true) {
                                    $resp_message .= " and ";
                                } else {
                                    $resp_message .= ", just need ";
                                }
                                $resp_message .= $kurang_laman . " page(s) more";
                            }
                            $resp_message .= " to sell your book";
                        }
                    } else {
                        $resp_message = "your book need more characters to publish";
                    }
                }
                $resp_data["is_sellable"] = $is_sellable;
                $resp_data["total_chapter"] = $total_chap;
                $resp_data["total_chapter_sellable"] = $total_chap_sellable;
                $resp_data["char_total"] = strlen($res_par_text);
                $resp_data["char_total_sellable"] = $total_char_sellable;
                $resp_data["is_publishable"] = $is_publishable;
                $resp_data["payment_fee"] = REST_Controller::payment_fee;
                $resp_data["ppn"] = REST_Controller::tax_fee;
                $resp_data["writer_fee"] = REST_Controller::merchant_profit * 100;
                $resp_data["baboo_fee"] = REST_Controller::baboo_profit * 100;
                $resp_data["price"] = $price;
            } else {
                $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                $error = $this->db->error();
                $resp_message = "book not found because " . $error["code"] . ":" . $error["message"];
            }
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "book not found";
        }

        $response = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$resp_data
        );
        set_resp($response, $resp_code);
    }

    public function validatePDF_post()
    {
        $this->load->helper("pdff");
        $this->load->library("Fpdf_gen", "fpdf_gen");
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_data = array();
        $book_id = $this->post("book_id");
        if (!empty($book_id)) {
            $this->db->where(array('book_id' => $book_id, "author_id" => $user_id));
            $gbook = $this->db->get("book");
            if ($gbook->num_rows() > 0) {
                $price = (string)$gbook->row()->price;
                $pdf_url = (string)$gbook->row()->pdf_url;
                $uploaddir = "../uploads/baboo-docs/";
                $pdf = $uploaddir . "docs_" . time() . ".pdf";
                file_put_contents($pdf, fopen($pdf_url, 'r'));
                chmod($pdf, 0777);
                if (file_exists($pdf)) {
                    $pdf_real = realpath($pdf);
                    $new_name = str_replace('.pdf', '', basename($pdf)) . "_decrypted.pdf";
                    $location_pdf = realpath($uploaddir) . "/$new_name";
                    $oldgenerateDate = (!empty($gbook->row()->generated_date)) ? date("Y-m-d H:i:s O", strtotime($gbook->row()->generated_date)) : date("Y-m-d H:i:s O");
                    $olddatapassword = 'ID#' . $book_id . '#' . $gbook->row()->title_book . '#' . strtotime($oldgenerateDate);
                    $oldpassword = hash_hmac('sha512', $olddatapassword, strtotime($oldgenerateDate));
                    exec("qpdf --decrypt $pdf_real --password=$oldpassword $location_pdf", $output, $return);
                    if (!$return) {
//                        $decrypted_pdf = $uploaddir.$new_name;
                        $newest_name = str_replace('.pdf', '', basename($pdf)) . "_1.4.pdf";
                        $locations_pdf = realpath($uploaddir) . "/$newest_name";
                        if (file_exists($uploaddir . $new_name)) chmod($uploaddir . $new_name, 0777);
                        $gs = "gs";
                        if (strpos(base_url(), REST_Controller::api_local) !== false) $gs = "gswin64c";
                        exec("$gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dBATCH -sOutputFile=$locations_pdf $location_pdf", $output, $return);
                        $total_chap = $this->fpdf_gen->countPage($uploaddir . $newest_name);
                        if (file_exists($pdf_real)) unlink($pdf_real);
                        if (file_exists($location_pdf)) unlink($location_pdf);
                        if (file_exists($locations_pdf)) unlink($locations_pdf);
                    }
                }
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = "get payment fee success";
                $res_par_text = ($total_chap) ? $total_chap : 0;
                $is_sellable = false;
                $is_publishable = false;
                if ($res_par_text >= REST_Controller::limitPDFPublish) {
                    $is_publishable = true;
                    $resp_message = "your book is publish-able";
                    if ($total_chap >= REST_Controller::limitPDFJual) {
                        $is_sellable = true;
                    } else {
                        if ($total_chap < REST_Controller::limitPDFJual) {
                            $kurang_chap = REST_Controller::limitPDFJual - $total_chap;
                            $resp_message .= ", just need $kurang_chap more page(s) to sell your book";
                        }
                    }
                } else {
                    $resp_message = "your book need more page to publish";
                }
                $resp_data["is_sellable"] = $is_sellable;
                $resp_data["total_page"] = $total_chap;
                $resp_data["total_page_sellable"] = REST_Controller::limitPDFJual;
                $resp_data["is_publishable"] = $is_publishable;
                $resp_data["payment_fee"] = REST_Controller::payment_fee;
                $resp_data["ppn"] = REST_Controller::tax_fee;
                $resp_data["price"] = $price;
            } else {
                $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                $error = $this->db->error();
                $resp_message = "book not found because " . $error["code"] . ":" . $error["message"];
            }
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "book not found";
        }
        $response = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$resp_data
        );
        set_resp($response, $resp_code);
    }

    public function detailBook_post()
    {
        $book_id = $this->post("book_id");
        $data = $this->post();
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $this->db->select("book_id as id, category_id, price, total_price, is_free, book_type, view_count, like_count, share_count, title_book, author_id, IFNULL(file_cover,'') as cover_url, fullname as author, prof_pict as author_pict, status_publish, publish_at, latest_update, generated_date, pdf_url, is_pdf");
        $this->db->from("book");
        $this->db->join("users", "users.user_id = book.author_id");
        $this->db->where(array('book_id' => $data['book_id']));
        $prevQuery = $this->db->get();
        $prevCheck = ($prevQuery) ? $prevQuery->num_rows() : 0;
        $stat_publish = "publish";
        $author_id = "";
        $books = array();
        $http_response = REST_Controller::HTTP_ACCEPTED;
        if ($prevCheck > 0) {
            $stat_publish = $prevQuery->row()->status_publish;
            $author_id = $prevQuery->row()->author_id;
            $view_count = 1;
            $is_bookmark = false;
            $is_likes = false;
            $publish_date = $prevQuery->row()->latest_update;
            $is_follow = false;
            $is_download = false;
            $is_bought = false;
            $status_payment = "";
            if ($prevQuery->row()->status_publish != "draft") {
                $publish_date = $prevQuery->row()->publish_at;
                if (!empty($user_id)) {
                    $this->db->order_by("id","DESC");
                    $this->db->select("transaction_status");
                    $cekPayment = $this->db->get_where("midtrans", array("book_id" => $data['book_id'], "user_id" => $user_id));
                    if ($cekPayment->num_rows() > 0) {
                        if ($cekPayment->row()->transaction_status == REST_Controller::stat_settlement) $status_payment = REST_Controller::stat_settlement;
                        elseif ($cekPayment->row()->transaction_status != REST_Controller::stat_settlement && $cekPayment->row()->transaction_status != REST_Controller::stat_expire && $cekPayment->row()->transaction_status != REST_Controller::stat_cancel) $status_payment = REST_Controller::stat_pending;
                    }
                    $is_follow = checkIsFollow($prevQuery->row()->author_id, $user_id);
                    $this->db->select("is_download");
                    $cekBought = $this->db->get_where("collections", array("id_user" => $user_id, "id_book" => $data["book_id"]));
                    if ($cekBought->num_rows() > 0) {
                        $is_bought = true;
                        if (isset($cekBought->row()->is_download) && $cekBought->row()->is_download == 1) $is_download = true;
                    }
                    $is_likes = checkIsLike($book_id, $user_id);
                    $is_bookmark = checkIsBookmark($book_id, $user_id);

                }
            }
            $where_stat = ($user_id != $author_id) ? " AND status_publish = 1" : "";
            $dcgg = $this->db->query("SELECT chapter_title, (select count(chapter_id) from chapter where id_book = ?) as chapter_count, status_free FROM chapter WHERE id_book = ? AND status_free = 'free'$where_stat ORDER BY chapter_id ASC", array($book_id, $book_id));
            $generateDate = (!empty($prevQuery->row()->generated_date)) ? date("Y-m-d H:i:s O", strtotime($prevQuery->row()->generated_date)) : "";
            $latest_update = "";
            if (!empty($prevQuery->row()->latest_update) && $prevQuery->row()->latest_update != $prevQuery->row()->publish_at && $stat_publish == "publish") $latest_update = time_ago($prevQuery->row()->latest_update);
            $count_comment = 0;
            $getcomment = $this->db->get_where("comment", array("id_book" => $prevQuery->row()->id));
            if ($getcomment->num_rows() > 0) $count_comment = $getcomment->num_rows();
            $is_free = false;
            if ($prevQuery->row()->is_free == "free" || $is_bought !== false) {
                $is_free = true;
            }
            $price = 0;
            if ($is_free == false) {
                $price = (double)$prevQuery->row()->total_price;
            }
            $is_pdf = false;
            if ($prevQuery->row()->is_pdf == 0) $is_pdf = true;
            $last_page = 1;
            $this->db->select("last_page");
            $chistsq = $this->db->get_where("history_read", array("latest_book" => $data["book_id"], "reader" => $user_id));
            if($chistsq && $chistsq->num_rows() > 0){
                $last_page = (int)$chistsq->row()->last_page;
            }
            $prevResult = array(
                "book_info" => array(
                    "book_id" => $prevQuery->row()->id,
                    "title_book" => (string)$prevQuery->row()->title_book,
                    "view_count" => (int)$prevQuery->row()->view_count,
                    "like_count" => (int)$prevQuery->row()->like_count,
                    "is_bookmark" => $is_bookmark,
                    "is_bought" => $is_bought,
                    "is_download" => $is_download,
                    "epoch_time" => $generateDate,
                    "is_like" => $is_likes,
                    "book_type" => (int)$prevQuery->row()->book_type,
                    "is_pdf" => $is_pdf,
                    "is_free" => $is_free,
                    "chapter_sale" => ($dcgg && $dcgg->num_rows() > 0 && $dcgg->num_rows() != $dcgg->row()->chapter_count) ? ($dcgg->num_rows() + 1) : $dcgg->num_rows(),
                    "chapter_count" => ($dcgg && $dcgg->num_rows() > 0 && $dcgg->num_rows() != $dcgg->row()->chapter_count) ? ($dcgg->num_rows() + 1) : $dcgg->num_rows(),
                    "chapter_allcount" => ($dcgg && $dcgg->num_rows() > 0) ? (int)$dcgg->row()->chapter_count : 0,
                    "book_price" => $price,
                    "last_page" => $last_page,
                    "url_book" => (string)$prevQuery->row()->pdf_url,
                    "share_count" => (int)$prevQuery->row()->share_count,
                    "book_comment_count" => (int)$count_comment,
                    "latest_update" => $latest_update,
                    "publish_date" => time_ago($publish_date),
                    "cover_url" => $prevQuery->row()->cover_url,
                    "status_payment" => $status_payment
                ),
                "author" => array(
                    "author_id" => $prevQuery->row()->author_id,
                    "author_name" => $prevQuery->row()->author,
                    "avatar" => (string)$prevQuery->row()->author_pict,
                    "isFollow" => $is_follow
                )
            );

            $data_category = array();
            if (isset($data['chapter']) && !empty($data['chapter'])) {
                $this->db->where(array('chapter_id' => $data['chapter']));
            } else {
                $this->db->order_by("chapter_id", "asc");
            }
            $this->db->where(array("id_book" => $data["book_id"]));
            $gchap = $this->db->get("chapter");
            $paragraph = array();
            if ($gchap->num_rows() > 0) {
                $chav = $gchap->row();
                $chapters_id = $chav->chapter_id;
                if (!empty($chapters_id) && !empty($user_id)) {
                    $qwr_c = $this->db->query("SELECT COUNT(id_chapter) as total_chap FROM view_counts WHERE id_chapter = ? AND count_by = ?", array($chapters_id, $user_id));
                    if($qwr_c && $qwr_c->row()->total_chap == 0){
                        $arr_c = array(
                            "count_by"  => $user_id,
                            "id_book"  => $data["book_id"],
                            "id_chapter"  => $chapters_id,
                        );
                        $this->db->insert("view_counts", $arr_c);
                        $acid = $this->db->insert_id();
                        if($acid){
                            $data_update = array();
                            $this->db->select("view_count");
                            $cgs = $this->db->get_where("book", array("book_id" => $data["book_id"]));
                            if ($cgs->num_rows() > 0) $view_count = (int)$cgs->row()->view_count + 1;
                            $data_update['view_count'] = $view_count;
                            $this->db->update("book", $data_update, array('book_id' => $data["book_id"]));
                        }
                    }
                    $gethist = array("latest_book" => $data["book_id"], "reader" => $user_id);
                    $this->db->select("history_id");
                    $chist = $this->db->get_where("history_read", $gethist);
                    if ($chist->num_rows() > 0) {
                        $upd_data = array("latest_read" => date("Y-m-d H:i:s"));
                        if(!empty($this->post("last_page"))) $upd_data["last_page"] = $this->post("last_page");
                        $this->db->update("history_read", $upd_data, $gethist);
                    }
                    else {
                        if(!empty($this->post("last_page"))) $gethist["last_page"] = $this->post("last_page");
                        $this->db->insert("history_read", $gethist);
                    }
                }
                //checking part of urutan book
                $chapter_free = true;
                if ($is_free == false && $chav->status_free != "free" && $is_bought == false) {
                    $chapter_free = false;
                }
                $chaptitle = $chav->chapter_title;
                if (!empty($user_id) && $user_id == $author_id) {
                    if (!empty($chav->chapter_draft)) $chaptitle = $chav->chapter_draft;
                    $gpar = $this->db->get_where("draft_paragraph", array("id_chapter" => $chav->chapter_id));
                    if ($gpar->num_rows() == 0) $gpar = $this->db->get_where("paragraph", array("id_chapter" => $chav->chapter_id));
                } else {
                    $gpar = $this->db->get_where("paragraph", array("id_chapter" => $chav->chapter_id));
                }
                $paragraph = array(
                    "chapter_id" => $chav->chapter_id,
                    "chapter_title" => $chaptitle,
                    "chapter_free" => $chapter_free
                );
                $arr_par = array();
                if ($gpar->num_rows() > 0) {
                    foreach ($gpar->result() as $gpval) {
                        $par_text = $gpval->txt_paragraph;
                        if (substr($gpval->txt_paragraph, 0, 2) != "<p") {
                            $par_text = "<p>" . $gpval->txt_paragraph . "</p>";
                        } elseif (substr($gpval->txt_paragraph, -4) != "</p>") {
                            $par_text = $gpval->txt_paragraph . "</p>";
                        }
                        $count_commentp = 0;
                        $paragraph_id = $gpval->paragraph_id;
                        if (!empty($paragraph_id)) {
                            $getcommentp = $this->db->get_where("comment", array("id_paragraph" => $gpval->paragraph_id));
                            if ($getcommentp->num_rows() > 0) $count_commentp = $getcommentp->num_rows();
                        } else {
                            $paragraph_id = $gpval->id_draftpar;
                        }
                        $arr_par[] = array(
                            "paragraph_id" => $paragraph_id,
                            "paragraph_text" => $par_text,
                            "comment_count" => (int)$count_commentp
                        );
                    }
                }
                $paragraph["paragraphs"] = $arr_par;
            }
            $this->db->select("CAST(id_catbook AS UNSIGNED) AS category_id, cat_book AS category_name");
            if (!empty($prevQuery->row()->category_id)) $this->db->where("id_catbook", $prevQuery->row()->category_id);
            else $this->db->order_by("id_catbook", "asc");
            $dct = $this->db->get("book_category");
            if ($dct->num_rows() > 0) $data_category = $dct->row();
            $prevResult["category"] = (object)$data_category;
            $prevResult["chapter"] = $paragraph;
            //set response
            $books["code"] = REST_Controller::HTTP_OK;
            $books["message"] = "show detail book success";
            $books["data"] = $prevResult;
            $http_response = REST_Controller::HTTP_OK;
        } else {
            $bbid = (object)array();
            $books["code"] = REST_Controller::HTTP_NOT_FOUND;
            $books["message"] = "Book Not Found";
            $books["data"] = $bbid;
        }
        set_resp($books, $http_response);
    }

    public function detailPDF_post()
    {
        $book_id = $this->post("book_id");
        $data = $this->post();
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $this->db->select("book_id as id, category_id, price, total_price, book_type, is_free, view_count, like_count, share_count, title_book, author_id, IFNULL(file_cover,'') as cover_url, fullname as author, prof_pict as author_pict, status_publish, publish_at, latest_update, generated_date, is_pdf, pdf_url, pdf_free, pdf_start, total_page, descriptions");
        $this->db->from("book");
        $this->db->join("users", "users.user_id = book.author_id");
        $this->db->where(array('book_id' => $data['book_id']));
        $prevQuery = $this->db->get();
        $prevCheck = $prevQuery->num_rows();
        $stat_publish = "publish";
        $status_payment = "";
        $author_id = "";
        $books = array();
        $http_response = REST_Controller::HTTP_ACCEPTED;
        if ($prevCheck > 0) {
            $stat_publish = $prevQuery->row()->status_publish;
            $author_id = $prevQuery->row()->author_id;
            $view_count = 1;
            $is_bookmark = false;
            $is_likes = false;
            $publish_date = $prevQuery->row()->latest_update;
            $is_follow = false;
            $is_download = false;
            $is_pdf = true;
            if($prevQuery->row()->is_pdf == 1) $is_pdf = false;
            $is_bought = false;
            if ($prevQuery->row()->status_publish != "draft") {
                $publish_date = $prevQuery->row()->publish_at;
                if (!empty($user_id)) {
                    $this->db->order_by("id","DESC");
                    $this->db->select("transaction_status");
                    $cekPayment = $this->db->get_where("midtrans", array("book_id" => $data['book_id'], "user_id" => $user_id));
                    if ($cekPayment->num_rows() > 0) {
                        if ($cekPayment->row()->transaction_status == REST_Controller::stat_settlement) $status_payment = REST_Controller::stat_settlement;
                        elseif ($cekPayment->row()->transaction_status != REST_Controller::stat_settlement && $cekPayment->row()->transaction_status != REST_Controller::stat_expire && $cekPayment->row()->transaction_status != REST_Controller::stat_cancel) $status_payment = REST_Controller::stat_pending;
                    }
                    $cekfollow = $this->db->get_where("follows", array("created_by" => $user_id, "is_follow" => $prevQuery->row()->author_id));
                    if ($cekfollow->num_rows() > 0) $is_follow = true;
                    $cekBought = $this->db->get_where("collections", array("id_user" => $user_id, "id_book" => $data["book_id"]));
                    if ($cekBought->num_rows() > 0) {
                        $is_bought = true;
                        if (isset($cekBought->row()->is_download) && $cekBought->row()->is_download == 1) $is_download = true;
                    }
                    $is_likes = checkIsLike($book_id, $user_id);
                    $is_bookmark = checkIsBookmark($book_id, $user_id);
                    if (!empty($book_id) && !empty($user_id)) {
                        $qwr_c = $this->db->query("SELECT COUNT(id_book) as total_chap FROM view_counts WHERE id_book = ? AND count_by = ?", array($book_id, $user_id));
                        if($qwr_c && $qwr_c->row()->total_chap == 0){
                            $arr_c = array(
                                "count_by"  => $user_id,
                                "id_book"  => $data["book_id"],
                            );
                            $this->db->insert("view_counts", $arr_c);
                            $acid = $this->db->insert_id();
                            if($acid){
                                $data_update = array();
                                $this->db->select("view_count");
                                $cgs = $this->db->get_where("book", array("book_id" => $data["book_id"]));
                                if ($cgs->num_rows() > 0) $view_count = (int)$cgs->row()->view_count + 1;
                                $data_update['view_count'] = $view_count;
                                $this->db->update("book", $data_update, array('book_id' => $data["book_id"]));
                            }
                        }
                        $gethist = array("latest_book" => $data["book_id"], "reader" => $user_id);
                        $this->db->select("history_id");
                        $chist = $this->db->get_where("history_read", $gethist);
                        if ($chist->num_rows() > 0) {
                            $upd_data = array("latest_read" => date("Y-m-d H:i:s"));
                            if(!empty($this->post("last_page"))) $upd_data["last_page"] = $this->post("last_page");
                            $this->db->update("history_read", $upd_data, $gethist);
                        }
                        else {
                            if(!empty($this->post("last_page"))) $gethist["last_page"] = $this->post("last_page");
                            $this->db->insert("history_read", $gethist);
                        }
                    }
                }
            }
            $generateDate = (!empty($prevQuery->row()->generated_date)) ? date("Y-m-d H:i:s O", strtotime($prevQuery->row()->generated_date)) : "";
            $latest_update = "";
            if (!empty($prevQuery->row()->latest_update) && $prevQuery->row()->latest_update != $prevQuery->row()->publish_at && $stat_publish == "publish") $latest_update = time_ago($prevQuery->row()->latest_update);
            $count_comment = 0;
            $getcomment = $this->db->get_where("comment", array("id_book" => $prevQuery->row()->id));
            if ($getcomment->num_rows() > 0) $count_comment = $getcomment->num_rows();
            $is_free = false;
            $pdf_url = (!empty($prevQuery->row()->pdf_free)) ? $prevQuery->row()->pdf_free : $prevQuery->row()->pdf_url;
            if ($prevQuery->row()->is_free == "free" || $is_bought !== false) {
                $is_free = true;
                $pdf_url = $prevQuery->row()->pdf_url;
            }
            if ($user_id == $author_id) $pdf_url = $prevQuery->row()->pdf_url;
            $price = 0;
            if ($is_free == false) {
                $price = (double)$prevQuery->row()->total_price;
            }
            $last_page = 1;
            $this->db->select("last_page");
            $chistsq = $this->db->get_where("history_read", array("latest_book" => $data["book_id"], "reader" => $user_id));
            if($chistsq && $chistsq->num_rows() > 0){
                $last_page = (int)$chistsq->row()->last_page;
            }
            $prevResult = array(
                "book_info" => array(
                    "book_id" => $prevQuery->row()->id,
                    "title_book" => (string)$prevQuery->row()->title_book,
                    "view_count" => (int)$prevQuery->row()->view_count,
                    "like_count" => (int)$prevQuery->row()->like_count,
                    "is_bookmark" => $is_bookmark,
                    "is_bought" => $is_bought,
                    "is_download" => $is_download,
                    "epoch_time" => $generateDate,
                    "is_like" => $is_likes,
                    "is_free" => $is_free,
                    "book_type" => (int)$prevQuery->row()->book_type,
                    "is_pdf" => $is_pdf,
                    "chapter_sale" => (!empty($prevQuery->row()->pdf_start)) ? (int)$prevQuery->row()->pdf_start : 0,
                    "chapter_count" => (!empty($prevQuery->row()->total_page)) ? (int)$prevQuery->row()->total_page : 0,
                    "chapter_allcount" => (!empty($prevQuery->row()->total_page)) ? (int)$prevQuery->row()->total_page : 0,
                    "book_price" => $price,
                    "last_page" => $last_page,
                    "url_book" => (string)$pdf_url,
                    "share_count" => (int)$prevQuery->row()->share_count,
                    "book_comment_count" => (int)$count_comment,
                    "latest_update" => $latest_update,
                    "publish_date" => time_ago($publish_date),
                    "cover_url" => $prevQuery->row()->cover_url,
                    "status_payment" => $status_payment,
                    "desc" => (string)$prevQuery->row()->descriptions
                ),
                "author" => array(
                    "author_id" => $prevQuery->row()->author_id,
                    "author_name" => $prevQuery->row()->author,
                    "avatar" => (string)$prevQuery->row()->author_pict,
                    "isFollow" => $is_follow
                )
            );
            $data_category = array();
            $this->db->select("CAST(id_catbook AS UNSIGNED) AS category_id, cat_book AS category_name");
            if (!empty($prevQuery->row()->category_id)) $this->db->where("id_catbook", $prevQuery->row()->category_id);
            else $this->db->order_by("id_catbook", "asc");
            $dct = $this->db->get("book_category");
            if ($dct->num_rows() > 0) $data_category = $dct->row();
            $prevResult["category"] = (object)$data_category;
            //set response
            $books["code"] = REST_Controller::HTTP_OK;
            $books["message"] = "show detail book success";
            $books["data"] = $prevResult;
            $http_response = REST_Controller::HTTP_OK;
        } else {
            $bbid = (object)array();
            $books["code"] = REST_Controller::HTTP_NOT_FOUND;
            $books["message"] = "Book Not Found";
            $books["data"] = $bbid;
        }
        set_resp($books, $http_response);
    }

    public function editBook_post()
    {
        $book_id = $this->post("book_id");
        $data = $this->post();
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $this->db->select("book_id as id, category_id, price, total_price, is_free, view_count, like_count, share_count, title_book, title_draft, author_id, IFNULL(file_cover,'') as cover_url, fullname as author, prof_pict as author_pict, status_publish, publish_at, latest_update");
        $this->db->from("book");
        $this->db->join("users", "users.user_id = book.author_id");
        $this->db->where(array('book_id' => $data['book_id']));
        $prevQuery = $this->db->get();
        $prevCheck = $prevQuery->num_rows();
        $books = array();
        $http_response = REST_Controller::HTTP_ACCEPTED;
        if ($prevCheck > 0) {
            $is_bookmark = false;
            $is_likes = false;
            $publish_date = $prevQuery->row()->latest_update;
            if ($prevQuery->row()->status_publish != "draft") {
                $publish_date = $prevQuery->row()->publish_at;
            }
            $count_comment = 0;
            $getcomment = $this->db->get_where("comment", array("id_book" => $prevQuery->row()->id));
            if ($getcomment->num_rows() > 0) $count_comment = $getcomment->num_rows();
            $is_follow = false;
            $cekfollow = $this->db->get_where("follows", array("created_by" => $user_id, "is_follow" => $prevQuery->row()->author_id));
            if ($cekfollow->num_rows() > 0) $is_follow = true;
            $is_free = false;
            if ($prevQuery->row()->is_free == "free") {
                $is_free = true;
            }
            $price = 0;
            if ($is_free == false) {
                $price = (double)$prevQuery->row()->total_price;
            }
            $titleBook = (string)$prevQuery->row()->title_book;
            if (!empty($prevQuery->row()->title_draft)) $titleBook = $prevQuery->row()->title_draft;
            $prevResult = array(
                "book_info" => array(
                    "book_id" => $prevQuery->row()->id,
                    "title_book" => $titleBook,
                    "view_count" => (int)$prevQuery->row()->view_count,
                    "like_count" => (int)$prevQuery->row()->like_count,
                    "is_bookmark" => $is_bookmark,
                    "is_like" => $is_likes,
                    "is_free" => $is_free,
                    "book_price" => $price,
                    "share_count" => (int)$prevQuery->row()->share_count,
                    "book_comment_count" => (int)$count_comment,
                    "latest_update" => time_ago($prevQuery->row()->latest_update),
                    "publish_date" => time_ago($publish_date),
                    "cover_url" => $prevQuery->row()->cover_url
                ),
                "author" => array(
                    "author_id" => $prevQuery->row()->author_id,
                    "author_name" => $prevQuery->row()->author,
                    "avatar" => (string)$prevQuery->row()->author_pict,
                    "isFollow" => $is_follow
                )
            );

            $data_category = array();
            if (isset($data['chapter']) && !empty($data['chapter'])) {
                $this->db->where(array('chapter_id' => $data['chapter']));
            } else {
                $this->db->order_by("chapter_id", "asc");
            }
            $this->db->where(array("id_book" => $data["book_id"]));
            $gchap = $this->db->get("chapter");
            $paragraph = array();
            if ($gchap->num_rows() > 0) {
                $chav = $gchap->row();
                //checking part of urutan book
                $keyes = getIndexChapter($chav->id_book, $chav->chapter_id);
                $chapter_free = true;
                $limitfree = REST_Controller::limitFree;
                if ($is_free == false && $keyes >= $limitfree) {
                    $chapter_free = false;
                }
                $chaptitle = (string)$chav->chapter_title;
                if (!empty($chav->chapter_draft)) $chaptitle = $chav->chapter_draft;
                $paragraph = array(
                    "chapter_id" => $chav->chapter_id,
                    "chapter_title" => $chaptitle,
                    "chapter_free" => $chapter_free
                );
                $cgasal = 0;
                $gpar = $this->db->get_where("draft_paragraph", array("id_chapter" => $chav->chapter_id));
                if ($gpar->num_rows() > 0) {
                    $cgasal = $gpar->num_rows();
                    $gasal = $gpar->result();
                } else {
                    $gpar = $this->db->get_where("paragraph", array("id_chapter" => $chav->chapter_id));
                    if ($gpar->num_rows() > 0) {
                        $cgasal = $gpar->num_rows();
                        $gasal = $gpar->result();
                    }
                }
                // check if result > 0
                if ($cgasal > 0) {
                    foreach ($gasal as $gpval) {
                        if (!$gpval->paragraph_id) {
                            $gpval->paragraph_id = $gpval->id_draftpar;
                        }
                        $par_text = $gpval->txt_paragraph;
                        if (substr($gpval->txt_paragraph, 0, 2) != "<p") {
                            $par_text = "<p>" . $gpval->txt_paragraph . "</p>";
                        } elseif (substr($gpval->txt_paragraph, -4) != "</p>") {
                            $par_text = $gpval->txt_paragraph . "</p>";
                        }
                        $count_commentp = 0;
                        $getcommentp = $this->db->get_where("comment", array("id_paragraph" => $gpval->paragraph_id));
                        if ($getcommentp->num_rows() > 0) $count_commentp = $getcommentp->num_rows();
                        $paragraph["paragraphs"][] = array(
                            "paragraph_id" => $gpval->paragraph_id,
                            "paragraph_text" => $par_text,
                            "comment_count" => (int)$count_commentp
                        );
                    }
                }
            }
            $this->db->select("id_catbook as category_id, cat_book as category_name");
            if (!empty($prevQuery->row()->category_id)) $this->db->where("id_catbook", $prevQuery->row()->category_id);
            else $this->db->order_by("id_catbook", "asc");
            $dct = $this->db->get("book_category");
            if ($dct->num_rows() > 0) $data_category = $dct->row();
            $prevResult["category"] = (object)$data_category;
            $prevResult["chapter"] = $paragraph;
            //set response
            $books["code"] = REST_Controller::HTTP_OK;
            $books["message"] = "show detail book success";
            $books["data"] = $prevResult;
            $http_response = REST_Controller::HTTP_OK;
        } else {
            $bbid = (object)array();
            $books["code"] = REST_Controller::HTTP_NOT_FOUND;
            $books["message"] = "Book Not Found";
            $books["data"] = $bbid;
        }
        set_resp($books, $http_response);
    }

    public function detailEdit_post()
    {
        $book_id = $this->post("book_id");
        $data = $this->post();
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;

        $this->db->select("book_id as id, category_id, is_free, view_count, like_count, share_count, title_book, author_id, file_cover as cover_url, fullname as author, prof_pict as author_pict, status_publish");
        $this->db->from("book");
        $this->db->join("users", "users.user_id = book.author_id");

        $this->db->where(array('book_id' => $data['book_id'], "author_id" => $user_id));
        $prevQuery = $this->db->get();
        $prevCheck = $prevQuery->num_rows();

        $books = array();
        $http_response = REST_Controller::HTTP_ACCEPTED;
        if ($prevCheck > 0) {
            if (isset($data['chapter_id']) && !empty($data['chapter_id'])) {
                $this->db->where(array('chapter_id' => $data['chapter_id']));
            } else {
                $this->db->order_by("chapter_id", "asc");
            }
            $this->db->where(array("id_book" => $data["book_id"]));
            $gchap = $this->db->get("chapter");
            $paragraph = array();
            if ($gchap->num_rows() > 0) {
                $chav = $gchap->row();
                $chaptitle = $chav->chapter_title;
                if ($prevQuery->row()->status_publish != "publish" && !empty($chav->chapter_draft)) {
                    $chaptitle = $chav->chapter_draft;
                }
                $paragraph["chapter_title"] = $chaptitle;
                $res_par_text = "";
                $cgasal = 0;
                $gpar = $this->db->get_where("draft_paragraph", array("id_chapter" => $chav->chapter_id));
                if ($gpar->num_rows() > 0) {
                    $cgasal = $gpar->num_rows();
                    $gasal = $gpar->result();
                } else {
                    $gpar = $this->db->get_where("paragraph", array("id_chapter" => $chav->chapter_id));
                    if ($gpar->num_rows() > 0) {
                        $cgasal = $gpar->num_rows();
                        $gasal = $gpar->result();
                    }
                }
                if ($cgasal > 0) {
                    foreach ($gasal as $gpval) {
                        $par_text = $gpval->txt_paragraph;
                        if (substr($gpval->txt_paragraph, 0, 2) != "<p") {
                            $par_text = "<p>" . $gpval->txt_paragraph . "</p>";
                        } elseif (substr($gpval->txt_paragraph, -4) != "</p>") {
                            $par_text = $gpval->txt_paragraph . "</p>";
                        }
                        $res_par_text .= $par_text;
                    }
                }
                $paragraph["chapter_paragraph"] = $res_par_text;
            }
            $prevResult["chapter"] = $paragraph;

            $books["code"] = REST_Controller::HTTP_OK;
            $books["message"] = "successfully stored paragraph on edit";
            $books["data"] = $paragraph;
            $http_response = REST_Controller::HTTP_OK;
        } else {
            $bbid = (object)array();
            $books["code"] = REST_Controller::HTTP_NOT_FOUND;
            $books["message"] = "Book Not Found";
            $books["data"] = $bbid;
        }
        set_resp($books, $http_response);
    }

    public function detailEditPDF_post()
    {
        $data = $this->post();
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $this->db->select("book_id as id, category_id, is_free, view_count, like_count, share_count, title_book, author_id, file_cover as cover_url, fullname as author, prof_pict as author_pict, pdf_url, pdf_free, status_publish, descriptions");
        $this->db->from("book");
        $this->db->join("users", "users.user_id = book.author_id");
        $this->db->where(array('book_id' => $data['book_id'], "author_id" => $user_id));
        $prevQuery = $this->db->get();
        $prevCheck = $prevQuery->num_rows();
        $books = array();
        $http_response = REST_Controller::HTTP_ACCEPTED;
        if ($prevCheck > 0) {
            $paragraph["book_id"] = $prevQuery->row()->id;
            $paragraph["title"] = $prevQuery->row()->title_book;
            $paragraph["description"] = (string)$prevQuery->row()->descriptions;
            $paragraph["url_book"] = (string)$prevQuery->row()->pdf_url;
            $books["code"] = REST_Controller::HTTP_OK;
            $books["message"] = "successfully stored paragraph on edit";
            $books["data"] = $paragraph;
            $http_response = REST_Controller::HTTP_OK;
        } else {
            $bbid = (object)array();
            $books["code"] = REST_Controller::HTTP_NOT_FOUND;
            $books["message"] = "Book Not Found";
            $books["data"] = (object)array();
        }
        set_resp($books, $http_response);
    }

    public function bestBook_get()
    {
        $count = $this->get("count");
        if (empty($count)) $count = REST_Controller::limit_timeline;
        $this->db->limit($count, 0);
        $this->db->join("users", "book.author_id = users.user_id");
        $books = $this->db->get("book")->result();
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        if (!empty($count)) {
            // Check if the books data store contains book id(in case the database result returns NULL)
            if ($books) {
                $http_response_header = REST_Controller::HTTP_OK;
                $resps = array("code" => REST_Controller::HTTP_OK, "message" => "best book successfully stored", "data" => $books);
                // Set the response and exit
            } else {
                $resps = [
                    'code' => REST_Controller::HTTP_NOT_FOUND,
                    'message' => 'No books were found',
                    'data' => (object)array(),
                ];
                // Set the response and exit
            }
        } else {
            $resps = [
                'code' => REST_Controller::HTTP_ACCEPTED,
                'message' => 'something wrong',
                'data' => (object)array(),
            ];
        }
        set_resp($resps, $http_response_header); // return HTTP response code
    }

    public function draftDelete_post()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $book_id = $this->post("book_id");
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $resp_message = "draft book cannot be deleted";
        $resp_data = (object)array();
        $gbook = $this->db->get_where("book", array("book_id" => $book_id, "status_publish" => "draft", "author_id" => $user_id));
        if ($gbook->num_rows() > 0) {
            $gchap = $this->db->get_where("chapter", array("id_book" => $book_id));
            if ($gchap->num_rows() > 0) {
                foreach ($gchap->result() as $gchval) {
                    $gpar = $this->db->get_where("paragraph", array("id_chapter" => $gchval->chapter_id));
                    if ($gpar->num_rows() > 0) {
                        $this->db->delete("paragraph", array("id_chapter" => $gchval->chapter_id));
                    }
                }
                $this->db->delete("chapter", array("id_book" => $book_id));
            }
            $gdbb = $this->db->delete("book", array("book_id" => $book_id));
            if ($gdbb) {
                $resp_message = "Book has been deleted";
                $resp_code = REST_Controller::HTTP_OK;
                $http_response_header = REST_Controller::HTTP_OK;
            }
        }
        $resps = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($resps, $http_response_header);
    }

    public function deleteBook_post()
    {
        $book_id = $this->post("book_id");
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $resp_message = "book not exist or already deleted";
        $resp_data = (object)array();
        $gbook = $this->db->get_where("book", array("book_id" => $book_id, "author_id" => $user_id));
        if ($gbook->num_rows() > 0) {
            $gcomment = $this->db->get_where("comment", array("id_book" => $book_id));
            if ($gcomment->num_rows() > 0) {
                $this->db->delete("comment", array("id_book" => $book_id));
            }
            $glike = $this->db->get_where("likes", array("id_book" => $book_id));
            if ($glike->num_rows() > 0) {
                $this->db->delete("likes", array("id_book" => $book_id));
            }
            $gbm = $this->db->get_where("bookmark", array("id_book" => $book_id));
            if ($gbm->num_rows() > 0) {
                $this->db->delete("bookmark", array("id_book" => $book_id));
            }
            $gnot = $this->db->get_where("notifications", array("notif_book" => $book_id));
            if ($gnot->num_rows() > 0) {
                $this->db->delete("notifications", array("notif_book" => $book_id));
            }
            $gchap = $this->db->get_where("chapter", array("id_book" => $book_id));
            if ($gchap->num_rows() > 0) {
                foreach ($gchap->result() as $gchval) {
                    $gcommentp = $this->db->get_where("comment", "id_paragraph IN (SELECT paragraph_id FROM paragraph WHERE id_chapter =  " . $gchval->chapter_id . ")");
                    if ($gcommentp->num_rows() > 0) {
                        $this->db->delete("comment", "id_paragraph IN (SELECT paragraph_id FROM paragraph WHERE id_chapter =  " . $gchval->chapter_id . ")");
                    }
                    $gpar = $this->db->get_where("paragraph", array("id_chapter" => $gchval->chapter_id));
                    if ($gpar->num_rows() > 0) {
                        $this->db->delete("paragraph", array("id_chapter" => $gchval->chapter_id));
                    }
                }
                $this->db->delete("chapter", array("id_book" => $book_id));
            }
            $gdbb = $this->db->delete("book", array("book_id" => $book_id));
            if ($gdbb) {
                $resp_message = "Book has been deleted";
                $resp_code = REST_Controller::HTTP_OK;
                $http_response_header = REST_Controller::HTTP_OK;
            }
        }
        $resps = array(
            "code" => $resp_code,
            "message" => $resp_message
        );
        set_resp($resps, $http_response_header);
    }

    public function chapterDelete_post()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $chapter_id = $this->post("chapter_id");
        $http_response_header = REST_Controller::HTTP_OK;
        $resp_data = (object)array();
        $delas = 0;
        if (!empty($chapter_id) && is_array($chapter_id) && count($chapter_id) > 0) {
            $charray = implode(",", $chapter_id);
            $where = "WHERE chapter_id IN ($charray)";
            $this->db->where("book_id IN(SELECT id_book FROM chapter $where)");
            $this->db->where("author_id", $user_id);
            $gchapbook = $this->db->get("book");
            if ($gchapbook && $gchapbook->num_rows() > 0) {
                foreach ($chapter_id as $chapk => $chapv) {
                    $gchap = $this->db->get_where("chapter", array("chapter_id" => $chapv));
                    if ($gchap->num_rows() > 0) {
                        foreach ($gchap->result() as $gchval) {
                            $gcommentp = $this->db->get_where("comment", "id_paragraph IN (SELECT paragraph_id FROM paragraph WHERE id_chapter =  " . $gchval->chapter_id . ")");
                            if ($gcommentp->num_rows() > 0) {
                                $this->db->delete("comment", "id_paragraph IN (SELECT paragraph_id FROM paragraph WHERE id_chapter =  " . $gchval->chapter_id . ")");
                            }
                            $gpar = $this->db->get_where("paragraph", array("id_chapter" => $gchval->chapter_id));
                            if ($gpar->num_rows() > 0) {
                                $this->db->delete("paragraph", array("id_chapter" => $gchval->chapter_id));
                            }
                        }
                        $gdbb = $this->db->delete("chapter", array("chapter_id" => $chapv));
                        if ($gdbb) {
                            $delas++;
                        }
                    }
                }
                if (!empty($delas)) {
                    $resp_message = "chapter has been deleted";
                    $resp_code = REST_Controller::HTTP_OK;
                } else {
                    $http_response_header = REST_Controller::HTTP_ACCEPTED;
                    $resp_message = "one or more of chapters are not exist or something happen with delete chapter";
                    $resp_code = REST_Controller::HTTP_NOT_FOUND;
                }
            } else {
                $http_response_header = REST_Controller::HTTP_FORBIDDEN;
                $resp_message = "unauthorized";
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
            }
        } else {
            $http_response_header = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "no chapter requested or chapter must be an array";
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
        }
        $resps = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($resps, $http_response_header);
    }

    public function commentDelete_post()
    {
        $comment_id = $this->post("comment_id");
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $resp_message = "comment failed to deleted";
        $resp_data = (object)array();
        $gbook = $this->db->get_where("comment", array("comment_id" => $comment_id));
        if ($gbook->num_rows() > 0) {
            $gchap = $this->db->get_where("comment", array("id_comment" => $comment_id));
            if ($gchap->num_rows() > 0) {
                $this->db->delete("comment", array("id_comment" => $comment_id));
            }
            $gdbb = $this->db->delete("comment", array("comment_id" => $comment_id));
            if ($gdbb) {
                $resp_message = "comment has been deleted";
                $resp_code = REST_Controller::HTTP_OK;
                $http_response_header = REST_Controller::HTTP_OK;
            }
        }
        $resps = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($resps, $http_response_header);
    }

    public function latestRead_post()
    {
        $limit = REST_Controller::limitLatestRead;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;
        $key = sethead();
        $reader = (!empty($key)) ? getUserFromKey($key) : 0;
        $this->db->limit(REST_Controller::limit_timeline, $count);
        $this->db->order_by("latest_read", "desc");
        $this->db->where("reader", $reader);
        $this->db->join("history_read", "history_read.latest_book = book.book_id");
        $this->db->join("users", "book.author_id = users.user_id");
        $books = $this->db->get("book");
        // Check if the books data store contains book id(in case the database result returns NULL)
        if ($books->num_rows() > 0) {
            $limit_text = REST_Controller::text_limits;
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_data = array();
            foreach ($books->result() as $bokeys => $boval) {
                $bokeks = $bokeys + 1;
                $is_bookmark = checkIsBookmark($boval->book_id, $reader);
                $is_likes = checkIsLike($boval->book_id, $reader);
                if ($boval->is_pdf == 1) $res_par_text = getParagraphByBook($boval->book_id);
                else $res_par_text = (string)$boval->descriptions;
                $getComment = $this->db->get_where("comment", array("id_book" => $boval->book_id));
                $category = getCategoryBook($boval->category_id);
                $is_download = false;
                $this->db->select("is_download");
                $cekBought = $this->db->get_where("collections", array("id_user" => $reader, "id_book" => $boval->book_id));
                if ($cekBought->num_rows() > 0) {
                    if (isset($cekBought->row()->is_download) && $cekBought->row()->is_download == 1) $is_download = true;
                }
                $latest_update = "";
                if (!empty($boval->latest_update) && $boval->latest_update != $boval->publish_at && $boval->status_publish == "publish") $latest_update = time_ago($boval->latest_update);
                $dasray = array(
                    "book_id" => $boval->book_id,
                    "author_id" => $boval->author_id,
                    "author_name" => $boval->fullname,
                    "author_avatar" => (string)$boval->prof_pict,
                    "cover_url" => (string)$boval->file_cover,
                    "image_url" => (string)$boval->image_url,
                    "title_book" => (string)$boval->title_book,
                    "category" => $category,
                    "is_like" => $is_likes,
                    "is_pdf" => (!empty($boval->is_pdf)) ? false : true,
                    "is_bookmark" => $is_bookmark,
                    "is_download" => $is_download,
                    "latest_update" => $latest_update,
                    "publish_date" => time_ago($boval->latest_read),
                    "like_count" => (int)$boval->like_count,
                    "comment_count" => $getComment->num_rows(),
                    "view_count" => (int)$boval->view_count,
                    "share_count" => (int)$boval->share_count,
                    "desc" => $res_par_text
                );

                $resp_data[] = $dasray;
            }
            $resps = array(
                "code" => REST_Controller::HTTP_OK,
                "message" => "latest read shown",
                "data" => $resp_data
            );
            // Set the response and exit
        } else {
            $http_response_header = REST_Controller::HTTP_OK;
            $resps = [
                'code' => REST_Controller::HTTP_NOT_FOUND,
                'message' => 'You dont read anything yet here',
                'data' => array(),
            ];
            // Set the response and exit
        }

        set_resp($resps, $http_response_header); // OK (200) being the HTTP response code
    }

    public function all_latestRead_get()
    {
        $key = sethead();
        $reader = (!empty($key)) ? getUserFromKey($key) : 0;
        $this->db->order_by("latest_read", "desc");
        $this->db->where("reader", $reader);
        $this->db->join("history_read", "history_read.latest_book = book.book_id");
        $this->db->join("users", "book.author_id = users.user_id");
        $books = $this->db->get("book");
        // Check if the books data store contains book id(in case the database result returns NULL)
        if ($books->num_rows() > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_data = array();
            foreach ($books->result() as $bokeys => $boval) {
                $bokeks = $bokeys + 1;
                if ($boval->is_pdf == 1) $res_par_text = getParagraphByBook($boval->book_id);
                else $res_par_text = (string)$boval->descriptions;
                $is_bookmark = checkIsBookmark($boval->book_id, $reader);
                $is_likes = checkIsLike($boval->book_id, $reader);
                $getComment = $this->db->get_where("comment", array("id_book" => $boval->book_id));
                $category = getCategoryBook($boval->category_id);
                $is_download = false;
                $this->db->select("is_download");
                $cekBought = $this->db->get_where("collections", array("id_user" => $reader, "id_book" => $boval->book_id));
                if ($cekBought->num_rows() > 0) {
                    if (isset($cekBought->row()->is_download) && $cekBought->row()->is_download == 1) $is_download = true;
                }
                $latest_update = "";
                if (!empty($boval->latest_update) && $boval->latest_update != $boval->publish_at && $boval->status_publish == "publish") $latest_update = time_ago($boval->latest_update);
                $dasray = array(
                    "book_id" => $boval->book_id,
                    "author_id" => $boval->author_id,
                    "author_name" => $boval->fullname,
                    "author_avatar" => (string)$boval->prof_pict,
                    "cover_url" => (string)$boval->file_cover,
                    "image_url" => (string)$boval->image_url,
                    "title_book" => (string)$boval->title_book,
                    "category" => $category,
                    "is_like" => $is_likes,
                    "is_bookmark" => $is_bookmark,
                    "is_download" => $is_download,
                    "is_pdf" => (!empty($boval->is_pdf)) ? false : true,
                    "latest_update" => $latest_update,
                    "publish_date" => time_ago($boval->latest_read),
                    "like_count" => (int)$boval->like_count,
                    "comment_count" => $getComment->num_rows(),
                    "view_count" => (int)$boval->view_count,
                    "share_count" => (int)$boval->share_count,
                    "desc" => $res_par_text
                );

                $resp_data[] = $dasray;
            }
            $resps = array(
                "code" => REST_Controller::HTTP_OK,
                "message" => "latest read shown",
                "data" => $resp_data
            );
            // Set the response and exit
        } else {
            $http_response_header = REST_Controller::HTTP_OK;
            $resps = [
                'code' => REST_Controller::HTTP_NOT_FOUND,
                'message' => 'You dont read anything yet here',
                'data' => array(),
            ];
            // Set the response and exit
        }

        set_resp($resps, $http_response_header); // OK (200) being the HTTP response code
    }

    public function uploadPDF_post()
    {
        $data = array();
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resps = array();
        if (!empty($user_id)) $data["user_id"] = $user_id;
        $a = base_url();
        if (strpos($a, REST_Controller::api_dev) !== false || strpos($a, REST_Controller::api_local) !== false) {
            $config['upload_path'] = $this->PDFUploadir;
            $config['allowed_types'] = 'epub|pdf';
            $config['file_name'] = (!empty($this->post("is_free")) && $this->post("is_free") == "1") ? 'docs_' . $this->post("book_id").'_free' : 'docs_' . $this->post("book_id");
            //$config['max_size'] = 25000;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('pdf_book')) {
                $this->response(array('code' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'message' => strip_tags($this->upload->display_errors()), 'data' => (object)array()), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            } else {
                if (!empty($this->post("book_id"))) {
                    $where_id = array("book_id" => $this->post('book_id'));
                    $upload = $this->upload->data();
                    $cover_url = array();
                    $assetscover = $this->PDFAssets . $upload["file_name"];
                    if($upload["file_ext"] == ".pdf") {
                        $cover_url["is_pdf"] = 0;
                        $cover_url["book_type"] = REST_Controller::book_type_pdf;
                    }
                    else{
                        $cover_url["book_type"] = REST_Controller::book_type_epub;
                    }
                    if (!empty($this->post("is_free")) && $this->post("is_free") == "1") $cover_url["pdf_free"] = $assetscover;
                    else $cover_url["pdf_url"] = $assetscover;
                    $update = $this->db->update("book", $cover_url, $where_id);
                    if ($update) {
                        $assetas["book_id"] = $this->post('book_id');
                        $http_response_header = REST_Controller::HTTP_OK;
                        $resps = array('code' => REST_Controller::HTTP_OK, 'message' => 'Upload '.substr($upload["file_ext"], 1).' Success', 'data' => (object)$assetas);
                    } else {
                        $http_response_header = REST_Controller::HTTP_ACCEPTED;
                        $resps = array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "Error Input to DB Image", "data" => (object)array());
                    }
                } else {
                    $http_response_header = REST_Controller::HTTP_ACCEPTED;
                    $resps = array("code" => REST_Controller::HTTP_NOT_FOUND, "message" => "document not found", "data" => (object)array());
                }
            }
        } else {
            if (!empty($_FILES) && $_FILES["pdf_book"]["error"] != 4) {
                $files = $_FILES["pdf_book"]["name"];
                $ext = pathinfo($files, PATHINFO_EXTENSION);
                $tmpName = $_FILES["pdf_book"]["tmp_name"];
                $fileName = "baboo-docs/docs_" . time() . ".$ext";
                //check if uploaded
                if (strtolower($ext) == "pdf" || strtolower($ext) == "epub") {
                    if ($this->s3->upload($fileName, $tmpName, $this->bucket)) {
                        $assetscover = $this->s3->url($fileName, $this->bucket);
                        $book_id = $this->post('book_id');
                        $gbook = $this->db->get_where("book", array("book_id" => $book_id, "author_id" => $user_id));
                        if (!empty($this->post("book_id")) && $gbook->num_rows() > 0) {
                            $where_id = array("book_id" => $this->post('book_id'));
                            if(strtolower($ext) == "pdf") {
                                $cover_url["is_pdf"] = 0;
                                $cover_url["book_type"] = REST_Controller::book_type_pdf;
                            }else{
                                $cover_url["book_type"] = REST_Controller::book_type_epub;
                            }
                            if (!empty($this->post("is_free")) && $this->pos("is_free") == "1") $cover_url["pdf_free"] = $assetscover;
                            else $cover_url["pdf_url"] = $assetscover;
                            $update = $this->db->update("book", $cover_url, $where_id);
                            if ($update) {
                                $http_response_header = REST_Controller::HTTP_OK;
                                $resps = array('code' => REST_Controller::HTTP_OK, 'message' => 'upload '.$ext.' success', 'data' => (object)array("book_id" => $book_id));
                            } else {
                                $http_response_header = REST_Controller::HTTP_ACCEPTED;
                                array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "error input to db cover", "data" => $this->db->last_query());
                            }
                        } else {
                            $http_response_header = REST_Controller::HTTP_ACCEPTED;
                            $resps = array("code" => REST_Controller::HTTP_NOT_FOUND, "message" => "book cannot be empty or unauthorized", "data" => (object)array());
                        }
                    } else {
                        $http_response_header = REST_Controller::HTTP_ACCEPTED;
                        $resps = array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "Error upload document", "data" => (object)array());
                    }
                }else {
                    $http_response_header = REST_Controller::HTTP_ACCEPTED;
                    $resps = array("code" => REST_Controller::HTTP_MULTIPLE_CHOICES, "message" => "only pdf or epub file allowed", "data" => (object)array());
                }
            } else {
                $http_response_header = REST_Controller::HTTP_ACCEPTED;
                $resps = array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "error upload, file not exist", "data" => (object)array());
            }
        }
        set_resp($resps, $http_response_header);
    }


    public function uploadImage_post()
    {
        $data = array();
        $book_id = $this->post("book_id");
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        if (!empty($user_id)) $data["user_id"] = $user_id;
        $a = base_url();
        if (strpos($a, REST_Controller::api_dev) !== false || strpos($a, REST_Controller::api_local) !== false) {
            $config['upload_path'] = $this->Uploadir;
            $config['allowed_types'] = 'gif|bmp|jpg|jpeg|png';
            $config['max_size'] = 8000;
            //in case you want to add something like limited size to perfect your social media
            //$config['max_width']            = 1024;
            //$config['max_height']           = 768;
            if (!empty($this->post("is_cover")) && $this->post("is_cover") == "true") {
                $config["file_name"] = "cover_" . time();
            } else {
                $config["file_name"] = "image_" . time();
            }
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image_url')) {
                $this->response(array('code' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'message' => strip_tags($this->upload->display_errors()), 'data' => (object)array()), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            } else {
                $book_id = $this->post('book_id');
                $gbook = $this->db->get_where("book", array("book_id" => $book_id, "author_id" => $user_id));
                if (!empty($this->post("book_id")) && $gbook->num_rows() > 0) {
                    $where_id = array("book_id" => $book_id);
                    $upload = $this->upload->data();
                    $cover_url = array();
                    $assetscover = $this->DirAssets . $upload["file_name"];
                    if (!empty($this->post("is_cover")) && $this->post("is_cover") == "true") {
                        $cover_url["file_cover"] = $assetscover;
                        $update = $this->db->update("book", $cover_url, $where_id);
                        if ($update) {
                            $http_response_header = REST_Controller::HTTP_OK;
                            $resps = array('code' => REST_Controller::HTTP_OK, 'message' => 'Upload Cover Success', 'data' => (object)array("asset_url" => $cover_url["file_cover"]));
                        } else {
                            $http_response_header = REST_Controller::HTTP_ACCEPTED;
                            array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "Error Input to DB Cover", "data" => $this->db->last_query());
                        }
                    } else {
                        $getbook = $this->db->get_where("book", $where_id);
                        if ($getbook->num_rows() && !empty($getbook->row()->image_url)) {
                            $http_response_header = REST_Controller::HTTP_OK;
                            $resps = array('code' => REST_Controller::HTTP_OK, 'message' => 'Upload Image Success', 'data' => (object)array("asset_url" => $assetscover));
                        } else {
                            $cover_url["image_url"] = $assetscover;
                            $update = $this->db->update("book", $cover_url, $where_id);
                            if ($update) {
                                $http_response_header = REST_Controller::HTTP_OK;
                                $resps = array('code' => REST_Controller::HTTP_OK, 'message' => 'Upload Image Success', 'data' => (object)array("asset_url" => $cover_url["image_url"]));
                            } else {
                                $http_response_header = REST_Controller::HTTP_ACCEPTED;
                                $resps = array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "Error Input to DB Image", "data" => (object)array());
                            }
                        }
                    }
                } else {
                    $http_response_header = REST_Controller::HTTP_NOT_FOUND;
                    $resps = array("code" => REST_Controller::HTTP_NOT_FOUND, "message" => "Book not exist or unauthorized upload image for books", "data" => (object)array());
                }
            }
        } else {
            if (!empty($_FILES) && $_FILES["image_url"]["error"] != 4) {
                $files = $_FILES["image_url"]["name"];
                $ext = $ext = pathinfo($files, PATHINFO_EXTENSION);
                $tmpName = $_FILES["image_url"]["tmp_name"];
                $fileName = "baboo-cover/image_" . time() . ".$ext";
                if (!empty($this->post("is_cover")) && $this->post("is_cover") == "true") {
                    $fileName = "baboo-cover/cover_" . time() . ".$ext";
                }
                //check if uploaded
                if ($this->s3->upload($fileName, $tmpName, $this->bucket)) {
                    $assetscover = $this->s3->url($fileName, $this->bucket);
                    $book_id = $this->post('book_id');
                    $gbook = $this->db->get_where("book", array("book_id" => $book_id, "author_id" => $user_id));
                    if (!empty($this->post("book_id")) && $gbook->num_rows() > 0) {
                        $where_id = array("book_id" => $this->post('book_id'));
                        if (!empty($this->post("is_cover")) && $this->post("is_cover") == "true") {
                            $cover_url["file_cover"] = $assetscover;
                            $update = $this->db->update("book", $cover_url, $where_id);
                            if ($update) {
                                $http_response_header = REST_Controller::HTTP_OK;
                                $resps = array('code' => REST_Controller::HTTP_OK, 'message' => 'Upload Cover Success', 'data' => (object)array("asset_url" => $cover_url["file_cover"]));
                            } else {
                                $http_response_header = REST_Controller::HTTP_ACCEPTED;
                                array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "Error Input to DB Cover", "data" => $this->db->last_query());
                            }
                        } else {
                            $getbook = $this->db->get_where("book", $where_id);
                            if ($getbook->num_rows() && !empty($getbook->row()->image_url)) {
                                $http_response_header = REST_Controller::HTTP_OK;
                                $resps = array('code' => REST_Controller::HTTP_OK, 'message' => 'Upload Image Success', 'data' => (object)array("asset_url" => $assetscover));
                            } else {
                                $cover_url["image_url"] = $assetscover;
                                $update = $this->db->update("book", $cover_url, $where_id);
                                if ($update) {
                                    $http_response_header = REST_Controller::HTTP_OK;
                                    $resps = array('code' => REST_Controller::HTTP_OK, 'message' => 'Upload Image Success', 'data' => (object)array("asset_url" => $cover_url["image_url"]));
                                } else {
                                    $http_response_header = REST_Controller::HTTP_ACCEPTED;
                                    $resps = array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "Error Input to DB Image", "data" => (object)array());
                                }
                            }
                        }
                    } else {
                        $http_response_header = REST_Controller::HTTP_ACCEPTED;
                        $resps = array("code" => REST_Controller::HTTP_NOT_FOUND, "message" => "Book cannot be empty or unauthorized", "data" => (object)array());
                    }
                } else {
                    $http_response_header = REST_Controller::HTTP_ACCEPTED;
                    $resps = array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "Error upload Image", "data" => (object)array());
                }
            } else {
                $http_response_header = REST_Controller::HTTP_ACCEPTED;
                $resps = array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "Error upload Image, file not exist", "data" => $_FILES);
            }
        }
        set_resp($resps, $http_response_header);
    }

    private function createMovieThumb($srcFile, $destFile = "")
    {
        // Change the path according to your server.
        if (empty($destFile)) {
            $destFile = "%d.jpg";
        }
        $files = $this->thumbndir . $destFile;
        $cmd = "ffmpeg -i $srcFile -vf fps=1/2 " . $files;

        exec($cmd, $result);
        return $result;
    }

    public function sendOTP_post()
    {
        // load library
        $this->load->library('Nexmo', 'nemo');
        // set response format: xml or json, default json
        $this->nexmo->set_format('json');

        // **********************************Text Message*************************************
        $from = '08512312311'; //server number
        $to = '085717152992'; //user id number
        $digits = 3;
        $otp = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
        $message = array(
            'text' => $otp . ' is your password for Baboo',
        );
        $response = $this->nexmo->send_message($from, $to, $message);
        $resp_data = array(
            "code" => $this->nexmo->get_http_status(),
            "message" => "",
            "data" => $response
        );
        set_resp($resp_data, REST_Controller::HTTP_OK);
    }

    public function uploadVideo_post()
    {
        $data = array();
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;

        if (!empty($user_id)) $data["user_id"] = $user_id;
        $resps = array();
        $a = base_url();
        if (strpos($a, REST_Controller::api_dev) !== false || strpos($a, REST_Controller::api_local) !== false) {
            $config['upload_path'] = $this->Uploadir;
            $config['allowed_types'] = 'mpeg|3gp|3gpp|mpg|mp4|avi|webm|ogg|flv|swf';
            $config['max_size'] = 8000;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('video_url')) {
                $this->response(array('code' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'message' => strip_tags($this->upload->display_errors()), 'data' => (object)array()), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            } else {
                $upload = $this->upload->data();
                $cover_url = array();
                $assetscover = $this->DirAssets . $upload["client_name"];
                $cover_url["file_cover"] = $assetscover;
                $images_path = $upload["raw_name"] . ".jpg";
                $video_path = $this->DUploadir . $upload["file_name"];
                $file = $this->createMovieThumb($video_path, $images_path);
                $http_response_header = REST_Controller::HTTP_OK;
                $resps = array('code' => REST_Controller::HTTP_OK, 'message' => 'Upload Video Success ' . implode(",", $file), 'data' => (object)array("asset_url" => $assetscover));
            }
        } else {
            if (!empty($_FILES) && $_FILES["video_url"]["error"] != 4) {
                $files = $_FILES["video_url"]["name"];
                $ext = pathinfo($files, PATHINFO_EXTENSION);
                $tmpName = $_FILES["video_url"]["tmp_name"];
                $fileName = "baboo-cover/video_" . time() . ".$ext";
                //check if uploaded
                if ($this->s3->upload($fileName, $tmpName, $this->bucket)) {
                    $assetscover = $this->s3->url($fileName, $this->bucket);
                    $images_path = ""; //nanti kalau sudah ada baru ditambahkan
                    $file = $this->createMovieThumb($assetscover, $images_path);
                    $http_response_header = REST_Controller::HTTP_OK;
                    $resps = array('code' => REST_Controller::HTTP_OK, 'message' => 'Upload Video Success ' . implode(",", $file), 'data' => (object)array("asset_url" => $assetscover));
                } else {
                    $http_response_header = REST_Controller::HTTP_ACCEPTED;
                    $resps = array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "Error upload PDF", "data" => (object)array());
                }
            } else {
                $http_response_header = REST_Controller::HTTP_ACCEPTED;
                $resps = array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "error upload, file not exist", "data" => $_FILES);
            }
        }
        set_resp($resps, $http_response_header);
    }

    public function download_post()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $data_where = array();
        $data_resp = array();
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_message = "download successfully";
        $book_id = $this->post("book_id");
        if (!empty($book_id)) {
            $data_where["book_id"] = $book_id;
            $data_where["status_publish !="] = "draft";
            $this->db->where($data_where);
            $gbook = $this->db->get("book");
            if ($gbook->num_rows() > 0) {
                $resp_code = REST_Controller::HTTP_OK;
                $http_response_header = REST_Controller::HTTP_OK;
                if (!empty($user_id)) {
                    $data_whereas = array("id_user" => $user_id, "id_book" => $book_id);
                    $data_update = array("is_download" => 1);
                    $gind = $this->db->update("collections", $data_update, $data_whereas);
                    if (!$gind) {
                        $resp_message .= (is_array($this->db->error())) ? " " . implode(",", $this->db->error()) : $this->db->error();
                    }
                }
                $gresult = $gbook->row();
                $gq = $this->db->query("UPDATE book SET download_count = download_count + 1 WHERE book_id = " . $data_where["book_id"]);
                $data_resp["id_book"] = $gresult->book_id;
                $data_resp["title_book"] = $gresult->title_book;
                if ($gq) {
                    $data_resp["url_book"] = $gresult->pdf_url;
                    //$gedate = gmdate("Y-m-d H:i:s \G\M\T", strtotime($gresult->generated_date));
                    $data_resp["created_date"] = (string)date("Y-m-d H:i:s O", strtotime($gresult->generated_date));
                    $data_resp["epoch_date"] = strtotime($data_resp["created_date"]);
                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "Book Not Found";
            }
        } else {
            $resp_code = REST_Controller::HTTP_FORBIDDEN;
            $resp_message = REST_Controller::HTTP_FORBIDDEN_MESSAGE;
        }
        set_resp(array("code" => $resp_code, "message" => $resp_message, "data" => $data_resp), $http_response_header);
    }

    public function books_get()
    {
        // Books from a data store e.g. database
        $this->db->select("book.book_id as id, chapters.chapter_id as id_chapter, chapters.chapter_title as judul_chapter, users.user_id as author_id, users.fullname as authorname");
        $this->db->join("users", "users.user_id = book.user_id");
        $this->db->join("chapters", "chapters.id_book = book.book_id");
        $books = $this->db->get()->result();
        $id = $this->get('book_id');
        // If the id parameter doesn't exist return all the books
        if ($id === NULL) {   // Check if the books data store contains books (in case the database result returns NULL)
            if ($books) {
                $resp_books = array(
                    "code" => REST_Controller::HTTP_OK,
                    "message" => "",
                    "data" => $books
                );
                // Set the response and exit
                $this->response($resp_books, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            } else {
                // Set the response and exit
                $this->response([
                    'code' => REST_Controller::HTTP_NOT_FOUND,
                    'message' => 'No books were found',
                    'data' => (object)array()
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
        // Find and return a single record for a particular books.
        $id = (int)$id;
        // Validate the id.
        if ($id <= 0) {
            // Invalid id, set the response and exit.
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'What are you looking for?',
                'data' => (object)array()
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (400) being the HTTP response code
        }
        // Get the book from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.
        $book = NULL;
        //check if books array is not empty
        if (!empty($books)) {
            foreach ($books as $key => $value) {
                if ($value->book_id && $value->book_id == $id) {
                    $book = $value;
                }
            }
        }
        //check if your selected book array not empty
        if (!empty($book)) {
            $resp_book = array(
                "code" => REST_Controller::HTTP_OK,
                "message" => "",
                "data" => $book
            );
            set_resp($resp_book, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            $this->response([
                'code' => REST_Controller::HTTP_NOT_FOUND,
                'message' => 'No books were found',
                'data' => (object)array()
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function preCreateBook_post()
    {
        error_reporting(0);
        $http_response_header = REST_Controller::HTTP_OK;
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $resp_message = "";
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_data = array();
        $post = array();
        if (!empty($this->post("title_book"))) $post["title_book"] = $this->post("title_book");
        if (!empty($user_id)) {
            if (!empty($this->post("book_id"))) {
                $data_update["latest_update"] = date("Y-m-d H:i:s");
                $data_update["title_draft"] = $this->post("title_book");
                $book_id = $this->post("book_id");
                $this->db->update("book", $post, array("book_id" => $book_id));
            } else {
                $post["author_id"] = $user_id;
                $this->db->insert("book", $post);
                $book_id = $this->db->insert_id();
            }
            if ($book_id) {
                $http_response_header = REST_Controller::HTTP_OK;
                $resp_code = REST_Controller::HTTP_OK;
                $resp_data = array(
                    "book_id" => $book_id
                );
            } else {
                $resp_message = "Error DB " . $this->db->error();
            }
        } else {
            set_resp(array("code" => REST_Controller::HTTP_BAD_REQUEST, "message" => "user cannot be empty"), REST_Controller::HTTP_ACCEPTED);
        }
        $message = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$resp_data
        );
        set_resp($message, $http_response_header); // CREATED (201) being the HTTP response code
    }

    public function preCreateBookPDF_post()
    {
        error_reporting(0);
        $http_response_header = REST_Controller::HTTP_OK;
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $resp_message = "";
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_data = array();
        $post = array();
        if (!empty($this->post("title_book"))) $post["title_book"] = $this->post("title_book");
        if (!empty($this->post("file_type")) && $this->post("file_type") == "epub") {
            $post["book_type"] = REST_Controller::book_type_epub;
        }else{
            $post["is_pdf"] = 0;
            $post["book_type"] = REST_Controller::book_type_pdf;
        }
        if (!empty($this->post("descriptions"))) $post["descriptions"] = $this->post("descriptions");
        if (!empty($user_id)) {
            if (!empty($this->post("book_id"))) {
                $post["latest_update"] = date("Y-m-d H:i:s");
                $book_id = $this->post("book_id");
                $this->db->update("book", $post, array("book_id" => $book_id));
            } else {
                $post["author_id"] = $user_id;
                $this->db->insert("book", $post);
                $book_id = $this->db->insert_id();
            }
            if ($book_id) {
                $http_response_header = REST_Controller::HTTP_OK;
                $resp_code = REST_Controller::HTTP_OK;
                $epoch_date = "";
                $title_book = $this->post("title_book");
                $gbooke = $this->db->select("title_book, generated_date")->from("book")->where("book_id", $book_id)->get();
                if ($gbooke->num_rows() > 0) {
                    $title_book = (!empty($gbooke->row()->title_book)) ? $gbooke->row()->title_book : $this->post("title_book");
                    $epoch_date = (!empty($gbooke->row()->generated_date)) ? $gbooke->row()->generated_date . date(" O") : "";
                }
                $resp_data = array(
                    "book_id" => $book_id,
                    "title_book" => $title_book,
                    "epoch_date" => $epoch_date
                );
            } else {
                $resp_message = "Error DB " . $this->db->error();
            }
        } else {
            set_resp(array("code" => REST_Controller::HTTP_BAD_REQUEST, "message" => "user cannot be empty"), REST_Controller::HTTP_ACCEPTED);
        }
        $message = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$resp_data
        );
        set_resp($message, $http_response_header); // CREATED (201) being the HTTP response code
    }

    public function saveBook_post()
    {
        $bodyPost = (object)$this->post();
        $this->load->library("Dobackground", "dobackground");
        $data = array();
        $key = sethead();
        $status_publish = "draft";
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        if (!empty($this->post('category'))) $data["category_id"] = $this->post('category');
        if (!empty($this->post("book_id"))) $data["book_id"] = $this->post('book_id');
        if (!empty($this->post("chapter_id"))) $data["chapter_id"] = $this->post('chapter_id');
        if (!empty($this->post("status_publish"))) {
            $status_publish = $this->post("status_publish");
        }
        $data["status_publish"] = $status_publish;
        if (!empty($this->post("title_book"))) $data["title_book"] = $this->post("title_book");
        $data_chapter = null;
        $data_phar = null;
        if (!empty($this->post("chapter_title"))) $data_chapter = $this->post('chapter_title');
        if (!empty($this->post('paragraph'))) $data_phar = $this->post('paragraph');
        if (!empty($this->post('price'))) {
            $data["price"] = $this->post('price');
            $data["merchant_profit"] = (double)$this->post("price") * REST_Controller::merchant_profit;
            $data["baboo_profit"] = (double)$this->post("price") * REST_Controller::baboo_profit;
            if (empty($this->post("total_price"))) $data["total_price"] = ceil((double)$this->post("price") + ($this->post("price") * (REST_Controller::tax_fee / 100) + REST_Controller::payment_fee));
            else $data["total_price"] = $this->post("total_price");
        }
        if (!empty($this->post("is_paid"))) {
            $is_paid = "free";
            if ($bodyPost->is_paid == true) $is_paid = "not_" . $is_paid;
            $data["is_free"] = $is_paid;
        }
        if (!empty($this->post("chapter_id"))) $chapter_id = $this->post("chapter_id");
        $book_id = $this->post("book_id");
        //set for usage of default image
        $a = base_url();
        $assets_serv = getAssetsEnv($a);
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        if (empty($book_id) && empty($user_id)) {
            $resps = array(
                "code" => REST_Controller::HTTP_BAD_REQUEST,
                "message" => "You can't access this without id book",
                "data" => (object)array()
            );
            $this->set_response($resps, $http_response_header);
        }

        $resp_message = "";
        $resp_data = array();
        $this->db->where(array("book_id" => $book_id, "author_id" => $user_id));
        $check_book = $this->db->get("book");
        if ($check_book->num_rows() > 0) {
            $publish_at = $check_book->row()->publish_at;
            if ($this->post("status_publish") == "publish" && empty($publish_at)) $data["publish_at"] = date("Y-m-d H:i:s");
            elseif ((empty($this->post("status_publish")) || $this->post("status_publish") == "draft") && !empty($publish_at)) $data["status_publish"] = "on editor";
            //check if image url exist
            if ($status_publish == "publish") {
                if (!empty($check_book->row()->file_cover)) {
                    $file_cover = $check_book->row()->file_cover;
                } else {
                    if ($book_id % 3 == 0) $file_cover = $assets_serv . "baboo-cover/default3.png";
                    else if ($book_id % 2 == 0) $file_cover = $assets_serv . "baboo-cover/default2.png";
                    else $file_cover = $assets_serv . "baboo-cover/default1.png";
                    $data["file_cover"] = $file_cover;
                }
                if (empty($check_book->row()->image_url)) $data["image_url"] = $file_cover;
            }
            $data["latest_update"] = date("Y-m-d H:i:s");
            $data["author_id"] = $check_book->row()->author_id;
            if (!empty($chapter_id)) {
                if ($this->post("status_publish") == "publish") {
                    $this->db->update("book", array("status_publish" => "publish"), array("book_id" => $book_id));
                }
                $message = $this->books->update_books($data, $data_chapter, $data_phar);
                $resp_code = $message["code"];
                $resp_message .= $message["message"];
                $resp_data = $message["data"];
            } else {
                if (!empty($this->post("chapter_title") && !empty($this->post("paragraph")))) {
                    $message = $this->books->insert_books($data, $data_chapter, $data_phar);
                    $resp_code = $message["code"];
                    $http_response_header = $message["code"];
                    $resp_message = $message["message"];
                    $resp_data = $message["data"];
                } else {
                    $resp_code = REST_Controller::HTTP_NOT_FOUND;
                    $http_response_header = REST_Controller::HTTP_ACCEPTED;
                    $resp_message = "book to publish not found";
                }
            }
            if ($resp_code == REST_Controller::HTTP_OK && $this->post("status_publish") == "publish") {
                $stat_book = recheck_publish($book_id);
                if ($stat_book) {
                    $resp_code = REST_Controller::HTTP_OK;
                    $resp_message = "book successfully published";
//                    else $resp_message = implode(",", $this->db->error());
                    $http_response_header = REST_Controller::HTTP_OK;
                    $url = site_url("book/Bg_process/generateEpub");
                    $param = array(
                        'book_id' => $book_id,
                    );
                    $this->dobackground->do_in_background($url, $param);
                } else {
                    $resp_code = REST_Controller::HTTP_NOT_FOUND;
                    $resp_message = "something went wrong";
                    $http_response_header = REST_Controller::HTTP_OK;
                }
            }
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $http_response_header = REST_Controller::HTTP_ACCEPTED;
            $resp_message = "book to publish not found";
        }
        set_resp(array("code" => $resp_code, "message" => $resp_message, "data" => (object)$resp_data), $http_response_header);
    }

    public function saveChapter_post()
    {
        $this->load->library("Dobackground", "dobackground");
        $data_chapter = null;
        $data_phar = null;
        $data = array();
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        if (!empty($this->post("book_id"))) $data["book_id"] = $this->post('book_id');
        if (!empty($this->post("chapter_id"))) $data["chapter_id"] = $this->post('chapter_id');
        if (!empty($this->post("chapter_title"))) $data_chapter = $this->post('chapter_title');
        if (!empty($this->post('paragraph'))) $data_phar = $this->post('paragraph');
        if (!empty($this->post("chapter_id"))) $chapter_id = $this->post("chapter_id");
        $book_id = $this->post("book_id");
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        if (empty($book_id)) {
            $resps = array(
                "code" => REST_Controller::HTTP_BAD_REQUEST,
                "message" => "You can't access this without id book",
                "data" => (object)array()
            );
            set_resp($resps, $http_response_header);
        } else {
            $resp_message = "";
            $resp_data = array();
            $this->db->where(array("book_id" => $book_id, "author_id" => $user_id));
            $check_book = $this->db->get("book");
            if ($check_book->num_rows() > 0) {
                if (!empty($check_book->row()->publish_at)) $data["status_publish"] = "on editor";
                $data["latest_update"] = date("Y-m-d H:i:s");
                $data["author_id"] = $check_book->row()->author_id;
                if (!empty($chapter_id)) {
                    $message = $this->books->update_books($data, $data_chapter, $data_phar);
                    $resp_code = $message["code"];
                    $resp_message .= $message["message"];
                    $resp_data = $message["data"];
                } else {
                    if (!empty($this->post("chapter_title"))) {
                        $message = $this->books->insert_books($data, $data_chapter, $data_phar);
                        $resp_code = $message["code"];
                        $http_response_header = $message["code"];
                        $resp_message = $message["message"];
                        $resp_data = $message["data"];
                    } else {
                        $resp_code = REST_Controller::HTTP_NOT_FOUND;
                        $http_response_header = REST_Controller::HTTP_ACCEPTED;
                        $resp_message = "book to save not found";
                    }
                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $http_response_header = REST_Controller::HTTP_ACCEPTED;
                $resp_message = "book to publish not found or unauthorized";
            }
            set_resp(array("code" => $resp_code, "message" => $resp_message, "data" => (object)$resp_data), $http_response_header);
        }
    }

    public function savePublish_post()
    {
        $last_query = "";
        $this->load->library("Dobackground", "dobackground");
        $data = array();
        $key = sethead();
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $book_id = $this->post("book_id");
        if (!empty($this->post('category'))) $data["category_id"] = $this->post('category');
        if (!empty($this->post('title_book'))) $data["title_book"] = $this->post('title_book');
        if (!empty($this->post('price'))) {
            $data["price"] = $this->post('price');
            $data["merchant_profit"] = (double)$this->post("price") * REST_Controller::merchant_profit;
            $data["baboo_profit"] = (double)$this->post("price") * REST_Controller::baboo_profit;
            $data["total_price"] = ceil((double)$this->post("price") + ($this->post("price") * (REST_Controller::tax_fee / 100) + REST_Controller::payment_fee));
        }
        $is_paid = "free";
        if (!empty($this->post("is_paid"))) {
            if ($this->post("is_paid") == true) $is_paid = "not_" . $is_paid;
            $data["is_free"] = $is_paid;
        }
        // check base url
        $a = base_url();
        $assets_serv = REST_Controller::assets_dev;
        if (strpos($a, REST_Controller::api_staging) !== false) {
            $assets_serv = REST_Controller::assets_stg;
        } elseif (strpos($a, REST_Controller::api_prod) !== false) {
            $assets_serv = REST_Controller::assets_prod;
        }
        $data["status_publish"] = "publish";
        $data["latest_update"] = date("Y-m-d H:i:s");
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        if (empty($book_id)) {
            $resps = array(
                "code" => REST_Controller::HTTP_BAD_REQUEST,
                "message" => "You can't access this without id book",
                "data" => (object)array()
            );
            set_resp($resps, $http_response_header);
        } else {
            $resp_message = "";
            $resp_data = array();
            $this->db->where(array("book_id" => $book_id, "author_id" => $user_id));
            $check_book = $this->db->get("book");
            if ($check_book->num_rows() > 0) {
                $generateDate = date("Y-m-d H:i:s O");
                $datapassword = 'ID#' . $book_id . '#' . $check_book->row()->title_book . '#' . strtotime($generateDate);
                $password = hash_hmac('sha512', $datapassword, strtotime($generateDate));
                $publish_at = $check_book->row()->publish_at;
                if (empty($publish_at)) $data["publish_at"] = date("Y-m-d H:i:s");
                $file_cover = $assets_serv . "baboo-cover/default1.png";
                if (!empty($this->post('file_cover'))) {
                    $file_cover = $this->post("file_cover");
                    $data["file_cover"] = $file_cover;
                } else {
                    if (!empty($book_id) && empty($check_book->row()->file_cover)) {
                        if ($book_id % 3 == 0) $file_cover = $assets_serv . "baboo-cover/default3.png";
                        else if ($book_id % 2 == 0) $file_cover = $assets_serv . "baboo-cover/default2.png";
                        else $file_cover = $assets_serv . "baboo-cover/default1.png";
                    }
                    $data["file_cover"] = $file_cover;
                }
                if (empty($check_book->row()->image_url)) $data["image_url"] = $file_cover;
                $upd = $this->db->update("book", $data, array("book_id" => $book_id));
                if ($upd) {
                    if ($check_book->row()->is_pdf == 0) {
                        $this->load->helper("pdff");
                        $this->load->library("Fpdf_gen", "fpdf_gen");
                        $pdf_url = $check_book->row()->pdf_url;
                        $uploaddir = "../uploads/baboo-docs/";
                        $pdf = $uploaddir . "docs_" . time() . ".pdf";
                        file_put_contents($pdf, fopen($pdf_url, 'r'));
                        chmod($pdf, 0777);
                        if (file_exists($pdf)) {
                            $pdf_real = realpath($pdf);
                            $new_name = str_replace('.pdf', '', basename($pdf)) . "_decrypted.pdf";
                            $location_pdf = realpath($uploaddir) . "/$new_name";
                            $oldgenerateDate = (!empty($check_book->row()->generated_date)) ? date("Y-m-d H:i:s O", strtotime($check_book->row()->generated_date)) : date("Y-m-d H:i:s O");
                            $olddatapassword = 'ID#' . $book_id . '#' . $check_book->row()->title_book . '#' . strtotime($oldgenerateDate);
                            $oldpassword = hash_hmac('sha512', $olddatapassword, strtotime($oldgenerateDate));
                            exec("qpdf --decrypt $pdf_real --password=$oldpassword $location_pdf", $output, $return);
                            if (!$return) {
                                $decrypted_pdf = $uploaddir . $new_name;
                                $newest_name = str_replace('.pdf', '', basename($pdf)) . "_1.4.pdf";
                                $locations_pdf = realpath($uploaddir) . "/$newest_name";
                                if (file_exists($uploaddir . $new_name)) chmod($uploaddir . $new_name, 0777);
                                $gs = "gs";
                                if (strpos(base_url(), REST_Controller::api_local) !== false) $gs = "gswin64c";
                                exec("$gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dBATCH -sOutputFile=$locations_pdf $location_pdf", $output, $return);
                                if ($is_paid != "free" && !empty($this->post("chapter_start"))) {
                                    $total_page = $this->fpdf_gen->countPage($uploaddir . $newest_name);
                                    $resp = $this->fpdf_gen->concat($uploaddir . $newest_name, $this->post("chapter_start"), $password);
                                    if ($resp && isset($resp["filename"]) && !empty($resp["filename"])) {
                                        $pdf_free = $resp["filename"];
                                        if (strpos(base_url(), REST_Controller::api_staging) !== false || strpos(base_url(), REST_Controller::api_prod) !== false) {
                                            $pdf_full = pdf_s3upload($uploaddir . $newest_name);
                                            $pdf_extract = pdf_s3upload($pdf_free);
                                        } else {
                                            $pdf_full = pdf_move($uploaddir . $newest_name);
                                            $pdf_extract = pdf_move($pdf_free);
                                        }
                                        //check if file is exist
                                        if (file_exists($uploaddir . $new_name)) {
                                            unlink($location_pdf);
                                            if (file_exists($pdf)) unlink($pdf_real);
                                        }
                                        $pdf_data["total_page"] = $total_page;
                                        $pdf_data["pdf_start"] = $this->post("chapter_start");
                                        $pdf_data["generated_date"] = $generateDate;
                                        $pdf_data["pdf_url"] = $pdf_full;
                                        $pdf_data["pdf_free"] = $pdf_extract;
                                        $updd = $this->db->update("book", $pdf_data, array("book_id" => $book_id));
                                        if ($updd) {
                                            $colls = $this->db->query("SELECT COUNT(id_user) as total_collection FROM collections WHERE id_book = ?", array($book_id));
                                            if ($colls && $colls->num_rows() > 0 && $colls->row()->total_collection > 0) $this->db->update("collections", array("is_download" => 0), array("id_book" => $book_id));
                                        }
                                        $resp_code = REST_Controller::HTTP_OK;
                                        $resp_message = "book pdf paid successfully published";
                                    } else {
                                        $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                                        $resp_message = "something went wrong on pdf download or third party not downloaded";
                                    }
                                } else {
                                    $total_page = $this->fpdf_gen->countPage($uploaddir . $newest_name);
//  for debugging purpose               if($this->fpdf_gen->setPass($uploaddir.$newest_name, $password)) {}
                                    if (strpos(base_url(), REST_Controller::api_staging) !== false || strpos(base_url(), REST_Controller::api_prod) !== false) {
                                        $pdf_full = pdf_s3upload($uploaddir . $newest_name);
                                        $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                                        $resp_message = "something went wrong on pdf download or third party not downloaded";
                                        if ($pdf_full) {
                                            $resp_code = REST_Controller::HTTP_OK;
                                            $resp_message = "book pdf free successfully published";
                                        }
                                    } else {
                                        $pdf_full = pdf_move($uploaddir . $newest_name);
                                        $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                                        $resp_message = "something went wrong on pdf download or third party not downloaded";
                                        if ($pdf_full) {
                                            $resp_code = REST_Controller::HTTP_OK;
                                            $resp_message = "book pdf free successfully published";
                                        }
                                    }
                                    //cbeck if still exist
                                    if (file_exists($uploaddir . $new_name)) {
                                        unlink($location_pdf);
                                        if (file_exists($pdf)) unlink($pdf_real);
                                    }
                                    $pdf_data["total_page"] = $total_page;
                                    $pdf_data["generated_date"] = $generateDate;
                                    $pdf_data["pdf_url"] = $pdf_full;
                                    $this->db->update("book", $pdf_data, array("book_id" => $book_id));
                                }
                            } else {
                                $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                                $resp_message = "something went wrong on pdf download or third party not downloaded";
                            }
                        }
                    } else {
                        $stat_book = recheck_publish($book_id);
                        if ($stat_book) {
                            $resp_code = REST_Controller::HTTP_OK;
                            $resp_message = "book successfully published";
                            //                    else $resp_message = implode(",", $this->db->error());
                            $http_response_header = REST_Controller::HTTP_OK;
                            $url = site_url("book/Bg_process/generateEpub");
                            $param = array(
                                'book_id' => $book_id,
                            );
                            $this->dobackground->do_in_background($url, $param);
                        } else {
                            $resp_code = REST_Controller::HTTP_NOT_FOUND;
                            $resp_message = "something went wrong";
                            //                    else $resp_message = implode(",", $this->db->error());
                            $http_response_header = REST_Controller::HTTP_OK;
                        }
                    }
                } else {
                    $errors = $this->db->error();
                    if (is_array($errors)) $resp_message .= $errors["code"] . ":" . $errors["message"];
                    else $resp_message = $errors;
                    $resp_code = REST_Controller::HTTP_BAD_REQUEST;
                }
            } else {
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "book to publish not found or unauthorized";
            }
            set_resp(array("code" => $resp_code, "message" => $resp_message . $last_query, "data" => (object)$resp_data), $http_response_header);
        }
    }

    private function checkPublish($book_id)
    {
        $cbok = $this->db->get_where("book", array("book_id" => $book_id, "status_publish !=" => "draft"));
        if ($cbok->num_rows() == 0) {
            set_resp(
                array(
                    "code" => REST_Controller::HTTP_BAD_REQUEST,
                    "message" => "you are trying to comment on draft book, please publish it first",
                    "data" => (object)array()
                ), REST_Controller::HTTP_ACCEPTED);
        }
    }

    public function lastSeen_post()
    {
        $keys = sethead();
        $user_id = (!empty($keys)) ? getUserFromKey($keys) : 0;
        $book_id = $this->post("book_id");
        $last_page = $this->post("last_page");
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = $http_response_header;
        $resp_message = "parameter user, book and page cannot be empty";
        if (!empty($user_id) && !empty($book_id)) {
            $this->db->select("title_book");
            $where_book = $this->db->get_where("book", array("book_id" => $book_id));
            if ($where_book && $where_book->num_rows() > 0) {
                $wherethis = array("latest_book" => $book_id, "reader" => $user_id);
                $gethist = array("latest_book" => $book_id, "reader" => $user_id, "last_page" => $last_page);
                try{
                    $this->db->select("history_id");
                    $chist = $this->db->get_where("history_read", $wherethis);
                    if ($chist->num_rows() > 0) $ins = $this->db->update("history_read", array("last_page"=>$last_page, "latest_read" => date("Y-m-d H:i:s")), $wherethis);
                    else $ins = $this->db->insert("history_read", $gethist);
                    if ($ins) {
                        $http_response_header = REST_Controller::HTTP_OK;
                        $resp_code = $http_response_header;
                        $resp_message = "set last page success";
                    } else {
                        $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                        $resp_message = "something wrong with our input, we will cover it soon!";
                    }
                }catch(Exception $ex){
                    $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                    $resp_message = "column last_page are not available on your table";
                }
            }else{
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "book that you trying to read isn't exist anymore, please choose another book";
            }
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message
        );
        set_resp($responses, $http_response_header);
    }

    public function archiveBook_post()
    {
        error_reporting(0);
        $http_response_header = REST_Controller::HTTP_OK;
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $resp_message = "";
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_data = array();
        $post = array();
        if (!empty($user_id)) {
            if (!empty($this->post("book_id"))) {
                $data_update["status_publish"] = "draft";
                $book_id = $this->post("book_id");
                $upd = $this->db->update("book", $data_update, array("book_id" => $book_id));
                if($upd){
                    $http_response_header = REST_Controller::HTTP_OK;
                    $resp_code = REST_Controller::HTTP_OK;
                    $resp_data = array(
                        "book_id" => $book_id
                    );
                }else{
                    $http_response_header = REST_Controller::HTTP_ACCEPTED;
                    $resp_code = REST_Controller::HTTP_NOT_FOUND;
                    $resp_message = "something wrong when archive book";
                }
            } else {
                $http_response_header = REST_Controller::HTTP_ACCEPTED;
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "book not found";
            }
        } else {
            $this->set_response(array("code" => REST_Controller::HTTP_BAD_REQUEST, "message" => "you must login to see this"), REST_Controller::HTTP_ACCEPTED);
        }
        $message = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$resp_data
        );
        set_resp($message, $http_response_header); // CREATED (201) being the HTTP response code
    }


    public function books_delete()
    {
        $id = (int)$this->get('id');

        // Validate the id.
        if ($id <= 0) {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $response = $this->books->delete_user($id);
        $message = [
            'id' => $id,
            'status' => $response,
            'message' => 'Deleted the resource'
        ];

        set_resp($message, REST_Controller::HTTP_OK); // NO_CONTENT (204) being the HTTP response code
    }

    /* Private Data Methods */
}
