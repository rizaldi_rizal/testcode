<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Book_model extends CI_Model
{
    function __construct()
    {
        $this->tableName = 'book';
        $this->primaryKey = 'book_id';
    }

    public function get_book()
    {
        $gq = $this->db->get($this->tableName);
        $sq = "books not exist";
        if ($gq->num_rows() > 0) {
            $sq = $gq->result();
        }
        return $sq;
    }

    public function get_bookdata($userID)
    {
        $gq = $this->db->get_where($this->tableName, array("book_id" => $userID));
        $sq = null;
        if ($gq->num_rows() > 0) {
            $sq = $gq->result();
        }
        return $sq;
    }

    public function insert_books($data_insert, $data_chapter = NULL, $data_phar = NULL)
    {
        $stat_response = REST_Controller::HTTP_ACCEPTED;
        $data_response = "";
        $message_response = "save book failed insert";
        $book_id = "";
        $title_book = "";
        $status_publish = "draft";
        if (count($data_insert) > 0 && !empty($data_insert)) {
            if (isset($data_insert["book_id"]) && !empty($data_insert["book_id"])) {
                $data_update = $data_insert;
                $bins = $data_insert["book_id"];
                unset($data_update["book_id"], $data_update["chapter_id"]);
                $this->db->update("book", $data_update, array("book_id" => $bins));
                $book_id = $bins;
            } else {
                $data_update = $data_insert;
                $bins = $data_insert["book_id"];
                unset($data_update["book_id"], $data_update["chapter_id"]);
                $this->db->update("book", $data_update, array("book_id" => $bins));
                $title_book = (isset($data_insert["title_book"]) && !empty($data_insert["title_book"]))? $data_insert["title_book"] : "";
                $book_id = $bins;
            }

            if (!empty($book_id) && !empty($data_chapter)) {
                $gdb = $this->db->get_where("book", array("book_id" => $book_id));
                if ($gdb->num_rows() > 0){
                    $title_book = (string)$gdb->row()->title_book;
                    $status_publish = $gdb->row()->status_publish;
                }
                $stpb = 2;
                if($status_publish == "publish") $stpb = 1;
                if (isset($data_insert["chapter_id"]) && !empty($data_insert["chapter_id"])) {
                    if($status_publish == "publish") {
                        $this->db->set("chapter_title", $data_chapter);
                        $this->db->set("chapter_draft", 'NULL', false);
                    }
                    else {
                        $this->db->set("chapter_draft", $data_chapter);
                    }
                    $insar = array(
                        "status_publish" => $stpb,
                        "id_book" => $book_id
                    );
                    $this->db->set($insar);
                    $this->db->where(array("chapter_id" => $data_insert["chapter_id"]));
                    $this->db->update("chapter");
                    $chapter_id = $data_insert["chapter_id"];
                } else {
                    $insar = array(
                        "status_publish" => $stpb,
                        "id_book" => $book_id,
                    );
                    if($status_publish == "publish") $insar["chapter_title"] = $data_chapter;
                    else {
                        $insar["chapter_title"] = "";
                        $insar["chapter_draft"] = $data_chapter;
                    }
                    $this->db->insert("chapter", $insar);
                    $chapter_id = $this->db->insert_id();
                }
                //test
                if ($chapter_id && !empty($data_phar)) {
                    $sdata_phar = preg_split(REST_Controller::delimiter_paragraph,
                        $data_phar, null, PREG_SPLIT_NO_EMPTY);
                    $insss = null;
                    foreach ($sdata_phar as $pharv) {
                        //check if your array take first part
                        $pharray = array(
                            "id_chapter" => $chapter_id,
                            "txt_paragraph" => $pharv,
                            "id_book" => $book_id
                        );
                        $this->db->insert("draft_paragraph", $pharray);
                        $insss = $this->db->insert_id();
                    }
                    if (!empty($insss)) {
                        $stat_response = REST_Controller::HTTP_OK;
                        $data_response = (object)array("book_id" => $book_id, "chapter_id" => $chapter_id, "title_book" => $title_book);
                        $message_response = "save books success";
                    } else {
                        $error = $this->db->error();
                        if (is_array($error)) $error = implode($error, ",");
                        $message_response = "save failed because " . $error;
                    }
                } else {
                    $message_response = "empty data phar";
                }

                if($status_publish == "publish"){
                    $gdbb = $this->db->get_where("book", array("book_id" => $book_id));
                    if($gdbb->num_rows() > 0) {
                        if(!empty($gdbb->row()->title_draft)) $this->db->update("book", array("title_book"=>$gdbb->row()->title_draft), array("book_id"=>$book_id));
                        $gchapps = $this->db->get_where("chapter", array("id_book"=>$book_id));
                        if($gchapps->num_rows() > 0){
                            foreach($gchapps->result() as $gchavals){
                                $chapterer_id = $gchavals->chapter_id;
                                $pchd = $this->db->get_where("paragraph", array("id_chapter" => $chapterer_id));
                                if ($pchd->num_rows() > 0) $this->db->delete("paragraph", array("id_chapter" => $chapterer_id));
                                $sdata_pharer = $this->db->get_where("draft_paragraph", array("id_chapter" => $chapterer_id));
                                if($sdata_pharer->num_rows() > 0){
                                    $insssa = 0;
                                    foreach ($sdata_pharer->result() as $pharav) {
                                        $spharray = array(
                                            "id_chapter" => $chapterer_id,
                                            "txt_paragraph" => $pharav->txt_paragraph,
                                            "id_book" => $book_id
                                        );
                                        $this->db->insert("paragraph", $spharray);
                                        $insssa = $this->db->insert_id();
                                    }
                                    if(!empty($insssa)) $this->db->delete("draft_paragraph", array("id_chapter" => $chapterer_id));
                                }
                            }
                        }
                    }
                }
            } else {
                $message_response = "empty book id";
            }
        }
        $response = array(
            "code" => $stat_response,
            "data" => $data_response,
            "message" => $message_response
        );
        return $response;

    }

    public function update_books($data_insert, $data_chapter = NULL, $data_phar = NULL)
    {
        $stat_response = REST_Controller::HTTP_ACCEPTED;
        $data_response = "";
        $message_response = "save book failed";
        $status_publish = "draft";
        $title_book = "";
        if (count($data_insert) > 0 && !empty($data_insert)) {
            if (isset($data_insert["chapter_id"]) && !empty($data_insert["chapter_id"])) {
                $data_update = $data_insert;
                $ins = $data_insert["chapter_id"];
                $bins = $data_insert["book_id"];
                unset($data_update["book_id"], $data_update["chapter_id"]);
                $this->db->update("book", $data_update, array("book_id" => $bins));
                $chapter_id = $ins;
            } else {
                $data_update = $data_insert;
                $bins = $data_insert["book_id"];
                $ins = $data_insert["chapter_id"];
                unset($data_update["book_id"], $data_update["chapter_id"]);
                $this->db->update("book", $data_update, array("book_id" => $bins));
                $chapter_id = $ins;
            }

            $getdb = $this->db->get_where("book", array("book_id" => $bins));
            if ($getdb->num_rows() > 0){
                $title_book = $getdb->row()->title_book;
                $status_publish = $getdb->row()->status_publish;
            }

            if (!empty($chapter_id) && !empty($data_chapter)) {
                if($status_publish == "publish"){
                    $this->db->set("chapter_title", $data_chapter);
                    $this->db->set("status_publish", 1);
                    $this->db->set("chapter_draft", 'NULL', false);
                }else{
                    $stpb = 2;
                    $sstab = 2;
                    $sschap = $this->db->get_where("chapter", array("chapter_id" => $chapter_id));
                    if($sschap->num_rows() > 0) $sstab = $sschap->row()->status_publish;
                    if($status_publish != "draft" && $sstab == 1) $stpb = 1;
                    $insar = array(
                        "chapter_draft" => $data_chapter,
                        "status_publish" => $stpb
                    );
                    $this->db->set($insar);
                }
                $this->db->where(array("chapter_id" => $chapter_id));
                $update_ch = $this->db->update("chapter");
                if ($update_ch && !empty($data_phar)) {
                    $stat_response = REST_Controller::HTTP_OK;
                    $pch = $this->db->get_where("draft_paragraph", array("id_chapter" => $chapter_id));
                    if ($pch->num_rows() > 0) {
                        $this->db->delete("draft_paragraph", array("id_chapter" => $chapter_id));
                    }
                    if (!empty($data_phar)) {
                        $sdata_phar = preg_split(REST_Controller::delimiter_paragraph, $data_phar, null, PREG_SPLIT_NO_EMPTY);
                        foreach ($sdata_phar as $pharv) {
                            $pharray = array(
                                "id_chapter" => $chapter_id,
                                "txt_paragraph" => $pharv,
                                "id_book" => $bins
                            );
                            $this->db->insert("draft_paragraph", $pharray);
                            $insss = $this->db->insert_id();
                            if ($insss) $stat_response = REST_Controller::HTTP_OK;
                        }
                    }
                    $data_response = (object)array("book_id" => $data_insert["book_id"], "chapter_id" => $chapter_id, "title_book" => $title_book);
                    $message_response = "save book success";
                }
            }
        }
        $response = array(
            "code" => $stat_response,
            "data" => $data_response,
            "message" => $message_response
        );
        return $response;

    }

    public function update_book($data_update, $book_id)
    {
        $stat_response = REST_Controller::HTTP_ACCEPTED;;
        if (isset($book_id) && !empty($book_id)) {
            $data_update["modified"] = date("Y-m-d H:i:s");
            $upd = $this->db->update($this->tableName, $data_update, array($this->primaryKey => $book_id));
            if ($upd) $stat_response = REST_Controller::HTTP_OK;
        }
        return $stat_response;
    }

    public function delete_book($book_id)
    {
        $stat_response = REST_Controller::HTTP_ACCEPTED;;
        if (isset($book_id) && !empty($book_id)) {
            $del = $this->db->delete($this->tableName, array($this->primaryKey => $book_id));
            $stat_response = REST_Controller::HTTP_OK;
        }
        return $stat_response;
    }

}
