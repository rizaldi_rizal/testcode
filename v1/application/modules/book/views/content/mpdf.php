<?php
function editVideo($tagHtml)
{
    $urlThumb = "https://assets.dev-baboo.co.id/baboo-thumbs"; #server
    $urlVideo = "https://assets.dev-baboo.co.id/baboo-cover"; #server
    preg_match_all("/<iframe[^>]*>(.*?)<\/iframe>/is", $tagHtml, $matches);
    $vaHTML = $tagHtml;
    if (count($matches) > 0) {
        foreach ($matches as $mval) {
            if (is_array($mval)) {
                foreach ($mval as $mvol) {
                    $get_name = "";
                    if (!empty(trim(html_entity_decode($mvol, ENT_QUOTES, 'UTF-8')))) {
                        $request = htmlspecialchars($mvol);
                        $arr_eav = explode(" ", $request);
                        foreach ($arr_eav as $arr_val) {
                            if (strpos($arr_val, 'src') !== false) {
                                $get_video = explode("=", $arr_val);
                                $get_IDVideo = explode("/", $get_video[1]);
                                $codey = substr(html_entity_decode(end($get_IDVideo), ENT_QUOTES, 'UTF-8'), 0, -1);
                                $img = "<a href='https://www.youtube.com/watch?v=$codey' target='_blank' style='width:100%;text-align:center'><img src='https://img.youtube.com/vi/$codey/hqdefault.jpg' style='width:400px'/></a>";
                                $get_name = $img;
                            }
                        }
                        if (!empty($get_name)) {
                            $vaHTML = str_replace($mvol, $img, $tagHtml);
                        }
                    }

                }
            }
        }
    }
    preg_match_all("/<video[^>]*>(.*?)<\/video>/is", $vaHTML, $matchev);
    if (count($matchev) > 0) {
        foreach ($matchev as $mvav) {
            if (is_array($mvav)) {
                foreach ($mvav as $mvov) {
                    $get_namev = "";
                    if (!empty(trim(html_entity_decode($mvov, ENT_QUOTES, 'UTF-8')))) {
                        $requesty = htmlspecialchars($mvov);
                        $arr_namev = explode(" ", $requesty);
                        foreach ($arr_namev as $arr_valv) {
                            if (strpos($arr_valv, 'src') !== false) {
                                $get_videos = explode("=", $arr_valv);
                                $get_IDVideox = explode("/", $get_videos[1]);
                                $codex = substr(html_entity_decode(end($get_IDVideox), ENT_QUOTES, 'UTF-8'), 0, -1);
                                $link = $codex;
                                $codexs = explode(".", $codex);
                                if (count($codexs) > 0) $link = $codexs[0] . ".jpg";
                                $img = "<a href='$urlVideo/$codex' target='_blank' style='width:100%;text-align:center'><img src='$urlThumb/$link' style='width:300px'/></a>";
                                $get_namev = $img;
                            }
                        }
                    }
                    if (!empty($get_namev)) {
                        $vaHTML = str_replace($mvov, $img, $vaHTML);
                    }
                }
            }
        }
    }
    return $vaHTML;
}

?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php
    $html = "";
    $chapters = array();
    $paragraphs = array();
    if (isset($book_id) && !empty($book_id)){
    $this->db->select("book_id as id, view_count, share_count, title_book, author_id, file_cover as cover_url, fullname as author, prof_pict as author_pict");
    $this->db->from("book");
    $this->db->join("users", "users.user_id = book.author_id");
    $this->db->where(array('book_id' => $book_id));
    $prevQuery = $this->db->get();
    if ($prevQuery->num_rows() > 0){
    $prevResult = $prevQuery->row();
    ?>
    <title><?php echo $prevResult->title_book; ?></title>
    <style>
        p, p > * {
            line-height: 35px !important;
            font-size: 28px !important;
            text-align: justify;
            word-wrap: break-word;
            white-space: normal;
            text-indent: 5%;
        }
        p > img{
            text-align: center !important;
            text-indent : 0% !important;
        }
        .text-center {
            text-align : center;
            margin-bottom : 0px;
        }
        div.bawah-chapter{
            font-style: italic;
            text-align: center !important;
            margin-top: 0px;
            padding-top:0px;
        }
        div, div > * {
            line-height: 35px !important;
        }
    </style>
</head>
<body>
<?php
        $getPages = array();
        $this->db->where(array("id_book" => $book_id));
        $gchap = $this->db->get("chapter");
        $html = "";
        if ($gchap->num_rows() > 0) {
            $i = 1;
            foreach ($gchap->result() as $gkey => $chapv) {
                $html .= "<h1 class='text-center'>" . $chapv->chapter_title . "<tocentry content='".$chapv->chapter_title."' /></h1>";
                $html .= "<div class='bawah-chapter'>Chapter ".$i++."</div>";
                $html .= "<div class='content-paragraph'>";
                $gpar = $this->db->get_where("paragraph", array("id_chapter" => $chapv->chapter_id));
                if ($gpar->num_rows() > 0) {
                    foreach ($gpar->result() as $gpval) {
                        $parags = editVideo($gpval->txt_paragraph);
                        if (substr($gpval->txt_paragraph, 0, 2) != "<p") {
                            $html .= "<p id='" . $gpval->paragraph_id . "'>" . $parags . "</p>";
                        } else {
                            $html .= $parags;
                        }
                    }
                }
                $html .= "</div>";
                $last = $gchap->num_rows() - 1;
                if ($gkey != $last) {
                    $html .= "<pagebreak sheet-size='A5' />";
                }
            }
        }
    }
    echo $html;
//    print_r($arr_namev);
}
else {
    echo "</head><body>Tidak ada PDF";
}
?>
</body>
</html>