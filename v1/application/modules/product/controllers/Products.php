<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
/**
* 
*/
class Products extends REST_Controller
{
	
	function __construct()
	{
		parent::__construct();
        $this->load->helper("baboo");
	}
	public function index_post()
	{
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_code = REST_Controller::HTTP_ACCEPTED;

        $limit = REST_Controller::limit_timeline;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;

        $this->db->limit(REST_Controller::limit_timeline, $count);
        $getProduct = $this->db->get("products");
        $resp_data = array();
        if ($getProduct->num_rows() > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;
            $resp_message = "success get product";
            foreach ($getProduct->result() as $productList) {
                $strings = strip_tags($productList->description);
                if (strlen($strings) > 3) {
                    $stringCut = substr($strings, 0, 100);
                    $endPoint = strrpos($stringCut, ' ');

                    //if the string doesn't contain any space then it will cut without word basis.
                    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                    $string .= '... <a href="'.base_url().'product/Products/detailProducts/'.$productList->id.'">Read More</a>';
                }
                $data_list['id'] = $productList->id;
                $data_list['code'] = $productList->code;
                $data_list['name'] = $productList->name;
                $data_list['description'] = ($productList->description != null) ? $string : 'Belum ada Deskripsi' ;
                $data_list['price'] = number_format(str_replace('.0000', '', $productList->price));
                $data_list['image'] = $productList->image;
                $data_list['quantity'] = str_replace('.0000', '', $productList->quantity);
                $data_list['category'] = $this->db->select('name')->get_where('categories',array('id'=>$productList->category_id))->row()->name;
                $data_list['category_id'] = $productList->category_id;
                $resp_data[] = $data_list;   
            }

        } else {
            $resp_message = "product not found";
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data,
        );
        set_resp($responses, $http_response_header);
	}
    public function detailProducts_get($i)
    {
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_code = REST_Controller::HTTP_ACCEPTED;

        $getProduct = $this->db->get_where("products",array('id'=>$i));
        $resp_data = array();
        if ($getProduct->num_rows() > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;
            $resp_message = "success get product";
            $resp_data = $getProduct->row();
        } else {
            $resp_message = "product not found";
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data,
        );
        set_resp($responses, $http_response_header);
    }
    public function detailProducts_post($i)
    {
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_code = REST_Controller::HTTP_ACCEPTED;

        $getProduct = $this->db->get_where("products",array('id'=>$i));
        $resp_data = array();
        if ($getProduct->num_rows() > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;
            $resp_message = "success get product";
            $resp_data = $getProduct->row();
        } else {
            $resp_message = "product not found";
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data,
        );
        set_resp($responses, $http_response_header);
    }
    public function search_post()
    {
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_code = REST_Controller::HTTP_ACCEPTED;

        $limit = REST_Controller::limit_timeline;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;

        $this->db->limit(REST_Controller::limit_timeline, $count);

        $body = (object)$this->post();
        // $this->db->like('name', $body->search, 'both'); 

        $where = "name LIKE '%" . $body->search . "%'";
        $this->db->where($where);
        $getProduct = $this->db->get("products");
        $resp_data = array();
        if ($getProduct->num_rows() > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;
            $resp_message = "success get product";
            foreach ($getProduct->result() as $productList) {
                $strings = strip_tags($productList->description);
                if (strlen($strings) > 3) {
                    $stringCut = substr($strings, 0, 100);
                    $endPoint = strrpos($stringCut, ' ');

                    //if the string doesn't contain any space then it will cut without word basis.
                    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                    $string .= '... <a href="'.base_url().'product/Products/detailProducts/'.$productList->id.'">Read More</a>';
                }
                $data_list['id'] = $productList->id;
                $data_list['code'] = $productList->code;
                $data_list['name'] = $productList->name;
                $data_list['description'] = ($productList->description != null) ? $string : 'Belum ada Deskripsi' ;
                $data_list['price'] = number_format(str_replace('.0000', '', $productList->price));
                $data_list['image'] = $productList->image;
                $data_list['quantity'] = str_replace('.0000', '', $productList->quantity);
                $data_list['category'] = $this->db->select('name')->get_where('categories',array('id'=>$productList->category_id))->row()->name;
                $data_list['category_id'] = $productList->category_id;
                $resp_data[] = $data_list;   
            }

        } else {
            $resp_message = "product not found";
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data,
        );
        set_resp($responses, $http_response_header);
    }
    public function filter_post()
    {
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_code = REST_Controller::HTTP_ACCEPTED;

        $limit = REST_Controller::limit_timeline;
        $count = (!empty($this->post("count") && (int)$this->post("count") > 1)) ? count_paging($this->post("count"), $limit) : 0;

        $this->db->limit(REST_Controller::limit_timeline, $count);

        $body = (object)$this->post();
        
        if (isset($body->filter_by_start_price)) {
            $priceWhere = " price >= ".$body->filter_by_start_price." AND price <= ".$body->filter_by_end_price;
            $this->db->where($priceWhere);
        }
        if (isset($body->filter_by_start_price)) {
            $priceWhere = " price >= ".$body->filter_by_start_price." AND price <= ".$body->filter_by_end_price;
            $this->db->where($priceWhere);
        }
        if (isset($body->filter_by_category)) {
            $categoryWhere = " category_id = ".$body->filter_by_category;
            $this->db->where($categoryWhere);
        }
        
        $getProduct = $this->db->get("products");
        $resp_data = array();
        if ($getProduct->num_rows() > 0) {
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;
            $resp_message = "success get product";
            foreach ($getProduct->result() as $productList) {
                $strings = strip_tags($productList->description);
                if (strlen($strings) > 3) {
                    $stringCut = substr($strings, 0, 100);
                    $endPoint = strrpos($stringCut, ' ');

                    //if the string doesn't contain any space then it will cut without word basis.
                    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                    $string .= '... <a href="'.base_url().'product/Products/detailProducts/'.$productList->id.'">Read More</a>';
                }
                $data_list['id'] = $productList->id;
                $data_list['code'] = $productList->code;
                $data_list['name'] = $productList->name;
                $data_list['description'] = ($productList->description != null) ? $string : 'Belum ada Deskripsi' ;
                $data_list['price'] = number_format(str_replace('.0000', '', $productList->price));
                $data_list['image'] = $productList->image;
                $data_list['quantity'] = str_replace('.0000', '', $productList->quantity);
                $data_list['category'] = $this->db->select('name')->get_where('categories',array('id'=>$productList->category_id))->row()->name;
                $data_list['category_id'] = $productList->category_id;
                $resp_data[] = $data_list;   
            }

        } else {
            $resp_message = "product not found";
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
        }
        $responses = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data,
        );
        set_resp($responses, $http_response_header);
    }
}
