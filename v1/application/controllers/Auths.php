<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//set rest controller
require APPPATH . 'libraries/REST_Controller.php';

class Auths extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper("baboo");
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function get_otp($no_tep){
	    if(!empty($no_tep)){
	        $spt = queryOTP($no_tep);
	        if(!empty($spt) && count($spt) > 0){
                foreach ($spt as $key => $part) {
                    $sort[$key] = strtotime($part['time']);
                }
                array_multisort($sort, SORT_DESC, $spt);
	            echo "<pre />";
	            print_r($spt);
            }else{
	            echo "nothing to show";
            }
        }else{
            $spt = queryOTP(0);
            echo "<pre />";
            print_r($spt);
        }
    }

    public function split_pdf($split = 3){
        $this->load->library("Fpdf_gen");
//        $pdf = "../uploads/baboo-docs/Documentation.pdf";
//        $pdf_url = "http://s3-ap-southeast-1.amazonaws.com/s3-stg.baboo.id/baboo-docs/Austreibung_der_Juden_aus_Russland_.Expulsion_of_the_Jews_from_Russia.pdf";
        $pdf_url = "http://localhost/uploads/baboo-docs/docs_1527232005.pdf";
        $uploaddir = "../uploads/baboo-docs/";
        $pdf = $uploaddir."docs_".time().".pdf";
        file_put_contents($pdf, fopen($pdf_url, 'r'));
        $password = '37c3a6b2a38c1d3287e228dac1ef6209b52ee828366c5417710e4eaf1dd2f8352564adbe65ca11f17b23c3e6c41a82e4adbf835ac4ddc1e0e8ae318368d8ff73';
//        $files = file_get_contents($pdf);
        if(file_exists($pdf)){
            $pdf_real = realpath($pdf);
            $new_name = str_replace('.pdf', '', basename($pdf))."_decrypted.pdf";
            $location_pdf = realpath($uploaddir)."/$new_name";
            exec("qpdf --decrypt $pdf_real --password=$password $location_pdf", $output, $return);
            if(!$return){
                $gs = "gs";
                $decrypted_pdf = $uploaddir.$new_name;
                $newest_name = str_replace('.pdf', '', basename($decrypted_pdf))."_1.4.pdf";
                $locations_pdf = realpath($uploaddir)."/$newest_name";
                if(strpos(base_url(), REST_Controller::api_local) !== false) $gs = "gswin64c";
                exec("$gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dBATCH -sOutputFile=$locations_pdf $location_pdf", $output, $return);
//                print_r($output);
                $resp = $this->fpdf_gen->concat($uploaddir.$newest_name, $split, $password);
                print_r($resp);
            }else{
                echo $location_pdf;
            }
        }
    }
}
