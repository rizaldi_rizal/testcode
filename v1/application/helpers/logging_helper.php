<?php
defined('BASEPATH') OR exit('No direct script access allowed');


if (!class_exists("REST_Controller")) {
    require_once APPPATH . 'libraries/REST_Controller.php';
}

if (!function_exists('file_user_log')) {
    function file_user_log($user, $message, $status_code = REST_Controller::HTTP_OK)
    {
        $ci =& get_instance();
        $headers = getallheaders();
        $rename = false;
        $saved = false;
        $stat_log = false;
        $file_path = '../../apis_log.txt';
        if (strpos(base_url(), REST_Controller::api_staging) !== false) {
            $file_path = REST_Controller::stgp_log;
        } elseif (strpos(base_url(), REST_Controller::api_dev) !== false) {
            $file_path = REST_Controller::dev_log;
        } elseif (strpos(base_url(), REST_Controller::api_prod) !== false) {
            $file_path = REST_Controller::prod_log;
        }
        $lastdate = (file_exists($file_path)) ? filemtime($file_path) : strtotime(date("Y-m-d"));
        if ($lastdate && !empty($lastdate) && strtotime(date("Y-m-d")) != $lastdate) {
            $rname = $file_path . "-" . date("Ymd", $lastdate);
            rename($file_path, $rname);
            $rename = true;
        }
        //checking if not empty user data
        if (!empty($user)) {
            $uid = getUsersById($user);
            if (!empty($uid)) {
                $user = $uid["fullname"];
            }
        }
        $IParray = array_values(array_filter(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])));
        $ip = end($IParray);
        if (!$ip || empty($ip)) $ip = $_SERVER["REMOTE_ADDR"];
        $log_message = $ip . ' - - [' . date("d/M/Y:H:i:s O") . '] "' . $_SERVER["REQUEST_METHOD"] . ' ' . $_SERVER["REQUEST_URI"] . ' ' . $_SERVER["SERVER_PROTOCOL"] . '" ' . $status_code . ' ' . intval($headers[strtolower('Content-Length')]) . ' "-" "' . $_SERVER["HTTP_USER_AGENT"] . '" - "' . $user . '" ' . $message . "\n";
        // Write to the file.
        if (file_put_contents($file_path, $log_message, FILE_APPEND)) {
            $saved = true;
            $stat_log = TRUE;
        }
        //checkig if already saved to file log and renamed (changed date)
        if ($saved == true && $rename == true) {
            $ci->load->library("zip");
            $compartment = $file_path . "-";
            $files = glob("$compartment*");
            if ($files && is_array($files) && count($files) >= 5) {
                $undata = array();
                $ci->zip->add_dir("log");
                foreach ($files as $file) {
                    $info = pathinfo($file);
                    $undata[] = $info["dirname"] . "/" . $info["basename"];
                    $ci->zip->add_data("log/" . $info["basename"], file_get_contents($info["dirname"] . "/" . $info["basename"]));
                }
                $ci->zip->archive("/home/devbaboo/apis_log_weekly_" . date("dmY") . ".gz");
                if (count($undata) > 0) {
                    foreach ($undata as $unfile) {
                        if (file_exists($unfile)) {
                            unlink($unfile);
                        }
                    }
                }
            }
        }

        return $stat_log;
    }
}