<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!class_exists("REST_Controller")) {
    require_once APPPATH . 'libraries/REST_Controller.php';
}

if (!function_exists('strip_tags_with_whitespace')) {
    function strip_tags_with_whitespace($string, $allowable_tags = null)
    {
        $string = str_replace('<', ' <', $string);
        $string = strip_tags($string, $allowable_tags);
        $string = str_replace('  ', ' ', $string);
        $string = trim($string);

        return html_entity_decode($string, ENT_QUOTES, 'UTF-8');
    }
}
if (!function_exists('sendsms')) {
    function sendsms($number, $message_body, $return = '0')
    {

        $sender = 'SEDEMO'; // Need to change

        $smsGatewayUrl = 'http://springedge.com';

        $apikey = '62q3z3hs4941mve32s9kf10fa5074n7'; // Need to change

        $textmessage = urlencode($message_body);

        $api_element = '/api/web/send/';

        $api_params = $api_element . '?apikey=' . $apikey . '&sender=' . $sender . '&to=' . $number . '&message=' . $textmessage;
        $smsgatewaydata = $smsGatewayUrl . $api_params;

        $url = $smsgatewaydata;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_POST, false);

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);

        curl_close($ch);

        if (!$output) {
            $output = file_get_contents($smsgatewaydata);
        }

        if ($return == '1') {
            return $output;
        } else {
            return "Sent, up to 10 minutes to verify or you need to resend the message again";
        }

    }
}
if (!function_exists('send_android_notification')) {
    function send_android_notification($registration_ids, $message, $type = "", $param = "")
    {
        $return_array = array();
        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => array(
                "type" => $type,
                "param" => $param,
                "desc" => $message
            ),
            'priority' => 10
        );
        $headers = array(
            'Authorization: key='.FIREBASE_KEYS, // FIREBASE_API_KEY_FOR_ANDROID_NOTIFICATION
            'Content-Type: application/json'
        );
        $return_array["fields"] = $fields;
        $return_array["headers"] = $headers;
// Open connection
        $ch = curl_init();
// Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
// Execute post
        $result = curl_exec($ch);
        if ($result === false) {
            die('Curl failed:' . curl_errno($ch));
        }
// Close connection
        curl_close($ch);
        $return_array["result"] = $result;
        return $return_array;
    }
}

if (!function_exists('time_ago')) {
    function time_ago($timestamp)
    {
        $time_ago = strtotime($timestamp);
        $current_time = time();
        $time_difference = $current_time - $time_ago;
        $seconds = $time_difference;
        $minutes = round($seconds / 60);           // value 60 is seconds
        $hours = round($seconds / 3600);           //value 3600 is 60 minutes * 60 sec
        $days = round($seconds / 86400);          //86400 = 24 * 60 * 60;
        $weeks = round($seconds / 604800);          // 7*24*60*60;
        $months = round($seconds / 2629440);     //((365+365+365+365+366)/5/12)*24*60*60
        $years = round($seconds / 31553280);     //(365+365+365+365+366)/5 * 24 * 60 * 60
        if ($seconds <= 60) {
            return "baru saja";
        } else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "semenit yang lalu";
            } else {
                return "$minutes menit yang lalu";
            }
        } else if ($hours <= 24) {
            if ($hours == 1) {
                return "sejam yang lalu";
            } else {
                return "$hours jam yang lalu";
            }
        } else if ($days <= 7) {
            if ($days == 1) {
                return "kemarin";
            } else {
                return "$days hari yang lalu";
            }
        } else if ($weeks <= 4.3) //4.3 == 52/12
        {
            if ($weeks == 1) {
                return "seminggu yang lalu";
            } else {
                return "$weeks minggu yang lalu";
            }
        } else if ($months <= 12) {
            if ($months == 1) {
                return "sebulan yang lalu";
            } else {
                return "$months bulan yang lalu";
            }
        } else {
            if ($years == 1) {
                return "setahun yang lalu";
            } else {
                return "$years tahun yang lalu";
            }
        }
    }
}

if (!function_exists("getAssetsEnv")) {
    function getAssetsEnv($a = '')
    {
        $ci =& get_instance();
        if (empty($a)) $a = base_url();
        $assets_serv = REST_Controller::assets_dev;
        if (strpos($a, REST_Controller::api_staging) !== false) {
            $assets_serv = REST_Controller::assets_stg;
        } elseif (strpos($a, REST_Controller::api_prod) !== false) {
            $assets_serv = REST_Controller::assets_prod;
        }
        return $assets_serv;
    }
}

if (!function_exists("otp_path")) {
    function otp_path()
    {
        $file_path = '../../otp.txt';
        if (strpos(base_url(), REST_Controller::api_staging) !== false) {
            $file_path = REST_Controller::stgprod_otp;
        } elseif (strpos(base_url(), REST_Controller::api_dev) !== false) {
            $file_path = REST_Controller::dev_otp;
        } elseif (strpos(base_url(), REST_Controller::api_prod) !== false) {
            $file_path = REST_Controller::stgprod_otp;
        }
        return $file_path;
    }
}

if (!function_exists("sms_api")) {
    function sms_api($no_telp = "", $otp = "")
    {
        $user = REST_Controller::user_otp_devstg;
        $pass = REST_Controller::pass_otp_devstg;
        if (strpos(base_url(), REST_Controller::api_staging) !== false || strpos(base_url(), REST_Controller::api_dev) !== false) {
            $user = REST_Controller::user_otp_prod;
            $pass = REST_Controller::pass_otp_prod;
        }

        $message = $otp . " adalah code verifikasi akun Baboo Anda";
        $message = urlencode($message);
        $url_api = (strpos(base_url(), REST_Controller::api_local) !== false) ? REST_Controller::sms_gateway_local : REST_Controller::sms_gateway_stage;
        $api_url = $url_api . "?u=$user&p=$pass&d=$no_telp&m=$message";
        $ch = curl_init();
        $curl_options = array(
            CURLOPT_URL => $api_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POST => false,
            CURLOPT_SSL_VERIFYPEER => 0,
        );
        curl_setopt_array($ch, $curl_options);
        $result = curl_exec($ch);
        if ($result) {
            return $result;
        } else {
            return curl_error($ch);
        }
    }
}
if (!function_exists("otp_send")) {
    function otp_send($no_telp = "", $otp = "")
    {
        $key = REST_Controller::api_key_sms;

        $message = $otp . " adalah code verifikasi akun Truckin Anda";
        $message = urlencode($message);
        $url_api = REST_Controller::sms_gateway_stage;
        $senddata = array(
            'apikey' => $key,  
            'callbackurl' => REST_Controller::callbackurl_otp, 
            'senderid' => REST_Controller::callbackurl_sender_id, 
            'datapacket'=>array()
        );

        $number= $no_telp;
        $message='Kode OTP Truckin anda '. $otp;
        array_push($senddata['datapacket'],array(
            'number' => trim($number),
            'message' => $message
        ));
        // $api_url = $url_api . "?u=$user&p=$pass&d=$no_telp&m=$message";

        $data=json_encode($senddata);
        $curlHandle = curl_init($url_api);
        curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
        );
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
        curl_setopt($curlHandle, CURLOPT_CONNECTTIMEOUT, 30);
        $respon = curl_exec($curlHandle);

        $curl_errno = curl_errno($curlHandle);
        $curl_error = curl_error($curlHandle);  
        $http_code  = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
        curl_close($curlHandle);
        if ($curl_errno > 0) {
            $senddatax = array(
            'sending_respon'=>array(
                'globalstatus' => 90, 
                'globalstatustext' => $curl_errno."|".$http_code)
            );
            $respon=json_encode($senddatax);
        } else {
            if ($http_code<>"200") {
                $senddatax = array(
                'sending_respon'=>array(
                    'globalstatus' => 90, 
                    'globalstatustext' => $curl_errno."|".$http_code)
                );
                $respon= json_encode($senddatax);   
            }
        }

        if ($respon) {
            return $respon;
        } else {
            return curl_error($ch);
        }

        // $ch = curl_init();
        // $curl_options = array(
        //     CURLOPT_URL => $api_url,
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_CUSTOMREQUEST => "GET",
        //     CURLOPT_POST => false,
        //     CURLOPT_SSL_VERIFYPEER => 0,
        // );
        // curl_setopt_array($ch, $curl_options);
        // $result = curl_exec($ch);
        // if ($result) {
        //     return $result;
        // } else {
        //     return curl_error($ch);
        // }
    }
}
if (!function_exists("queryOTP")) {
    function queryOTP($no_telp)
    {
        ini_set('memory_limit', '-1');
        $searchthis = $no_telp;
        $matches = array();
        $arr_otp = array();
        $otp = "";
        $dateTime = "";
        $file_path = otp_path();
        $handle = @fopen($file_path, "r");
        if ($handle) {
            while (!feof($handle)) {
                $buffer = fgets($handle);
                if (strpos($buffer, $searchthis) !== FALSE)
                    $matches[] = $buffer;
            }
            fclose($handle);
        } else {
            $arr_otp[] = array(
                "otp" => $otp,
                "time" => $dateTime
            );
        }
        //show results:
        if (count($matches) > 0) {
            foreach ($matches as $matchav) {
                $satire = explode("|", $matchav);
                if (count($satire) > 0) {
                    $otp = $satire[1];
                    $dateTime = $satire[2];
                    $arr_otp[] = array(
                        "otp" => $otp,
                        "time" => $dateTime
                    );
                } else {
                    $arr_otp[] = array(
                        "otp" => $otp,
                        "time" => $dateTime
                    );
                }
            }
        } else {
            $arr_otp[] = array(
                "otp" => $otp,
                "time" => $dateTime
            );
        }

        return $arr_otp;
    }
}

if (!function_exists("getOTP")) {
    function getOTP($no_telp)
    {
        $ci =& get_instance();
        $otp = "";
        $dateTime = "";
        if (!empty($no_telp)) {
            $cekOTP = $ci->db->get_where("otp", array("phone" => $no_telp));
            if ($cekOTP->num_rows() > 0) {
                $otp = $cekOTP->row()->otp;
                $dateTime = $cekOTP->row()->created_by;
            }
        }
        $arr_otp = array(
            "otp" => $otp,
            "time" => $dateTime
        );
        return $arr_otp;
    }
}

if (!function_exists("cekOTP")) {
    function cekOTP($no_telp, $otp)
    {
        $arr_otp = false;
        if (!empty($no_telp) && !empty($otp)) {
            $gotp = getOTP($no_telp);
            if ($gotp && count($gotp) > 0) {
                if ($otp == $gotp["otp"]) {
                    $arr_otp = true;
                }
            }
        }

        return $arr_otp;
    }
}

if (!function_exists("delOTP")) {
    function delOTP($no_telp)
    {
        $ci =& get_instance();
        $stat = FALSE;
        if (!empty($no_telp)) {
            $cekOTP = $ci->db->get_where("otp", array("phone" => $no_telp));
            if ($cekOTP->num_rows() > 0) {
                $del = $ci->db->delete("otp", array("phone" => $no_telp));
                if ($del) $stat = TRUE;
            }
        }
        return $stat;
    }
}

if (!function_exists("logOTP")) {
    function logOTP($no_telp, $otp = "")
    {
        $ci =& get_instance();
        $cekidot = false;
        if (!empty($no_telp) && !empty($otp)) {
            $ceks = $ci->db->get_where("otp", array("phone" => $no_telp));
            if ($ceks->num_rows() > 0) $ci->db->delete("otp", array("phone" => $no_telp));
            $ci->db->insert("otp", array("phone" => $no_telp, "otp" => $otp));
            $cekidot = $ci->db->insert_id();
        }
        ini_set('memory_limit', '-1');
        $file_path = otp_path();
        $log_message = $no_telp . "|" . $otp . "|" . date("Y-m-d H:i:s") . PHP_EOL;
        // if (file_put_contents($file_path, $log_message, FILE_APPEND) && $cekidot) {
        //     sms_api($no_telp, $otp);
        //     return TRUE;
        // } else return FALSE;
        otp_send($no_telp, $otp);
        return TRUE;
    }
}

if (!function_exists("deleteOTP")) {
    function deleteOTP($no_telp)
    {
        ini_set('memory_limit', '-1');
        $file_path = otp_path();
        if (file_exists($file_path)) {
            $rows = file($file_path);
            foreach ($rows as $key => $row) {
                if (preg_match('/^\+\d+$/', $no_telp, $row)) {
                    unset($rows[$key]);
                }
            }
            if (file_put_contents($file_path, implode("\n", $rows))) return TRUE;
            else return FALSE;
        } else return TRUE;
    }
}

if (!function_exists("count_paging")) {
    function count_paging($count = 0, $limit = REST_Controller::limit_timeline)
    {
        if (!empty($count) && (int)$count > 1) {
            $mcount = (int)$count - 1;
            $county = $mcount * $limit;
            $count = $county;
        }
        return $count;
    }
}

if (!function_exists("randoms_otp")) {
    function randoms_otp()
    {
        $digits = REST_Controller::count_random_otp;
        return rand(pow(10, $digits - 1), pow(10, $digits) - 1);
    }
}

if (!function_exists('setHeaders')) {
    function setHeaders()
    {
        $ci =& get_instance();
        $headers = array();
        foreach (getallheaders() as $name => $value) {
            $name = strtolower($name);
            $headers[$name] = $value;
        }
        $key = $headers["zal-auth-key"];
        $new_key = regenerateKey($key, '', '');
        if (empty($new_key)) {
            $ci->set_response(array("code" => REST_Controller::HTTP_FORBIDDEN, "message" => "Invalid API Key"), REST_Controller::HTTP_FORBIDDEN);
        }
        return $new_key;
    }
}

if (!function_exists('password_encrypt')) {
    function password_encrypt($password = '')
    {
        $ci =& get_instance();
        $ci->load->library("encrypt");
        $password = $ci->encrypt->encode($password);
        return $password;
    }
}

if (!function_exists('password_decrypt')) {
    function password_decrypt($hashpassword = '')
    {
        $ci =& get_instance();
        $ci->load->library("encrypt");
        $hashpassword = $ci->encrypt->decode($hashpassword);
        return $hashpassword;
    }
}

if (!function_exists('recheck_publish')) {
    function recheck_publish($book_id)
    {
        $ci =& get_instance();
        $stat = FALSE;
        if (!empty($book_id)) {
            $stat = TRUE;
            $ci->db->select("book_id, author_id, title_book, title_draft, status_publish, category_id, latest_update, publish_at, file_cover, image_url");
            $gdbb = $ci->db->get_where("book", array("book_id" => $book_id));
            if ($gdbb->num_rows() > 0) {
                if (!empty($gdbb->row()->title_draft)) {
                    $ci->db->update("book", array("title_book" => $gdbb->row()->title_draft), array("book_id" => $book_id));
                }
                $ci->db->select("chapter_id, chapter_draft, chapter_title");
                $gchapps = $ci->db->get_where("chapter", array("id_book" => $book_id));

                if ($gchapps->num_rows() > 0) {
                    foreach ($gchapps->result() as $dckey => $gchavals) {
                        $chapterer_id = $gchavals->chapter_id;
                        if (!empty($gchavals->chapter_draft) || !empty($ci->post("chapter_start"))) {

                            if (!empty($gchavals->chapter_draft)) {
                                $ci->db->set("chapter_draft", 'NULL', false);
                                $ci->db->set(array("chapter_title" => $gchavals->chapter_draft));
                            }
                            //check if empty
                            if (!empty($ci->post("chapter_start")) && $ci->post("is_paid") == true) {
                                $chapter_start = (int)$ci->post("chapter_start");
                                if (!empty($chapter_start)) $chapter_start = $chapter_start - 1;
                                if ($dckey >= $chapter_start) {
                                    $ci->db->set(array("status_free" => "not free"));
                                } else {
                                    $ci->db->set(array("status_free" => "free"));
                                }
                            } else {
                                $ci->db->set(array("status_free" => "free"));
                            }
                        }

                        if ($ci->post("is_paid") == false) {
                            $ci->db->set(array("status_free" => "free"));
                        }

                        $ci->db->set("status_publish", 1);
                        $ci->db->where(array("chapter_id" => $chapterer_id));
                        $upd = $ci->db->update("chapter");
//                            if(!$upd) $last_query = implode(",", $ci->db->error());
                        if (!$upd) $stat = FALSE;
                        else $stat = TRUE;


                        $sdata_pharer = $ci->db->get_where("draft_paragraph", array("id_chapter" => $gchavals->chapter_id));
                        if ($sdata_pharer->num_rows() > 0) {
                            $pchd = $ci->db->get_where("paragraph", array("id_chapter" => $sdata_pharer->row()->id_chapter));
                            if ($pchd->num_rows() > 0) {
                                $ci->db->delete("paragraph", array("id_chapter" => $sdata_pharer->row()->id_chapter));
                            }
                            $insssa = 0;
                            foreach ($sdata_pharer->result() as $pharava) {
                                $sxpharray = array(
                                    "id_chapter" => $pharava->id_chapter,
                                    "txt_paragraph" => $pharava->txt_paragraph
                                );
                                $inz = $ci->db->insert("paragraph", $sxpharray);
                                if ($inz) $insssa = $ci->db->insert_id();
                            }
                            if ($insssa && !empty($insssa)) $ci->db->delete("draft_paragraph", array("id_chapter" => $chapterer_id));
                        }
                    }
                } else {
                    $stat = FALSE;
                }
            } else {
                $stat = FALSE;
            }
        }
        return $stat;
    }
}

if (!function_exists('my_encrypt')) {
    function my_encrypt($data)
    {
        $ci =& get_instance();
        $ci->load->library('encryption');
        $ci->encryption->initialize(
            array(
                'cipher' => 'aes-256',
                'driver' => 'openssl',
                'mode' => 'ctr'
            )
        );
        return $ci->encryption->encrypt($data);
    }
}

if (!function_exists('my_decrypt')) {
    function my_decrypt($data)
    {
        // Remove the base64 encoding from our key
        $ci =& get_instance();
        $ci->load->library('encryption');
        $ci->encryption->initialize(
            array(
                'cipher' => 'aes-256',
                'driver' => 'openssl',
                'mode' => 'ctr'
            )
        );
        return $ci->encryption->decrypt($data);
    }
}

if (!function_exists('sethead')) {
    function sethead()
    {
        $headers = array();
        foreach (getallheaders() as $name => $value) {
            $name = strtolower($name);
            $headers[$name] = $value;
        }
        $lastKey = isset($headers["zal-auth-key"]) ? $headers["zal-auth-key"] : "";
        return $lastKey;
    }
}

if (!function_exists('getUserFromKey')) {
    function getUserFromKey($key = '')
    {
        $user_id = 0;
        if (!empty($key)) {
            $users = my_decrypt($key);
            if (!empty($users)) {
                $userz = explode("#", $users);
                if (!empty($userz)) {
                    $user_id = $userz[0];
                    $user_id = (int)$user_id;
                }
            }
        }
        return $user_id;
    }

}

if (!function_exists('getUsersById')) {
    function getUsersById($user_id = '')
    {
        $result_user = array();
        $ci =& get_instance();
        if (!empty($user_id)) {
            $ci->db->select("user_id, fullname, email, IFNULL(date_of_birth, '') as date_of_birth,  IFNULL(jk, '') as jk, IFNULL(prof_pict, '') as prof_pict, IFNULL(address, '') as address,  IFNULL(about_me, '') as about_me, book_sold, balance, IFNULL(phone, '') as phone");
            $usxgf = $ci->db->get_where("users", array("user_id" => $user_id));
            if ($usxgf->num_rows() > 0) {
                $result_user = $usxgf->row_array();
            }
        }
        return $result_user;
    }
}

if (!function_exists('getCategoryBook')) {
    function getCategoryBook($category_id = '')
    {
        $ci =& get_instance();
        $category = "Science Fiction";
        if (!empty($category_id)) {
            $ci->db->where(array("id_catbook" => $category_id));
        } else {
            $ci->db->limit(1);
            $ci->db->order_by("id_catbook", "asc");
        }
        $ci->db->select("cat_book");
        $getCat = $ci->db->get("book_category");
        if ($getCat && $getCat->num_rows() > 0) $category = (string)$getCat->row()->cat_book;
        return $category;
    }
}

if (!function_exists('checkIsLike')) {
    function checkIsLike($book_id = '', $user_id = '')
    {
        $ci =& get_instance();
        $is_likes = false;
        if (!empty($book_id) && !empty($user_id)) {
            $getlikes = $ci->db->get_where("likes", array("id_book" => $book_id, "id_user" => $user_id));
            if ($getlikes->num_rows() > 0) $is_likes = true;
        }
        return $is_likes;
    }
}

if (!function_exists('checkIsFollow')) {
    function checkIsFollow($user_follow = '', $user_id = '')
    {
        $keyz = sethead();
        if (empty($user_id)) $user_id = getUserFromKey($keyz);
        $ci =& get_instance();
        $is_follow = false;
        if (!empty($user_id)) {
            $ci->db->select("created_by");
            $cekfollow = $ci->db->get_where("follows", array("created_by" => $user_id, "is_follow" => $user_follow));
            if ($cekfollow->num_rows() > 0) $is_follow = true;
//            if ($cekfollow->num_rows() > 0 || $user_follow == $user_id) $is_follow = true;
        }
        return $is_follow;
    }
}

if (!function_exists('checkIsBookmark')) {
    function checkIsBookmark($book_id = '', $user_id = '')
    {
        $ci =& get_instance();
        $is_bookmark = false;
        if (!empty($book_id) && !empty($user_id)) {
            $ci->db->select("id_book, id_user");
            $getbookmark = $ci->db->get_where("bookmark", array("id_book" => $book_id, "id_user" => $user_id));
            if ($getbookmark->num_rows() > 0) $is_bookmark = true;
        }
        return $is_bookmark;
    }
}

if (!function_exists("array_diff_assoc_recursive")) {
    function array_diff_assoc_recursive($array1, $array2)
    {
        foreach ($array1 as $key => $value) {
            if (is_array($value)) {
                if (!isset($array2[$key])) {
                    $difference[$key] = $value;
                } elseif (!is_array($array2[$key])) {
                    $difference[$key] = $value;
                } else {
                    $new_diff = array_diff_assoc_recursive($value, $array2[$key]);
                    if ($new_diff != FALSE) {
                        $difference[$key] = $new_diff;
                    }
                }
            } elseif (!isset($array2[$key]) || $array2[$key] != $value) {
                $difference[$key] = $value;
            }
        }
        return !isset($difference) ? array() : $difference;
    }
}

if (!function_exists("check_diff_multi")) {
    function check_diff_multi($array1, $array2)
    {
        foreach ($array1 as $keya => $valuea) {
            if (in_array($valuea, $array2)) {
                unset($array1[$keya]);
            }
        }
        return $array1;
    }
}

if (!function_exists("search_array_key")) {
    function search_array_key($array, $term)
    {
        foreach ($array AS $key => $value) {
            if (strpos($key, $term) === FALSE) {
                continue;
            } else {
                return $key;
            }
        }

        return FALSE;
    }
}

if (!function_exists("search_array_val")) {
    function search_array_val($array, $term)
    {
        foreach ($array AS $key => $value) {
            if (strpos($key, $term) === FALSE) {
                continue;
            } else {
                return $value;
            }
        }

        return FALSE;
    }
}

if (!function_exists("update_paragraph")) {
    function update_paragraph($book_id = '', $chapter_id = '', $new_par = '')
    {
        $ci =& get_instance();
        $resp_code = REST_Controller::HTTP_NOT_FOUND;
        $sdata_phar = array();
        $compare_phar = array();
        $arr_data = array();
        if (empty($chapter_id)) {
            $ci->db->where("id_chapter IN(SELECT chapter_id FROM chapter WHERE id_book = $book_id ORDER BY chapter_id ASC LIMIT 1)");
        } else {
            $ci->db->where(array("id_chapter" => $chapter_id));
        }
        $gpar = $ci->db->get("paragraph");
        if ($gpar->num_rows() > 0) {
            foreach ($gpar->result() as $gpkey => $gpval) {
                $compare_phar[$gpval->paragraph_id . "-" . $gpkey] = $gpval->txt_paragraph;
            }
        }
        if (!empty($new_par)) $sdata_phar = preg_split(REST_Controller::delimiter_paragraph, $new_par, null, PREG_SPLIT_NO_EMPTY);
        if (!empty($sdata_phar) && !empty($compare_phar)) {
            $data = check_diff_multi($sdata_phar, $compare_phar);
            foreach ($data as $dakey => $daval) {
                $data_update["txt_paragraph"] = $daval;
                $parid = $dakey;
                $paragraph_id = search_array_key($compare_phar, "-" . $parid);
                if ($paragraph_id) {
                    $dzray = explode("-", $paragraph_id);
                    $parid = $dzray[0];
                    $where["paragraph_id"] = $parid;
                    $cepar = $ci->db->get_where("paragraph", $where);
                    if ($cepar->num_rows() > 0) {
                        $upd = $ci->db->update("paragraph", $data_update, $where);
                        if (!$upd) $resp_message = "ada error di " . implode(",", $ci->db->error());
                    }
                } else {
                    $data_update["id_chapter"] = $chapter_id;
                    $upd = $ci->db->insert("paragraph", $data_update);
                    if (!$upd) $resp_message = "ada error di " . implode(",", $ci->db->error());
                }
                //checking if stored data is less than the database


                if ($upd) $resp_code = REST_Controller::HTTP_OK;
                elseif (!$upd) $resp_message = "ada error di " . implode(",", $ci->db->error());
            }

            if (count($sdata_phar) < count($compare_phar)) {
                foreach ($compare_phar as $sadako => $sadaval) {
                    $kokey = explode("-", $sadako);
                    $pardi = $kokey[1];
                    $arr_keyd = search_array_key($sdata_phar, $pardi);
                    if ($arr_keyd === false) {
                        $paragraphs_id = $kokey[0];
                        $ggdp = $ci->db->get_where("paragraph", array("paragraph_id" => $paragraphs_id));
                        if (!empty($paragraphs_id) && $ggdp->num_rows() > 0) {
                            $arr_data[] = $paragraphs_id;
                            $resp_message = "aneh";
                            $del = $ci->db->delete("paragraph", array("paragraph_id" => $paragraphs_id));
                            if (!$del) {
                                $resp_message = "ada error di " . implode(",", $ci->db->error());
                            } else {
                                $resp_code = REST_Controller::HTTP_OK;
                            }
                        }
                    }
                }
            }

        } elseif (!empty($sdata_phar) && empty($compare_phar)) {
            foreach ($sdata_phar as $pharv) {
                $pharray = array(
                    "id_chapter" => $chapter_id,
                    "txt_paragraph" => $pharv
                );
                $ci->db->insert("paragraph", $pharray);
                $insss = $ci->db->insert_id();
                if ($insss) $resp_code = REST_Controller::HTTP_OK;
            }
        }

        if ($resp_code == REST_Controller::HTTP_OK) $resp_message = "success";

        $resp_data = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $arr_data
        );
        return $resp_data;
    }
}

if (!function_exists('getParagraphByBook')) {
    function getParagraphByBook($book_id = '', $chapter_id = '')
    {
        $ci =& get_instance();
        $res_par_text = "";
        $limit_text = REST_Controller::text_limits;
        if (!empty($book_id)) {
            $stat_publish = "publish";
            $ci->db->select("book_id, title_book, status_publish");
            $books = $ci->db->get_where("book", array("book_id" => $book_id));
            if ($books && $books->num_rows() > 0) {
                $stat_publish = $books->row()->status_publish;
            }
            if (empty($chapter_id)) {
                $ci->db->limit(5, 0);
                $ci->db->order_by("chapter_id", "asc");
                $ci->db->where(array("id_book" => $book_id));
            } else {
                $ci->db->where(array("chapter_id" => $chapter_id));
            }
            $ci->db->select("chapter_id, chapter_title, chapter_draft, status_free");
            $schapx = $ci->db->get("chapter");
            if ($schapx->num_rows() > 0) {
                foreach($schapx->result() as $svalx){
                    // $svalx = $schapx->row();
                    if ($stat_publish == "publish") {
                        $ci->db->order_by("paragraph_id", "asc");
                        $ci->db->select("txt_paragraph");
                        $gepars = $ci->db->get_where("paragraph", array("id_chapter" => $svalx->chapter_id));
                    } else {
                        $ci->db->select("txt_paragraph");
                        $gepars = $ci->db->get_where("draft_paragraph", array("id_chapter" => $svalx->chapter_id));
                        if ($gepars->num_rows() == 0) {
                            $ci->db->select("txt_paragraph");
                            $gepars = $ci->db->get_where("paragraph", array("id_chapter" => $svalx->chapter_id));
                        }
                    }
                    foreach ($gepars->result() as $par_resultx => $paragraphv) {
                        $par_text = strip_tags_with_whitespace($paragraphv->txt_paragraph);
                        $limit_count = $gepars->num_rows() - 1;
                        if ($par_resultx != $limit_count) $par_text .= " ";
                        $res_par_text .= $par_text;
                    }
                    if (strlen($res_par_text) > $limit_text) {
                        $res_par_text = substr($res_par_text, 0, $limit_text) . "...";
                        break;
                    }
                }
            }
        }

        return $res_par_text;
    }

}

if (!function_exists('setStats')) {
    function setStats($stat_publish)
    {
        $retval = array();
        switch ($stat_publish) {
            case "publish" :
                $retval = array(
                    "status_id" => 2,
                    "status_desc" => $stat_publish,
                );
                break;
            case "draft" :
                $retval = array(
                    "status_id" => 1,
                    "status_desc" => $stat_publish,
                );
                break;
            case "on editor" :
                $retval = array(
                    "status_id" => 3,
                    "status_desc" => "edit publish",
                );
                break;
            default :
                $retval = array(
                    "status_id" => 3,
                    "status_desc" => $stat_publish,
                );
                break;
        }
        return $retval;
    }
}

if (!function_exists('getFinalisBook')) {
    function getFinalisBook($count = 0, $startDate = '2018-02-11', $endDate = '2018-03-30')
    {
        $ci =& get_instance();
        $madray = array();
        $getEvent = $ci->db->get_where("events_baboo", "NOW() >= event_start AND NOW() <= event_end");
        if ($getEvent->num_rows() > 0) {
            foreach ($getEvent->result() as $gavel) {
                if (!empty($startDate) && !empty($endDate)) {
                    $startDate = $gavel->event_start;
                    $endDate = $gavel->event_end;
                    $eventId = $gavel->event_id;
                    $finalize = $ci->db->query("SELECT book_id as popular_book_id, IFNULL(title_book, '') as popular_book_title, author_id as popular_author_id, fullname as popular_author_name, IFNULL(prof_pict,'') as popular_author_avatar, IFNULL(file_cover,'') as popular_cover_url, ((" . REST_Controller::percent_share_event . " * (SELECT count(*) FROM shares WHERE id_book = book_id AND date(created) BETWEEN '$startDate' AND '$endDate')) + (" . REST_Controller::percent_like_event . " * (SELECT count(*) FROM likes WHERE id_book = book_id AND date(created) BETWEEN '$startDate' AND '$endDate'))) as score FROM book INNER JOIN users ON book.author_id = users.user_id WHERE date(book.created_date) BETWEEN '$startDate' AND '$endDate' AND author_id IN(SELECT user_id FROM events_detail WHERE event_id = $eventId) ORDER BY score DESC LIMIT ? , ?", array($count, REST_Controller::limit_BestBook));
                    if ($finalize->num_rows() > 0) {
                        $bufi = $finalize->result_array();
                        foreach (array_keys($bufi) as $mfakey) {
                            unset($bufi[$mfakey]["score"]);
                            $bufi[$mfakey]["followers"] = "";
                            $bufi[$mfakey]["book_made"] = "";
                            $bufi[$mfakey]["isFollow"] = false;
                        }
                        $madray = $bufi;
                    }
                }
            }
        }
        return $madray;
    }
}

if (!function_exists('getFinalisLomba')) {
    function getFinalisLomba($count = 0, $startDate = '2018-05-16', $endDate = '2018-06-11')
    {
        $ci =& get_instance();
        $madray = array();
        $keyes = sethead();
        $useres = ($keyes && !empty($keyes)) ? getUserFromKey($keyes) : "";
        $isFollow = (!empty($useres)) ? checkIsFollow($useres) : false;
        $getEvent = $ci->db->get_where("events_baboo", "NOW() >= event_start AND NOW() <= event_end");
        if ($getEvent->num_rows() > 0) {
            $gavel = $getEvent->row();
            $startDate = $gavel->event_start;
            $endDate = $gavel->event_end;
            if (!empty($startDate) && !empty($endDate)) {
                $cat_book = $gavel->event_name;
                $ci->db->select("id_catbook");
                $gcat = $ci->db->get_where("book_category", array("cat_book" => $cat_book));
                $cat_id = ($gcat && $gcat->num_rows() > 0) ? $gcat->row()->id_catbook : 0;
                $finalize = $ci->db->query("SELECT book_id as popular_book_id, IFNULL(title_book, '') as popular_book_title, author_id as popular_author_id, fullname as popular_author_name, IFNULL(prof_pict,'') as popular_author_avatar, IFNULL(file_cover,'') as popular_cover_url, ((" . REST_Controller::percent_share_event . " * (SELECT count(*) FROM shares WHERE id_book = book_id AND date(created) BETWEEN '$startDate' AND '$endDate')) + (" . REST_Controller::percent_like_event . " * (SELECT count(*) FROM likes WHERE id_book = book_id AND date(created) BETWEEN '$startDate' AND '$endDate'))) as score FROM book INNER JOIN users ON book.author_id = users.user_id WHERE book.category_id = ? AND date(book.created_date) BETWEEN '$startDate' AND '$endDate' ORDER BY score DESC LIMIT ? , ?", array($cat_id, $count, REST_Controller::limit_BestBook));
                if ($finalize->num_rows() > 0) {
                    $bufi = $finalize->result_array();
                    foreach (array_keys($bufi) as $mfakey) {
                        unset($bufi[$mfakey]["score"]);
                        $useres_id = $bufi[$mfakey]["popular_author_id"];
                        $followers = $ci->db->query("SELECT COUNT(created_by) as followers FROM follows WHERE is_follow = ?", array($useres_id));
                        $book_made = $ci->db->query("SELECT COUNT(book_id) as book_made FROM book WHERE author_id = ? AND status_publish !=  'draft'", array($useres_id));
                        $bufi[$mfakey]["followers"] = ($followers && $followers->num_rows() > 0) ? (int)$followers->row()->followers : 0;
                        $bufi[$mfakey]["book_made"] = ($book_made && $book_made->num_rows() > 0) ? (int)$book_made->row()->book_made : 0;
                        $bufi[$mfakey]["isFollow"] = $isFollow;
                    }
                    $madray = $bufi;
                }
            }
        }
        return $madray;
    }
}

if (!function_exists('getFinalisWebBook')) {
    function getFinalisWebBook($count = 0, $startDate = '2018-02-11', $endDate = '2018-03-30')
    {
        $ci =& get_instance();
        $keyes = sethead();
        $useres = getUserFromKey($keyes);
        $madray = array();
        $getEvent = $ci->db->get_where("events_baboo", "NOW() >= event_start AND NOW() <= event_end");
        if ($getEvent->num_rows() > 0) {
            foreach ($getEvent->result() as $gavel) {
                if (!empty($startDate) && !empty($endDate)) {
                    $startDate = $gavel->event_start;
                    $endDate = $gavel->event_end;
                    $eventId = $gavel->event_id;
                    $finalize = $ci->db->query("SELECT book_id as popular_book_id, title_book as popular_book_title,  author_id as popular_author_id, fullname as popular_author_name, publish_at, IFNULL(prof_pict,'') as popular_author_avatar, IFNULL(file_cover,'') as popular_cover_url, view_count as popular_book_view, ((" . REST_Controller::percent_share_event . " * (SELECT count(*) FROM shares WHERE id_book = book_id AND date(created) BETWEEN '$startDate' AND '$endDate')) + (" . REST_Controller::percent_like_event . " * (SELECT count(*) FROM likes WHERE id_book = book_id AND date(created) BETWEEN '$startDate' AND '$endDate'))) as score FROM book INNER JOIN users ON book.author_id = users.user_id WHERE date(book.created_date) BETWEEN '$startDate' AND '$endDate' AND author_id IN(SELECT user_id FROM events_detail WHERE event_id = $eventId) ORDER BY score DESC LIMIT ? , ?", array($count, REST_Controller::limit_BestBook));
                    if ($finalize->num_rows() > 0) {
                        $bufi = $finalize->result_array();
                        foreach (array_keys($bufi) as $mfakey) {
                            $book_id = $bufi[$mfakey]["popular_book_id"];
                            if (!empty((int)$book_id)) {
                                $likes = 0;
                                $share = 0;
                                $comment = 0;
                                $likesql = $ci->db->query("SELECT count(*) as total_like FROM likes WHERE id_book = $book_id");
                                if ($likesql && $likesql->num_rows() > 0) $likes = $likesql->row()->total_like;
                                $sharesql = $ci->db->query("SELECT count(*) as total_share FROM `shares` WHERE id_book = $book_id");
                                if ($sharesql && $sharesql->num_rows() > 0) $share = $sharesql->row()->total_share;
                                $commentsql = $ci->db->query("SELECT COUNT(*) as total_comment FROM comment WHERE id_book = $book_id OR id_paragraph IN (SELECT paragraph_id FROM paragraph WHERE id_chapter IN (SELECT chapter_id FROM chapter WHERE id_book = $book_id))");
                                if ($commentsql && $commentsql->num_rows() > 0) $comment = $commentsql->row()->total_comment;
                                $bufi[$mfakey]["popular_book_like"] = $likes;
                                $bufi[$mfakey]["popular_book_share"] = $share;
                                $bufi[$mfakey]["popular_book_comment"] = $comment;
                                $bufi[$mfakey]["popular_book_isLike"] = checkIsLike($book_id, $useres);
                                $bufi[$mfakey]["popular_book_isBookmark"] = checkIsBookmark($book_id, $useres);
                                $bufi[$mfakey]["popular_publish_date"] = time_ago($bufi[$mfakey]["publish_at"]);
                            }
                            unset($bufi[$mfakey]["score"], $bufi[$mfakey]["publish_at"]);
                        }
                        $madray = $bufi;
                    }
                }
            }
        }
        return $madray;
    }
}

if (!function_exists('getFinalisWinner')) {
    function getFinalisWinner($count = 0, $startDate = '2018-02-11', $endDate = '2018-03-30')
    {
        $ci =& get_instance();
        $marray = array();
        $getEvent = $ci->db->get_where("events_baboo", "NOW() >= event_start AND NOW() <= event_end");
        if ($getEvent->num_rows() > 0) {
            foreach ($getEvent->result() as $gavel) {
                if (!empty($startDate) && !empty($endDate)) {
                    $startDate = $gavel->event_start;
                    $endDate = $gavel->event_end;
                    $eventId = $gavel->event_id;
                    $ci->db->select("users.user_id as user_id, fullname, IFNULL(prof_pict, '') as prof_pict, email, (SELECT COUNT(*) FROM follows WHERE is_follow = users.user_id) as followers");
                    $ci->db->join("users", "users.user_id = events_detail.user_id");
                    $booksx = $ci->db->get_where("events_detail", array("event_id" => $eventId, "is_winner" => 1));
                    if ($booksx->num_rows() > 0) {
                        $marray = $booksx->result_array();
                    }
                }
            }
        }
        return $marray;
    }
}

if (!function_exists('topEvent')) {
    function topEvent($eventId = '')
    {
        $ci =& get_instance();
        $marray = array();
        if (!empty($eventID)) $ci->db->where("event_id", $eventId);
        else $ci->db->where("NOW() >= event_start AND NOW() <= event_end");
        $getEvent = $ci->db->get("events_baboo");
        if ($getEvent->num_rows() > 0) {
            $gavel = $getEvent->row();
            $marray["start_date"] = $gavel->event_start;
            $marray["end_date"] = $gavel->event_end;
            $marray["img_mobile"] = $gavel->event_image;
            $marray["img_web"] = $gavel->event_sideimage;
            $marray["img_detail"] = $gavel->event_banner;
            $redirect = $gavel->event_redirect;
            //set for redirecting url
            $marray["url_redirect"] = $redirect;
        }
        return $marray;
    }
}

if (!function_exists('getPopularBook')) {
    function getPopularBook($count = 0, $limit = 0)
    {
        if (empty($limit)) $limit = REST_Controller::limit_BestBook;
        $ci =& get_instance();
        $keyes = sethead();
        $useres = getUserFromKey($keyes);
        $marray = array();
        $booksx = $ci->db->query("SELECT book_id as popular_book_id, title_book as popular_book_title, author_id as popular_author_id, fullname as popular_author_name, IFNULL(prof_pict,'') as popular_author_avatar, IFNULL(file_cover,'') as popular_cover_url, is_pdf, score_weekly_book(book_id) as score FROM book INNER JOIN users ON book.author_id = users.user_id HAVING score > 0 ORDER BY score DESC LIMIT ? , ?", array($count, $limit));
        if ($booksx->num_rows() > 0) {
            $booaks = $booksx->result_array();
            foreach (array_keys($booaks) as $bakey) {
                $is_pdf = $booaks[$bakey]["is_pdf"];
                $book_id = $booaks[$bakey]["popular_book_id"];
                $is_download = false;
                $ci->db->select("is_download");
                if(!empty($useres)){
                    $cekBought = $ci->db->get_where("collections", array("id_user" => $useres, "id_book" => $book_id));
                    if ($cekBought && $cekBought->num_rows() > 0) {
                        if (isset($cekBought->row()->is_download) && $cekBought->row()->is_download == 1) $is_download = true;
                    }
                }
                $booaks[$bakey]["user_id"] = $useres;
                $booaks[$bakey]["is_download"] = $is_download;
                $booaks[$bakey]["is_pdf"] = (!empty($is_pdf)) ? false : true;
                unset($booaks[$bakey]["score"]);
                $booaks[$bakey]["followers"] = "";
                $booaks[$bakey]["book_made"] = "";
                $booaks[$bakey]["isFollow"] = false;
            }
            $marray = $booaks;
        }
        return $marray;
    }
}

if (!function_exists('getSlideBook')) {
    function getSlideBook($count = 0, $limit = 0)
    {
        if (empty($limit)) $limit = REST_Controller::limit_slideBook;

        $ci =& get_instance();
        $marray = array();
        $booksx = $ci->db->query("SELECT book_id as popular_book_id, title_book as popular_book_title, author_id as popular_author_id, fullname as popular_author_name, IFNULL(prof_pict,'') as popular_author_avatar, IFNULL(file_cover,'') as popular_cover_url, book_type, is_pdf, score_weekly_book(book_id) as score, descriptions as popular_book_desc FROM book INNER JOIN users ON book.author_id = users.user_id HAVING score > 0 ORDER BY score DESC LIMIT ? , ?", array($count, $limit));
        if ($booksx && $booksx->num_rows() > 0) {
            $booaks = $booksx->result_array();
            foreach (array_keys($booaks) as $bakey) {
                $is_pdf = $booaks[$bakey]["is_pdf"];
                $booaks[$bakey]["is_pdf"] = (!empty($is_pdf)) ? false : true;
                $book_id = $booaks[$bakey]["popular_book_id"];
                if (!empty($book_id)) {
                    if(!empty($is_pdf) && empty($booaks[$bakey]["popular_book_desc"] )) $booaks[$bakey]["popular_book_desc"] = getParagraphByBook($book_id);
                    else substr($booaks[$bakey]["popular_book_desc"], 0, 30);
                }
                unset($booaks[$bakey]["score"]);
            }
            $marray = $booaks;
        }
        return $marray;
    }
}

if (!function_exists('getPopularWriter')) {
    function getPopularWriter($count = 0, $limit = 0)
    {
        $keyz = sethead();
        $userz = getUserFromKey($keyz);
        $ci =& get_instance();
        if (empty($limit)) $limit = REST_Controller::limit_webPopularWriter;
        $marray = array();
        $booksx = $ci->db->query("SELECT author_id, fullname as author_name, IFNULL(prof_pict,'') as avatar, (SELECT COUNT(*) FROM follows WHERE is_follow = user_id) as followers, IFNULL(category_id, '1') as category, score_weekly_writer(author_id) as score FROM book INNER JOIN users ON book.author_id = users.user_id GROUP BY user_id HAVING score > 0 ORDER BY score DESC LIMIT ? , ?", array($count, $limit));
        if ($booksx->num_rows() > 0) {
            $booaks = $booksx->result_array();
            foreach (array_keys($booaks) as $bakey) {
                unset($booaks[$bakey]["score"]);
                $category_id = $booaks[$bakey]["category"];
                $booaks[$bakey]["category"] = getCategoryBook($category_id);
                if (!empty($userz)) {
                    $is_follow = checkIsFollow($booaks[$bakey]["author_id"], $userz);
                    $booaks[$bakey]["isFollow"] = $is_follow;
                }
            }
            $marray = $booaks;
        }
        return $marray;
    }
}

if (!function_exists('getAllPopularWriter')) {
    function getAllPopularWriter($count = 0)
    {
        $keyz = sethead();
        $userz = getUserFromKey($keyz);
        $ci =& get_instance();
        $limit = REST_Controller::limit_PopularWriter;
        $marray = array();
        $booksx = $ci->db->query("SELECT user_id, fullname, IFNULL(prof_pict,'') as prof_pict, (SELECT COUNT(*) FROM follows WHERE is_follow = user_id) as followers, IFNULL(category_id, '1') as category_name, (SELECT COUNT(*) FROM book WHERE author_id = user_id AND status_publish != 'draft') as book_made, score_weekly_writer(author_id) as score FROM book INNER JOIN users ON book.author_id = users.user_id GROUP BY user_id HAVING score > 0 ORDER BY score DESC LIMIT ? , ?", array($count, $limit));
        if ($booksx->num_rows() > 0) {
            $booaks = $booksx->result_array();
            foreach (array_keys($booaks) as $bakey) {
                unset($booaks[$bakey]["score"]);
                $category_id = $booaks[$bakey]["category_name"];
                $booaks[$bakey]["category_name"] = getCategoryBook($category_id);
                if (!empty($userz)) {
                    $is_follow = checkIsFollow($booaks[$bakey]["user_id"], $userz);
                    $booaks[$bakey]["isFollow"] = $is_follow;
                }
            }
            $marray = $booaks;
        }
        return $marray;
    }
}

if (!function_exists('getMobilePopularWriter')) {
    function getMobilePopularWriter($count = 0, $limit = 0)
    {
        $keyz = sethead();
        $userz = getUserFromKey($keyz);
        $ci =& get_instance();
        if (empty($limit)) $limit = REST_Controller::limit_BestWriter;
        $marray = array();
        $booksx = $ci->db->query("SELECT '' as popular_book_id, '' as popular_book_title, author_id as popular_author_id, fullname as popular_author_name, IFNULL(prof_pict,'') as popular_author_avatar, '' as popular_cover_url, (SELECT COUNT(is_follow) FROM follows WHERE is_follow = user_id) as followers, (SELECT COUNT(book_id) FROM book WHERE author_id = user_id AND status_publish != 'draft') as book_made, score_weekly_writer(author_id) as score FROM book INNER JOIN users ON book.author_id = users.user_id GROUP BY user_id HAVING score > 0 ORDER BY score DESC LIMIT ? , ?", array($count, $limit));
        if ($booksx && $booksx->num_rows() > 0) {
            $booaks = $booksx->result_array();
            foreach (array_keys($booaks) as $bakey) {
                unset($booaks[$bakey]["score"]);
                $is_follow = false;
                if (!empty($userz)) {
                    $is_follow = checkIsFollow($booaks[$bakey]["popular_author_id"], $userz);
                }
                $booaks[$bakey]["isFollow"] = $is_follow;
            }
            $marray = $booaks;
        }
        return $marray;
    }
}

if (!function_exists('setpush_notif')) {
    function setpush_notif($user_id = '', $notif_text = '', $types = '', $params = '')
    {
        $ci =& get_instance();

        if (!class_exists('REST_Controller')) {
            $ci->load->library("REST_Controller");
        }
        //set default types of this if empty
        if (empty($types)) {
            $types = REST_Controller::type_book;
        }

        if (!empty($user_id)) {
            $ci->db->group_by("fcm_id");
            $getdevice = $ci->db->get_where("device", array("uuid" => $user_id));
            if ($getdevice->num_rows()) {
                $registid = array();
                foreach ($getdevice->result() as $gav) {
                    $registid[] = $gav->fcm_id;
                }
                if (empty($params)) $params = $user_id;
                $sndnotif = send_android_notification($registid, $notif_text, $types, $params);
                $activities = "SET NOTIFICATION WITH RESULT : " . (string)json_encode($sndnotif);
                $ci->db->insert("logs", array("user_id" => $user_id, "activities" => $activities, "activities_url" => current_url()));
            }
        }
    }
}

if (!function_exists("getIndexChapter")) {
    function getIndexChapter($book_id = '', $chapter_id = '')
    {
        $ci =& get_instance();
        $keyes = 0;
        if (!empty($book_id) && !empty($chapter_id)) {
            $ci->db->order_by("chapter_id", "asc");
            $ci->db->select("chapter_id");
            $ci->db->where(array("id_book" => $book_id));
            $smchap = $ci->db->get("chapter");
            foreach ($smchap->result() as $smkey => $smval) {
                if ($smval->chapter_id == $chapter_id) {
                    $keyes = $smkey;
                }
            }
        }
        return $keyes;
    }
}

if (!function_exists('set_resp')) {
    function set_resp($responses = array(), $http_response_header = REST_Controller::HTTP_OK)
    {
        $type_data = array();
        $ci =& get_instance();
        if (!class_exists('REST_Controller')) {
            $ci->load->library("REST_Controller");
        }
        if (!function_exists('file_user_log')) {
            $ci->load->helper("logging");
        }
        //check empty responses
        if (empty($responses)) {
            $responses = array(
                "code" => REST_Controller::HTTP_FORBIDDEN,
                "message" => "invalid module, because of empty response",
                "data" => (object)array()
            );
            $ci->set_response($responses, REST_Controller::HTTP_FORBIDDEN);
            file_user_log("", json_encode($responses), $responses["code"]);
        } else {
            if (isset($responses["data"])) {
                $data = $responses["data"];
                if (is_object($data)) {
                    $type_data = (object)array();
                }
            }
            $lastKey = sethead();
            if (!empty($lastKey)) {
                $user_id = getUserFromKey($lastKey);
                //check if empty user id
                if (empty($user_id)) {
                    $responses = array(
                        "code" => REST_Controller::HTTP_FORBIDDEN,
                        "message" => "unable to detect login session",
                        "data" => $type_data
                    );
                    $ci->set_response($responses, REST_Controller::HTTP_FORBIDDEN);
                    file_user_log("", json_encode($responses), $responses["code"]);
                } else {
                    $new_key = _generate_key();
                    $ci->db->select("last_updatekey, pre_hash, current_hash");
                    $qlast = $ci->db->get_where("users", "user_id = $user_id");
                    if ($qlast->num_rows() > 0) {
                        $waktunow = time();
                        $waktulast = strtotime($qlast->row()->last_updatekey);
                        $selisih = floor(($waktunow - $waktulast) / 60);
                        if ($selisih > 5) {
                            $encrypted = my_encrypt($user_id . "#" . $new_key);
                            $data_upd["pre_hash"] = $qlast->row()->current_hash;
                            $data_upd["current_hash"] = $encrypted;
                            $data_upd["last_updatekey"] = date("Y-m-d H:i:s");
                            $upd = $ci->db->update("users", $data_upd, array("user_id" => $user_id));
                            if ($upd) {
                                $ci->set_response($responses, REST_Controller::HTTP_OK, $encrypted);
                                file_user_log($user_id, json_encode($responses), $responses["code"]);
                            } else {
                                $responses["message"] = "ERROR DB SERVER";
                                $responses["data"] = $type_data;
                                $ci->set_response($responses, REST_Controller::HTTP_FORBIDDEN, $lastKey);
                                file_user_log($user_id, json_encode($responses), $responses["code"]);
                            }
                        } else {
                            $ci->set_response($responses, REST_Controller::HTTP_OK, $qlast->row()->current_hash);
                            file_user_log($user_id, json_encode($responses), $responses["code"]);
                        }
                    } else {
                        $responses["code"] = REST_Controller::HTTP_FORBIDDEN;
                        $responses["message"] = "api key expired";
                        $responses["data"] = $type_data;
                        $ci->set_response($responses, REST_Controller::HTTP_FORBIDDEN);
                        file_user_log($user_id, json_encode($responses), $responses["code"]);
                    }
                }
            } else {
                $responses = array(
                    "code" => REST_Controller::HTTP_FORBIDDEN,
                    "message" => "invalid authentication, because of empty keys",
                    "data" => $type_data
                );
                $ci->set_response($responses, REST_Controller::HTTP_FORBIDDEN);
                file_user_log("", json_encode($responses), $responses["code"]);
            }
        }
    }
}

if (!function_exists('new_resp')) {
    function new_resp($responses = array(), $http_response_header = REST_Controller::HTTP_OK, $user_id = '')
    {
        $data = array();
        $type_data = array();
        $ci =& get_instance();
        if (!function_exists('file_user_log')) {
            $ci->load->helper("logging");
        }
        //check empty responses
        if (empty($responses)) {
            $responses = array(
                "code" => REST_Controller::HTTP_BAD_REQUEST,
                "message" => "Empty Response",
                "data" => (object)$data
            );
            $ci->set_response($responses, REST_Controller::HTTP_BAD_REQUEST);
            file_user_log($user_id, json_encode($responses), $responses["code"]);
        } else if (!empty($responses) && isset($responses["data"])) {
            $data = $responses["data"];
            if (is_object($data)) {
                $type_data = (object)array();
            }
        }
        // check if already registered
        if (empty($user_id)) {
            if (empty($responses)) {
                $http_response_header = REST_Controller::HTTP_FORBIDDEN;
                $responses = array(
                    "code" => REST_Controller::HTTP_FORBIDDEN,
                    "message" => "Unable to detect login session",
                    "data" => $type_data
                );
            } else {
                $http_response_header = REST_Controller::HTTP_ACCEPTED;
                if ($responses["code"] > 300) {
                    $http_response_header = REST_Controller::HTTP_FORBIDDEN;
                }
            }
            $ci->set_response($responses, $http_response_header);
            file_user_log($user_id, json_encode($responses), $responses["code"]);
        }
        $new_key = _generate_key();
        $ci->db->select("last_updatekey, pre_hash, current_hash");
        $qlast = $ci->db->get_where("users", "user_id = $user_id");
        if ($qlast->num_rows() > 0) {
            $waktunow = time();
            $waktulast = strtotime($qlast->row()->last_updatekey);
            $selisih = floor(($waktunow - $waktulast) / 60);
            $encrypted = my_encrypt($user_id . "#" . $new_key);
            if (!empty($qlast->row()->current_hash) || !empty($qlast->row()->current_hash)) {
                if ($selisih > 1) {
                    $data_upd["pre_hash"] = $qlast->row()->current_hash;
                    $data_upd["current_hash"] = $encrypted;
                    $data_upd["last_updatekey"] = date("Y-m-d H:i:s");
                    $upd = $ci->db->update("users", $data_upd, array("user_id" => $user_id));
                    if ($upd) {
                        $ci->set_response($responses, REST_Controller::HTTP_OK, $encrypted);
                        file_user_log($user_id, json_encode($responses), $http_response_header);
                    } else {
                        $responses["message"] = "ERROR DB SERVER";
                        $responses["data"] = $type_data;
                        $ci->set_response($responses, REST_Controller::HTTP_FORBIDDEN);
                        file_user_log($user_id, json_encode($responses), $http_response_header);
                    }
                } else {
                    $ci->set_response($responses, REST_Controller::HTTP_OK, $qlast->row()->current_hash);
                    file_user_log($user_id, json_encode($responses), $http_response_header);
                }
            } else {
                $data_upd["pre_hash"] = $qlast->row()->current_hash;
                $data_upd["current_hash"] = $encrypted;
                $data_upd["last_updatekey"] = date("Y-m-d H:i:s");
                $upd = $ci->db->update("users", $data_upd, array("user_id" => $user_id));
                if ($upd) {
                    $ci->set_response($responses, REST_Controller::HTTP_OK, $encrypted);
                    file_user_log($user_id, json_encode($responses), $responses["code"]);
                } else {
                    $responses["message"] = "ERROR DB SERVER";
                    $responses["data"] = $type_data;
                    $ci->set_response($responses, REST_Controller::HTTP_FORBIDDEN);
                    file_user_log($user_id, json_encode($responses), $responses["code"]);
                }
            }
        } else {
            if (empty($responses)) {
                $http_response_header = REST_Controller::HTTP_FORBIDDEN;
                $responses["code"] = REST_Controller::HTTP_FORBIDDEN;
                $responses["message"] = "Invalid API Key";
            } else {
                $http_response_header = REST_Controller::HTTP_ACCEPTED;
                $responses["code"] = (isset($responses["code"]) && !empty($responses["code"])) ? $responses["code"] : $http_response_header;
                $responses["message"] = (isset($responses["code"]) && !empty($responses["message"])) ? $responses["message"] : "";
            }
            $responses["data"] = $type_data;
            $ci->set_response($responses, $http_response_header);
            file_user_log($user_id, json_encode($responses), $responses["code"]);
        }
    }
}

if (!function_exists('setDeviceFCM')) {
    function setDeviceFCM($user_id, $fcm_id = '', $device_id = '')
    {
        $ci =& get_instance();
        if (!empty($user_id) && !empty($fcm_id) && !empty($device_id)) {
            $userz = getUsersById($user_id);
            if (!empty($userz)) {
                $cekdevice = $ci->db->get_where("device", array("uuid" => $user_id, "device_id" => $device_id));
                if ($cekdevice->num_rows() > 0) {
                    $ci->db->update("device", array("fcm_id" => $fcm_id), array("device_id" => $device_id));
                } else {
                    $ci->db->insert("device", array("uuid" => $user_id, "fcm_id" => $fcm_id, "device_id" => $device_id));
                }
            }
        }
    }
}

if (!function_exists('_generate_key')) {
    function _generate_key()
    {
        $ci =& get_instance();
        if (!class_exists('REST_Controller')) {
            $ci->load->library("REST_Controller");
        }

        // Generate a random salt
        $salt = base_convert(bin2hex($ci->security->get_random_bytes(64)), 16, 36);
        // base_convert(
        // If an error occurred, then fall back to the previous method
        if ($salt === FALSE) {
            $salt = hash('sha256', time() . mt_rand());
        }

        $new_key = substr($salt, 0, config_item('rest_key_length'));

        return $new_key;
    }
}

if (!function_exists('html_purify')) {
    function html_purify($dirty_html)
    {
        $clean_html = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($dirty_html))))));

        return $clean_html;
    }
}

if (!function_exists("base_trans")) {
    function base_trans()
    {
        $api_url = MIDTRANS_BASE;
        return $api_url;
    }
}

if (!function_exists("base_iris")) {
    function iris_config()
    {
        $api_url = IRIS_BASE;
        $server_key = IRIS_KEY;
        $iris = array(
            "api_url" => $api_url,
            "server_key" => $server_key
        );
        return $iris;
    }
}

if (!function_exists("key_trans")) {
    function key_trans()
    {
        $server_key = MIDTRANS_KEY;
        return $server_key;
    }
}

if(!function_exists("random_pass")){
    function random_pass(){
        $pass = bin2hex(openssl_random_pseudo_bytes(4));
        return $pass;
    }
}

if (!function_exists("sendx_email")) {
    function sendx_email($to, $subject, $message, $format = 'html')
    {
        $ci =& get_instance();
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'admin@baboo.id',
            'smtp_pass' => 'babooLxgiwyL58',
            'mailtype'  => 'html',
            'newline'  => "\r\n",
            'crlf'  => "\r\n",
            'charset'   => 'iso-8859-1',
            'wordwrap'   => TRUE
        );
        $ci->load->library('email',$config);
        // $ci->email->set_header('MIME-Version', '1.0; charset=utf-8');
        // $ci->email->set_header('Content-type', 'text/html');
        $ci->email->set_mailtype("html");
        $ci->email->from('admin@baboo.id', 'Baboo Digital Indonesia');
        $ci->email->to($to);
        $ci->email->subject($subject);
        $ci->email->message($message);

        if ($ci->email->send()):
            return TRUE;
        else:
            $ci->email->print_debugger();
        endif;

    }
}
/* End of htmlpurifier_helper.php */
/* Location: ./application/helpers/htmlpurifier_helper.php */
